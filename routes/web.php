<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


Route::group(['prefix' => App\Http\Middleware\Locale::getLocale()], function() {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('takehelp', 'HomeController@takehelp');
    Route::get('programs', 'HomeController@programs');
    Route::get('thankyou', 'DonationController@thankyou');
    Route::post('donatesuccess/{id}', 'DonationController@donatesuccess');
    Route::get('search', 'SearchController@search');
    Route::get('smshelp', 'HomeController@smshelp');
    Route::get('alreadyhelp', 'AlreadyController@index');
    Route::get('alreadyhelp/{id}/{slag}', 'AlreadyController@show');
    Route::get('children/donate', 'ChildrenController@donate');
    Route::get('children/{id}/{slag}', 'ChildrenController@show');
    Route::get('news', 'NewsController@index');
    Route::get('news/{id}/{slag}', 'NewsController@show');
    Route::get('videos', 'VideosController@index');
    Route::get('promotions', 'StocksController@index');
    Route::get('promotions/{id}/{slag}', 'StocksController@show');
    Route::get('specinfo', 'SpecinfoController@index');
    Route::get('specinfo/{id}/{slag}', 'SpecinfoController@show');
    Route::get('staff', 'StaffController@index');
    Route::get('staff/{id}/{slag}', 'StaffController@show');
    Route::get('donation/{id}', 'DonationController@show');
});


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){
    Route::get('/', ['uses' => 'AdminController@show'])->name('admin_index');
    Route::resource('news','Admin\NewsController', ['except' => ['show']]);
    Route::get('news/port', 'Admin\NewsController@ajax')
        ->name('get.news.data');
    Route::resource('users','Admin\UsersController', ['except' => ['show']]);
    Route::get('users/port', 'Admin\UsersController@ajax')
        ->name('get.users.data');
    Route::resource('pages','Admin\PagesController', ['except' => ['show']]);
    Route::resource('stocks','Admin\StocksController', ['except' => ['show']]);
    Route::get('stocks/port', 'Admin\StocksController@ajax')
        ->name('get.stocks.data');
    Route::resource('staff','Admin\StaffController', ['except' => ['show']]);
    Route::resource('videos','Admin\VideosController', ['except' => ['show']]);
    Route::resource('specinfo','Admin\SpecinfoController', ['except' => ['show']]);
    Route::get('specinfo/port', 'Admin\SpecinfoController@ajax')
        ->name('get.specinfo.data');
    Route::resource('cities','Admin\CitiesController', ['except' => ['show']]);

    Route::post('options/addgroup', 'Admin\OptionsController@addgroup');
    Route::get('currency', 'Admin\OptionsController@currency');
    Route::resource('options','Admin\OptionsController', ['except' => ['show']]);

    Route::resource('sponsors','Admin\SponsorsController', ['except' => ['show']]);
    Route::resource('donate','Admin\DonateController', ['except' => ['show']]);
    Route::resource('children','Admin\ChildrenController', ['except' => ['show']]);
    Route::get('children/port', 'Admin\ChildrenController@ajax')
        ->name('get.children.data');
    Route::get('album/port', 'Admin\AlbumController@ajax')
        ->name('get.album.data');
    Route::get('donate/port', 'Admin\DonateController@ajax')
        ->name('get.donate.data');
    Route::resource('photos','Admin\PhotoController', ['except' => ['show']]);
    Route::resource('album','Admin\AlbumController');

});



Route::get('setlocale/{locale}', function ($locale) {
    if (in_array($locale, \Config::get('app.locales'))) {
        $referer = Redirect::back()->getTargetUrl();
        $parse_url = parse_url($referer, PHP_URL_PATH);
        if ($parse_url == null) $parse_url = '/';
        $segments = explode('/', $parse_url);
        $params = substr(strstr($referer, '?'), 1, strlen($referer));

        if (in_array($segments[1],  Config::get('app.locales'))) {

            unset($segments[1]);
        }
        if ($locale != Config::get('app.locale')){
            if ($locale != 'ru') {  array_splice($segments, 1, 0, $locale); }

        }
        if ($params != '') {
            $url = Request::root().implode("/", $segments).'?'.$params;
        } else{
            $url = Request::root().implode("/", $segments).$params;

        }
    }

    return redirect($url);

})->name('setlocale');


