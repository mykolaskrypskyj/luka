$(function() {
    $('#toggle-link').click(function(e) {
        var $message = $('#message');

        if ($message.css('display') != 'block') {
            $message.show();

            var firstClick = true;
            $(document).bind('click.myEvent', function(e) {
                if (!firstClick && $(e.target).closest('#message').length == 0) {
                    $message.hide();
                    $(document).unbind('click.myEvent');
                }
                firstClick = false;
            });
        }
        e.preventDefault();
    });
});

$(function() {
    $('#toggle-search').click(function(e) {
        var $message = $('#message-search');

        if ($message.css('display') != 'block') {
            $message.show();

            var firstClick = true;
            $(document).bind('click.myEvent', function(e) {
                if (!firstClick && $(e.target).closest('#message-search').length == 0) {
                    $message.hide();
                    $(document).unbind('click.myEvent');
                }
                firstClick = false;
            });
        }
        e.preventDefault();
    });
});
