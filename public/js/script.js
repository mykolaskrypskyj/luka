(function(){
	var deskToggle = document.querySelector('.desk-toggle');
var mobileToggle = document.querySelector('.mobile-toggle');

if(deskToggle){
	deskToggle.onclick = function(){
		if (mobileToggle.style.display == 'block') {
			mobileToggle.style.display = 'none';
		}else {
			mobileToggle.style.display = 'block';
		}	
	}
}
var menuToggle = document.querySelector('.header-bottom .menu-icon');
var mobileMenu = document.querySelector('.header-bottom .mobile');

if(menuToggle){
	menuToggle.onclick = function(){
		if (mobileMenu.style.display == 'block') {
			mobileMenu.style.display = 'none';
		}else {
			mobileMenu.style.display = 'block';
		}	
	}
}
var sponsorToggle = document.querySelector('.left-block .left-block-text');
var mobileSponsor = document.querySelector('.left-block .noleft-block');

if(sponsorToggle){
	sponsorToggle.onclick = function(){
		if (mobileSponsor.style.display == 'flex') {
			mobileSponsor.style.display = 'none';
		}else {
			mobileSponsor.style.display = 'flex';
		}	
	}
}
})();

(function(){
	var btn_next, btn_prev;
	var images = document.querySelectorAll('#gallery .photos img');
		if(images.length <= 1) return;
	if(btn_next){
		btn_next.addEventListener('click', autoSlider);
	}

	var timer;
	autoSlider();
	var i = 0;


	function autoSlider(){
		if(images.length > 1){
			clearTimeout(timer);
			timer = setTimeout(function(){		
				if(i >= 0) {
					images[i].className = '';
					i++;
					if(i >= images.length){
						i = 0;
					}
					
					var dots = document.querySelectorAll('.dots .dot');
					for(var j = 0; j<images.length; j++){
						images[j].removeAttribute('class');
						
						dots[j].className = "dot";
					}
					dots[i].className = 'dot active';
					images[i].className = 'show';
				}
				autoSlider();
			}, 3000);
		}
	}


	var imgButtons = document.querySelector('.buttons');


	if(images.length > 1){
		var imgPrev = document.createElement('img');
	  	imgPrev.className = "prev";
	  	imgPrev.setAttribute('src', '/img/back.png');
	  	imgButtons.append(imgPrev);
		btn_prev = document.querySelector('#gallery .buttons .prev');
		
	}
	if(images.length > 1){
		var imgNext = document.createElement('img');
	  	imgNext.className = "next";
	  	imgNext.setAttribute('src', '/img/forward.png');
	  	imgButtons.append(imgNext);
		btn_next = document.querySelector('#gallery .buttons .next');
	}

	if(images.length > 1){
		for(var e = 0; e < images.length; e++){
			var dots_wrapper = document.querySelector('.dots');
			var div = document.createElement('div');
		  	div.className = "dot";
			div.setAttribute('data-id', e);
			div.addEventListener('click', dotActive);
			dots_wrapper.append(div);
		}
	}
	var dots = document.querySelectorAll('.dots .dot');
	dots[i].className = 'dot active';
	

	function dotActive(e){
		var slide_id = e.target.getAttribute('data-id');
		var dots = document.querySelectorAll('.dots .dot');
		clearTimeout(timer);
		for(var j = 0; j<images.length; j++){
			images[j].removeAttribute('class');
			
			dots[j].className = "dot";
		}
		e.target.className = 'dot active';
		images[slide_id].className = 'show';
		
		i = slide_id;

		setTimeout(function(){autoSlider()}, 3000);
	}

	btn_prev.onclick = function(){
		clearTimeout(timer);
		images[i].className = '';
		i--;
		if(i < 0){
			i = images.length - 1;	
		}
		
		var dots = document.querySelectorAll('.dots .dot');
		for(var j = 0; j<images.length; j++){
			images[j].removeAttribute('class');
			
			dots[j].className = "dot";
		}
		dots[i].className = 'dot active';
		images[i].className = 'show';
		
		setTimeout(function(){autoSlider()}, 3000);
	}

	btn_next.onclick = function(){
		clearTimeout(timer);
		images[i].className = '';
		i++;
		if(i >= images.length){
			i = 0;
		}
		
		var dots = document.querySelectorAll('.dots .dot');
		for(var j = 0; j<images.length; j++){
			images[j].removeAttribute('class');
			
			dots[j].className = "dot";
		}
		dots[i].className = 'dot active';
		images[i].className = 'show';

		setTimeout(function(){autoSlider()}, 3000);
	}

	document.querySelector('#gallery').onmouseover = function(){clearTimeout(timer);}
	document.querySelector('#gallery').onmouseout = function(){clearTimeout(timer);
		setTimeout(function(){autoSlider()}, 3000);}

})();


$(function(){
    $('a.receipt.invoice').click(function(){
        var form = $(this).closest('div.form').find('form').get(0);
        console.log(form);
        var id = $(this).data('id');
        form.action = '/donation/'+id
        form.submit();
    });
    
});   