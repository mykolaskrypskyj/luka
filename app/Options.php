<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Options extends Model
{
    protected $table="options";
    protected $fillable=['key','values','data'];
    protected $primaryKey = 'key';
    public $incrementing = false;

    public function getDataAttribute () {

        return json_decode($this->attributes['values'],true);

    }
    public function getDataObjAttribute () {

        return json_decode($this->attributes['values']);

    }

    public static function getSettings($name){
        if( $obj = self::where('key',$name)->first() ){
            return $obj->data_obj;
        }
        return [];
    }

    public static function getKeyValue($name,$key){
        if( $obj = self::where('key',$name)->first() ){
            $value = $obj->data;
            return $value[$key];
        }
        return [];
    }

    public function setDataAttribute($value){
        $this->attributes['values'] = json_encode($value);
    }

}
