<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Session;

class Locale
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//        $request->session()->forget('set_lang');

//        var_dump(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2)); die();

        $raw_locale = $this->getLocale();

        if (in_array($raw_locale, Config::get('app.locales'))) {
            $locale = $raw_locale;
        }
        else $locale ='ru';


        if ($request->session()->get('set_lang') == null) {
            $request->session()->put('set_lang', 1);

            $browser_lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
            switch ($browser_lang) {
                case 'uk' : return Redirect::to('/setlocale/ua'); break;
                case 'en' : return Redirect::to('/setlocale/en'); break;
                case 'de' : return Redirect::to('/setlocale/de'); break;
            }

        }


        App::setLocale($locale);
        return $next($request);
    }

    public static function getLocale()
    {
        if (!empty(Request::segment(1)) && in_array(Request::segment(1), Config::get('app.locales'))) {
            return Request::segment(1);
        }
        return '';
    }
}
