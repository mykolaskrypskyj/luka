<?php

namespace App\Http\Controllers;

use App\Stocks;
use Illuminate\Http\Request;

class StocksController extends Controller
{
    public function index()
    {
        $stocks=Stocks::where([['public','=',1]])->orderBy('id','DESC')->paginate(6);
        return view('stocks.index',['stocks'=>$stocks]);
    }

    public function show($id)
    {
        $stocks=Stocks::findOrFail($id);
        return view('stocks.show',['stocks'=>$stocks]);
    }

}
