<?php

namespace App\Http\Controllers;

use App\Donate;
use App\Pages;
use Illuminate\Http\Request;
use App\Children;
use App\Providers\LiqPayServiceProvider;
use Illuminate\Support\Facades\URL;

class ChildrenController extends Controller
{
/*    
    public function edit($id)
    {
        $record=Children::findOrFail($id);
        return view('children.child',['child'=>$record]);
    }
 * 
 */
    public function show($id)
    {
        $record=Children::findOrFail($id);
        $program = Pages::where('type', 'programs')->firstOrFail();
        return view('children.show',['child'=>$record,'program'=>$program]);
    }

    public function donate(Request $request)
    {
        $data = $request->only('children_id','_token','sum','comment','lang');
        if ($data['sum'] == '') {$data['sum'] = 100;}
        $data['channel']='Пожертвование добавлено с помощью канала LiqPay. С коментарием: '.$data['comment'];
        $data['status']= 0 ;
        $child = Children::FindorFail($data['children_id']);
            if ($data['lang'] != '') {
                $data['lang'] ='/'.$data['lang'];
            }

        $donate = Donate::create($data);
        $liqpay = new LiqPayServiceProvider(\App\Options::getKeyValue('liqpay','public_key'), \App\Options::getKeyValue('liqpay','private_key'));
                $html = $liqpay->cnb_form(array(
                'action'         => 'pay',
                'amount'         => $data['sum'],
                'currency'       => 'UAH',
                'description'    => $child['name'].' '.$data['comment'],
                'order_id'       => $donate['id'],
                'version'        => '3',
                'sandbox'       => \App\Options::getKeyValue('liqpay','sandbox'),
                'server_url'    => URL::to('/').'/donatesuccess/'.$donate['id'],
                'result_url'    => URL::to('/').$data['lang'].'/thankyou'
                ));
        return $html;

    }
}
