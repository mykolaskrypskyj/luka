<?php

namespace App\Http\Controllers;

use App\Children;
use App\Pages;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $children=Children::where([['public','=',1],['collected_flag','=',0]])->orderBy('id','DESC')->paginate(6);
        return view('hp',['children'=>$children ]);
    }

    public function takehelp()
    {
        $locale = \App::getLocale();
        $page=Pages::where('type', 'takehelp')->firstOrFail();
        $fields = $page->content;
        return view('pages.takehelp',['page'=>$page,'field'=>$fields,'locale'=>$locale]);
    }
    public function programs()
    {
        $locale = \App::getLocale();
        $page=Pages::where('type', 'programs')->firstOrFail();
        $fields = $page->content;
        return view('pages.programs',['page'=>$page,'field'=>$fields,'locale'=>$locale]);
    }

    public function smshelp()
    {
        $locale = \App::getLocale();
        $page=Pages::where('type', 'smshelp')->firstOrFail();
        $fields = $page->content;
        return view('pages.programs',['page'=>$page,'field'=>$fields,'locale'=>$locale]);
    }
}
