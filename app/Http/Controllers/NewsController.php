<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
    public function index()
    {
        $news=News::where([['public','=',1]])->orderBy('id','DESC')->paginate(6);
        return view('news.index',['news'=>$news]);
    }

    public function show($id)
    {
        $news=News::findOrFail($id);
        return view('news.show',['news'=>$news]);
    }

}
