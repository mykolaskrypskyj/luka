<?php

namespace App\Http\Controllers;

use App\Videos;
use Illuminate\Http\Request;

class VideosController extends Controller
{
    public function index()
    {
        $videos=Videos::where([['public','=',1]])->orderBy('id','DESC')->paginate(6);
        return view('videos.index',['videos'=>$videos]);
    }
}
