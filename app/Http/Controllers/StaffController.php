<?php

namespace App\Http\Controllers;

use App\Pages;
use App\Staff;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    public function index()
    {
        $staff=Staff::where([['public','=',1]])->orderBy('id','DESC')->paginate(100);
        $locale = \App::getLocale();
        $page= Pages::where('type', 'team')->firstOrFail();
        $field = $page->content;
        return view('staff.index',['staff'=>$staff,'page'=>$page,'field'=>$field,'locale'=>$locale]);
    }

    public function show($id)
    {
        $staff=Staff::findOrFail($id);
        return view('staff.show',['staff'=>$staff]);
    }
}
