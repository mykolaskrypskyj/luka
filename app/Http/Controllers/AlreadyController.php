<?php

namespace App\Http\Controllers;

use App\Children;
use App\Pages;
use Illuminate\Http\Request;

class AlreadyController extends Controller
{
    public function index()
    {
        $children=Children::where([['public','=',1],['collected_flag','=',1]])->orderBy('id','DESC')->paginate(6);
        return view('already.index',['children'=>$children]);
    }

    public function show($id)
    {
        $record=Children::findOrFail($id);
        $program = Pages::where('type', 'programs')->firstOrFail();
        return view('already.show',['child'=>$record,'program'=>$program]);
    }
}
