<?php

namespace App\Http\Controllers\Admin;

use App\Album;
use App\News;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;

class NewsController extends Controller
{

    public function ajax() {
        $query = News::query();
        return Datatables::of($query)
            ->rawColumns(['edit','img','name']) // set columsn to render html
            ->addColumn('id', function($row) { return $row->id; })
            ->addColumn('img', function($row) { return '<img width=20 height=20 src="'.$row->preview.'">' ; })
            ->addColumn('name', function($row) { return $row->title_ru; })

            ->addColumn('edit', function($row) { return '<a href="/admin/news/'.$row->id.'/edit"><i class="fa fa-pencil"></i></a> &nbsp; &nbsp;
                         <form style="display:none;"  method="POST" action="'.action('Admin\NewsController@destroy',['articles'=>$row->id]).'">
                             <input type="hidden" name="_method" value="delete"/>
                             <input type="hidden" name="_token" value="'.csrf_token().'"/>
                             <button type="submit"><i class="fa fa-trash"></i></button>
                         </form>'; })
            ->make(true);
    }
    public function index()
    {
        $news=News::orderBy('id','DESC')->get();
        return view('admin.news.index',['news'=>$news]);
    }

    public function create()
    {
        $albums = Album::pluck('title','id');
        $locales =  \Config::get('app.locales');

        return view('admin.news.form',['albums'=>$albums,'locales'=>$locales]);
    }

    public function store(Request $request)
    {
            $data = $request->only('preview','title_ru','content_ru','title_ua','content_ua','title_en','content_en','title_de','content_de','public');
            $data['user_id']=\Auth::id();

            $news = News::create($data);
            $news->update($request->only('images'));
            return redirect('admin/news')->with('message','Новость добавлена');
    }

    public function edit($id)
    {
        $record=News::findOrFail($id);
        $albums = Album::pluck('title','id');
        $locales =  \Config::get('app.locales');

        return view('admin.news.form',['record'=>$record,'albums'=>$albums,'locales'=>$locales]);
    }


    public function update(Request $request,$id)
    {
        $news=News::findOrFail($id);
        $data = $request->only('preview','title_ru','content_ru','title_ua','content_ua','title_en','content_en','title_de','content_de','public','images');
        if (!(\Request::has('images'))) {
            $data['images'] =[];
        }

        $news->update($data);


        return back()->with('message', 'Статья изменена');
    }
    public function destroy($id)
    {
        $news=News::findOrFail($id);
        $news->delete();
        return redirect('admin/news')->with('message','Новость удалена');
    }
}
