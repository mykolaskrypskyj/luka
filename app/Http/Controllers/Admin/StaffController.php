<?php

namespace App\Http\Controllers\Admin;

use App\Album;
use App\Staff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaffController extends Controller
{
    public function index()
    {
        $staff=Staff::orderBy('id','DESC')->paginate(10);
        return view('admin.staff.index',['staff'=>$staff]);
    }

    public function create()
    {
        $albums = Album::pluck('title','id');
        $locales =  \Config::get('app.locales');

        return view('admin.staff.form',['albums'=>$albums,'locales'=>$locales]);
    }

    public function store(Request $request)
    {
        $data = $request->only('name_ru','details_ru','position_ru','name_ua','details_ua','position_ua','name_en','details_en','position_en','name_de','details_de','position_de','telephon','email','photo','public');
        $data['user_id']=\Auth::id();

        $staff = Staff::create($data);
        return redirect('admin/staff')->with('message','Сотрудник добавлен');
    }

    public function edit($id)
    {
        $record=Staff::findOrFail($id);
        $albums = Album::pluck('title','id');
        $locales =  \Config::get('app.locales');

        return view('admin.staff.form',['record'=>$record,'albums'=>$albums,'locales'=>$locales]);
    }


    public function update(Request $request,$id)
    {
        $staff=Staff::findOrFail($id);
        $data = $request->only('name_ru','details_ru','position_ru','name_ua','details_ua','position_ua','name_en','details_en','position_en','name_de','details_de','position_de','telephon','email','photo','public');
        $staff->update($data);
        return back()->with('message', 'Сотрудник изменен');
    }
    public function destroy($id)
    {
        $staff=Staff::findOrFail($id);
        $staff->delete();
        return redirect('admin/staff')->with('message','Сотрудник удален');
    }
}
