<?php

namespace App\Http\Controllers\Admin;

use App\Specinfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;

class SpecinfoController extends Controller
{

    public function ajax() {
        $query = Specinfo::query();
        return Datatables::of($query)
            ->rawColumns(['edit','name']) // set columsn to render html
            ->addColumn('id', function($row) { return $row->id; })
            ->addColumn('name', function($row) { return $row->title_ru; })
            ->addColumn('edit', function($row) { return '<a href="/admin/specinfo/'.$row->id.'/edit"><i class="fa fa-pencil"></i></a> &nbsp; &nbsp;
                         <form style="display:none;"  method="POST" action="'.action('Admin\SpecinfoController@destroy',['articles'=>$row->id]).'">
                             <input type="hidden" name="_method" value="delete"/>
                             <input type="hidden" name="_token" value="'.csrf_token().'"/>
                             <button type="submit"><i class="fa fa-trash"></i></button>
                         </form>'; })
            ->make(true);
    }

    public function index()
    {
        $specinfo=Specinfo::orderBy('id','DESC')->paginate(10);
        return view('admin.specinfo.index',['specinfo'=>$specinfo]);
    }

    public function create()
    {
        $locales =  \Config::get('app.locales');

        return view('admin.specinfo.form',['locales'=>$locales]);
    }

    public function store(Request $request)
    {
        $data = $request->only('title_ru','title_ua','title_en','title_de','video_ru','video_ua','video_en','video_de','details_ru','details_ua','details_en','details_de','public');
        $data['user_id']=\Auth::id();

        $specinfo = Specinfo::create($data);

        return redirect('admin/specinfo')->with('message','Информация добавлена');
    }

    public function edit($id)
    {
        $record=Specinfo::findOrFail($id);
        $locales =  \Config::get('app.locales');

        return view('admin.specinfo.form',['record'=>$record,'locales'=>$locales]);
    }


    public function update(Request $request,$id)
    {
        $specinfo=Specinfo::findOrFail($id);
        $data = $request->only('title_ru','title_ua','title_en','title_de','video_ru','video_ua','video_en','video_de','details_ru','details_ua','details_en','details_de','public');
        $specinfo->update($data);
        return back()->with('message', 'Информация изменена');
    }
    public function destroy($id)
    {
        $specinfo=Specinfo::findOrFail($id);
        $specinfo->delete();
        return redirect('admin/specinfo')->with('message','Информация удалена');
    }
}
