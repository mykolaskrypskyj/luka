<?php

namespace App\Http\Controllers\Admin;

use App\Album;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use DataTables;


class AlbumController extends Controller
{

    public function ajax() {
        $query = Album::query();
        return Datatables::of($query)
            ->rawColumns(['edit','img','name']) // set columsn to render html
            ->addColumn('id', function($row) { return $row->id; })
            ->addColumn('img', function($row) { return '<a href="/admin/album/'.$row->id.'"><img width=100 height=100 src="'.$row->icon_url.'"></a>' ; })
            ->addColumn('name', function($row) { return '<a href="/admin/album/'.$row->id.'">'.$row->title.'</a>'; })

            ->addColumn('edit', function($row) { return '<a href="/admin/album/'.$row->id.'/edit"><i class="fa fa-pencil"></i></a> &nbsp; &nbsp;
                         <form style="display:none;"  method="POST" action="'.action('Admin\AlbumController@destroy',['articles'=>$row->id]).'">
                             <input type="hidden" name="_method" value="delete"/>
                             <input type="hidden" name="_token" value="'.csrf_token().'"/>
                             <button type="submit"><i class="fa fa-trash"></i></button>
                         </form>'; })
            ->make(true);
    }


    public function index()
    {
        $list = Album::orderBy('id','DESC')->paginate(16);
        return view('admin.album.index',['list'=>$list]);
    }

    public function create()
    {
        return view('admin.album.form');
    }

    public function store(Request $request)
    {
        $id = Album::create($request->all())->id;
        return redirect('admin/album')->with('message','Альбом добавлен');
    }

    public function edit($id){
        $record = Album::findOrFail($id);
        return view('admin.album.form',['record'=>$record]);
    }
    public function show($id){
        if (\Request::ajax())
        {
            $list = Album::findOrFail($id)->photos;
            $response = [];
            foreach($list as $item){
                $response['response']['photos'][] = $item->getProperties();
            }
            return response()->json($response);
        } else {
            $record = Album::findOrFail($id);
            return view('admin.album.photos',['record'=>$record]);
        }
    }
    
    public function update(Request $request, $id)
    {
        $record = Album::findOrFail($id);
        $data = $request->only('title');  
        $record->update($data);
        return redirect('admin/album')->with('message','Альбом изменен');
    }

    public function destroy($id) //?????
    {
        $record = Album::findOrFail($id);
        $record->delete();
        return redirect('admin/album');
    }
}
