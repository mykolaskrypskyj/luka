<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Photos;
use App\Http\Controllers\Controller;

class PhotoController extends Controller
{
    public function index()
    {
        return 'ok';
    }
    
    public function store(Request $request)
    {
        Photos::add($request);
        return response()->json([
            'state' => 'ok'
        ]);        
    }

    public function destroy($id)
    {
        Photos::findOrFail($id)->del();
        return response()->json([
            'state' => 'ok'
        ]);        
    }

}
