<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Options;
use App\Http\Controllers\Controller;


class OptionsController extends Controller
{
    public function index()
    {
        $options=Options::orderBy('key','ASC')->paginate(50);
        return view('admin.options.index',['options'=>$options]);

    }

    public function addgroup(Request $request)
    {
        $data = $request->only('key');
        $options = Options::create($data);

        return redirect('admin/options')->with('message','Група добавлена');
    }

    public function currency()
    {
        $options=Options::where([['key','=','currency']])->first();

        return view('admin.options.currency',['option'=>$options]);
    }

    public function update(Request $request,$key)
    {
        $options=Options::where('key', '=' ,$key)->firstOrFail();

        $data = $request->only('key','data');
        $options->update($data);
        return back()->with('message', 'Опция изменена');
    }


}
