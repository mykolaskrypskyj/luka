<?php

namespace App\Http\Controllers\Admin;

use App\Stocks;
use App\Album;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
class StocksController extends Controller
{
    public function ajax() {
        $query = Stocks::query();
        return Datatables::of($query)
            ->rawColumns(['edit','img','name']) // set columsn to render html
            ->addColumn('id', function($row) { return $row->id; })
            ->addColumn('img', function($row) { return '<img width=20 height=20 src="'.$row->preview.'">' ; })
            ->addColumn('name', function($row) { return $row->title_ru; })
            ->addColumn('edit', function($row) { return '<a href="/admin/stocks/'.$row->id.'/edit"><i class="fa fa-pencil"></i></a> &nbsp; &nbsp;
                         <form style="display:none;"  method="POST" action="'.action('Admin\StocksController@destroy',['articles'=>$row->id]).'">
                             <input type="hidden" name="_method" value="delete"/>
                             <input type="hidden" name="_token" value="'.csrf_token().'"/>
                             <button type="submit"><i class="fa fa-trash"></i></button>
                         </form>'; })
            ->make(true);
    }

    public function index()
    {
        $news=Stocks::orderBy('id','DESC')->get();
        return view('admin.stocks.index',['news'=>$news]);
    }

    public function create()
    {
        $albums = Album::pluck('title','id');
        $locales =  \Config::get('app.locales');

        return view('admin.stocks.form',['albums'=>$albums,'locales'=>$locales]);
    }

    public function store(Request $request)
    {
        $data = $request->only('preview','title_ru','content_ru','title_ua','content_ua','title_en','content_en','title_de','content_de','public');
        $data['user_id']=\Auth::id();

        $news = Stocks::create($data);
        $news->update($request->only('images'));

        return redirect('admin/stocks')->with('message','Акция добавлена');
    }

    public function edit($id)
    {
        $record=Stocks::findOrFail($id);
        $albums = Album::pluck('title','id');
        $locales =  \Config::get('app.locales');

        return view('admin.stocks.form',['record'=>$record,'albums'=>$albums,'locales'=>$locales]);
    }


    public function update(Request $request,$id)
    {
        $news=Stocks::findOrFail($id);
        $data = $request->only('preview','title_ru','content_ru','title_ua','content_ua','title_en','content_en','title_de','content_de','public','images');
        if (!(\Request::has('images'))) {
            $data['images'] =[];
        }
        $news->update($data);

        return back()->with('message', 'Акция изменена');
    }
    public function destroy($id)
    {
        $news=Stocks::findOrFail($id);
        $news->delete();
        return redirect('admin/stocks')->with('message','Акция удалена');
    }
}
