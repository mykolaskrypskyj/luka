<?php

namespace App\Http\Controllers\Admin;

use App\Children;
use App\Donate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Support\Facades\Mail;
class DonateController extends Controller
{
    public function ajax() {
        $query = Donate::query();
        return Datatables::of($query)
            ->rawColumns(['edit','img','name']) // set columsn to render html
            ->addColumn('id', function($row) { return $row->id; })
            ->addColumn('child', function($row) { return $row->child ; })
            ->addColumn('channel', function($row) { return $row->channel ; })
            ->addColumn('status', function($row) { return $row->status; })
            ->addColumn('created_at', function($row) { return $row->created_at; })

            ->addColumn('edit', function($row) { return '<a href="/admin/donate/'.$row->id.'/edit"><i class="fa fa-pencil"></i></a> &nbsp; &nbsp;
                         '; })
            ->make(true);
    }

    public function index()
    {
        $donate=Donate::orderBy('id','DESC')->paginate(10);
        return view('admin.donate.index',['donate'=>$donate]);
    }

    public function create()
    {
        $children = Children::pluck('name_ru','id');

        return view('admin.donate.form',['children'=>$children]);
    }

    public function store(Request $request)
    {
        $data = $request->only('sum','children_id','channel','status');
        $user=\Auth::user();
        $child = Children::findorfail($data['children_id']);
        if ($child->lack_money <= $data['sum']) {
            $child['collected_flag'] = 1;
            $child['public'] = 0;
            $child->update();
        }
        $data['channel']='Пользователь '.$user->name.'(ID: '.$user->id.') добавил пожертвование через админку';
        $donate = Donate::create($data);
        if ($child->collected_money >= $child->sum_need) {
            $flag = [
                'collected_flag' => 1,
                'public' => 0
            ];
            $child->update($flag);
        }

            Mail::send(array(),array(), function($message) use ($child, $donate)
        {

            $message->subject('Получено пожертвования c админ-части');
            $message->setBody('Полученно пожертвование для '.$child->name_ru.' с админки сайта. ID пожертвования:'.$donate->id);
            $message->to(\App\Options::getKeyValue('notification_email','email_child_notification'));
        });


        return redirect('admin/donate')->with('message','Пожертвование добавлено');
    }

    public function edit($id)
    {
        $record=Donate::findOrFail($id);
        $children = Children::findOrFail($record->children_id);


        return view('admin.donate.show',['record'=>$record,'children'=>$children]);
    }

/*
    public function update(Request $request,$id)
    {
        $donate=Donate::findOrFail($id);
        $data = $request->only('sum','children_id','channel','status');
        $child = Children::findorfail($data['children_id']);
        if ($child->lack_money <= $data['sum']) {
            $child['collected_flag'] = 1;
            $child['public'] = 0;
            $child->update();
        }
        $donate->update($data);

        if ($child->collected_money >= $child->sum_need) {
            $flag = [
                'collected_flag' => 1,
                'public' => 0
            ];
            $child->update($flag);
        }
        Mail::send(array(),array(), function($message) use ($child, $donate)
        {

            $message->subject('Редактирование пожертвования c админ-части');
            $message->setBody('Редактировано пожертвования для '.$child->name_ru.' с админки сайта. ID пожертвования:'.$donate->id);
            $message->to(\App\Options::getKeyValue('notification_email','email_child_notification'));
        });

        return back()->with('message', 'Пожертвование изменено');

    }*/
  /*  public function destroy($id)
    {
        $donate=Donate::findOrFail($id);
        $donate->delete();
        return redirect('admin/donate')->with('message','Пожертвование удалено');
    }*/
}
