<?php

namespace App\Http\Controllers\Admin;

use App\Pages;
use Illuminate\Http\Request;
use App\Album;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function index()
    {
        $pages=Pages::orderBy('id','DESC')->paginate(10);
        return view('admin.pages.index',['pages'=>$pages]);
    }

    public function edit($id)
    {
        $record=Pages::findOrFail($id);
        $albums = Album::pluck('title','id');
        $locales =  \Config::get('app.locales');
        foreach ($locales as  $locale) {
            $fields[$locale] = json_decode($record['content_'.$locale],true);
        }
//        print_r($fields); die();
        return view('admin.pages.form',['record'=>$record,'albums'=>$albums,'locales'=>$locales, 'fields' =>$fields]);
    }

    public function update(Request $request,$id)
    {
        $page=Pages::findOrFail($id);
        $type = $page->type;
        $locales =  \Config::get('app.locales');
        switch ($type) {
            case ('takehelp'):
                 foreach ($locales as $locale) {
                     $data = array();
                     $datas = array();
                     $data =  $request->only('title_'.$locale);
                     $datas = $request->only('text_before_file_'.$locale, 'text_after_file_'.$locale);
                     if ($request->hasFile('file_'.$locale)){
                         $root=$_SERVER['DOCUMENT_ROOT']."/ankets/";
                         $f_name = 'anketa_'.$locale.'.doc';
                         $file = $request->file('file_'.$locale)->move($root,$f_name);
                     }
                     $datas['file_'.$locale] = '/ankets/anketa_'.$locale.'.doc';
                     $data['content_'.$locale] =  json_encode($datas);
                     $page->update($data);
                 }

                break;
            case ('team'):
                 foreach ($locales as $locale) {
                     $data = array();
                     $datas = array();
                     $data =  $request->only('title_'.$locale,'photo_id');
                     $datas = $request->only('text_'.$locale);
                     $data['content_'.$locale] =  json_encode($datas);
                     $page->update($data);
                 }

                break;
            case ('programs'):
                 foreach ($locales as $locale) {
                     $data = array();
                     $datas = array();
                     $data =  $request->only('title_'.$locale,'photo_id');
                     $datas = $request->only('text_'.$locale);
                     $data['content_'.$locale] =  json_encode($datas);
                     $page->update($data);
                 }

                break;
            case ('smshelp'):
                 foreach ($locales as $locale) {
                     $data = array();
                     $datas = array();
                     $data =  $request->only('title_'.$locale,'photo_id');
                     $datas = $request->only('text_'.$locale);
                     $data['content_'.$locale] =  json_encode($datas);
                     $page->update($data);
                 }

                break;
        }




        return back()->with('message', 'Страница изменена');
    }
}
