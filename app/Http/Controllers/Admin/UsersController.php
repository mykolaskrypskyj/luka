<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
class UsersController extends Controller
{
    public function ajax()
    {
        $query = User::query();
        return Datatables::of($query)
            ->rawColumns(['email','edit'])// set columsn to render html
            ->addColumn('id', function ($row) {
                return $row->id;
            })
            ->addColumn('name', function ($row) {
                return $row->name;
            })
            ->addColumn('email', function ($row) {
                return $row->email;
            })
            ->addColumn('created', function ($row) {
                return $row->created_at;
            })
            ->addColumn('updated', function ($row) {
                return $row->updated_at;
            })
            ->addColumn('edit', function ($row) {
                return '<a href="/admin/users/' . $row->id . '/edit"><i class="fa fa-pencil"></i></a> &nbsp; &nbsp;
                         <form style="display:none;"  method="POST" action="' . action('Admin\UsersController@destroy', ['articles' => $row->id]) . '">
                             <input type="hidden" name="_method" value="delete"/>
                             <input type="hidden" name="_token" value="' . csrf_token() . '"/>
                             <button type="submit"><i class="fa fa-trash"></i></button>
                         </form>';
            })
            ->make(true);
    }
    public function index()
    {
        $users=User::orderBy('id','DESC')->get();
        return view('admin.users.index',['users'=>$users]);
    }
    public function create()
    {

        return view('admin.users.form');
    }

    public function store(Request $request)
    {
        $data = $request->only('name','email','password_form','password_repeat');
        if ($data['password_form'] == $data['password_repeat'] ) {
            $data['password'] = $data['password_form'];
            $users = User::create($data);
            return redirect('admin/users')->with('message','Пользователь создан');
        } else {
            return redirect('admin/users')->with('message','Пароли не совпадают');

        }
    }

    public function edit($id)
    {
        $record=User::findOrFail($id);

        return view('admin.users.form',['record'=>$record]);
    }

    public function update(Request $request,$id)
    {
        $user=User::findOrFail($id);

        $data = $request->only('name','email','password_form','password_repeat');
        if ($data['password_form'] == $data['password_repeat'] ) {
            if ($data['password_form'] != null) {
                $data['password'] = $data['password_form'];
            }
            $user->update($data);
            return redirect('admin/users')->with('message','Пользователь обновлен');
        } else {
            return redirect('admin/users/'.$user->id.'/edit')->with('message','Пароли не совпадают');

        }
    }


}

?>