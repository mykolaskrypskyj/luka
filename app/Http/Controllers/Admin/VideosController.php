<?php

namespace App\Http\Controllers\Admin;

use App\Videos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideosController extends Controller
{
    public function index()
    {
        $videos=Videos::orderBy('id','DESC')->paginate(10);
        return view('admin.videos.index',['videos'=>$videos]);
    }

    public function create()
    {
        $locales =  \Config::get('app.locales');

        return view('admin.videos.form',['locales'=>$locales]);
    }

    public function store(Request $request)
    {
        $data = $request->only('title','video_ru','video_ua','video_en','video_de','public');
        $data['user_id']=\Auth::id();

        $videos = Videos::create($data);

        return redirect('admin/videos')->with('message','Видео добавлено');
    }

    public function edit($id)
    {
        $record=Videos::findOrFail($id);
        $locales =  \Config::get('app.locales');

        return view('admin.videos.form',['record'=>$record,'locales'=>$locales]);
    }


    public function update(Request $request,$id)
    {
        $videos=Videos::findOrFail($id);
        $data = $request->only('title','video_ru','video_ua','video_en','video_de','public');
        $videos->update($data);
        return back()->with('message', 'Видео изменено');
    }
    public function destroy($id)
    {
        $videos=Videos::findOrFail($id);
        $videos->delete();
        return redirect('admin/videos')->with('message','Видео удалено');
    }
}
