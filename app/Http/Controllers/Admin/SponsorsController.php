<?php

namespace App\Http\Controllers\Admin;

use App\Sponsors;
use Illuminate\Http\Request;
use App\Album;
use App\Http\Controllers\Controller;


class SponsorsController extends Controller
{
    public function index()
    {
        $sponsors=Sponsors::orderBy('id','DESC')->paginate(10);
        return view('admin.sponsors.index',['sponsors'=>$sponsors]);
    }

    public function create()
    {
        $albums = Album::pluck('title','id');

        return view('admin.sponsors.form',['albums'=>$albums]);
    }

    public function store(Request $request)
    {
        $data = $request->only('title','photo','link','public');
        $data['user_id']=\Auth::id();

        $sponsors=Sponsors::create($data);
        return redirect('admin/sponsors')->with('message','Спонсор добавлен');
    }

    public function edit($id)
    {
        $record=Sponsors::findOrFail($id);
        $albums = Album::pluck('title','id');

        return view('admin.sponsors.form',['record'=>$record,'albums'=>$albums]);
    }


    public function update(Request $request,$id)
    {
        $sponsors=Sponsors::findOrFail($id);
        $data = $request->only('title','photo','link','public');
        $sponsors->update($data);
        return back()->with('message', 'Спонсор изменен');
    }
    public function destroy($id)
    {
        $staff=Sponsors::findOrFail($id);
        $staff->delete();
        return redirect('admin/sponsors')->with('message','Спонсор удален');
    }
}
