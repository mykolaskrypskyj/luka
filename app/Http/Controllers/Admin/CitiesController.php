<?php

namespace App\Http\Controllers\Admin;

use App\Cities;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CitiesController extends Controller
{
    public function index()
    {
        $cities=Cities::orderBy('id','DESC')->paginate(10);
        return view('admin.cities.index',['cities'=>$cities]);
    }

    public function create()
    {
        $locales =  \Config::get('app.locales');

        return view('admin.cities.form',['locales'=>$locales]);
    }

    public function store(Request $request)
    {
        $data = $request->only('name_ru','name_ua','name_en','name_de');
        $cities = Cities::create($data);
        return redirect('admin/cities')->with('message','Город добавлен');
    }

    public function edit($id)
    {
        $record=Cities::findOrFail($id);
        $locales =  \Config::get('app.locales');

        return view('admin.cities.form',['record'=>$record,'locales'=>$locales]);
    }


    public function update(Request $request,$id)
    {
        $cities=Cities::findOrFail($id);
        $data = $request->only('name_ru','name_ua','name_en','name_de');
        $cities->update($data);
        return back()->with('message', 'Город изменен');
    }
    public function destroy($id)
    {
        $cities=Cities::findOrFail($id);
        $cities->delete();
        return redirect('admin/cities')->with('message','Город удален');
    }

}
