<?php

namespace App\Http\Controllers\Admin;

use App\Children;
use App\Cities;
use App\Album;
use App\Photos;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use DataTables;
class ChildrenController extends Controller
{

    public function ajax() {
        $query = Children::query();
        return Datatables::of($query)
            ->rawColumns(['edit','img','name','collected_flag','public']) // set columsn to render html
            ->addColumn('id', function($row) { return $row->id; })
            ->addColumn('img', function($row) { return '<img width=20 height=20 src="'.$row->preview_photo.'">' ; })
            ->addColumn('name', function($row) { return $row->name_ru; })
            ->addColumn('need', function($row) { return $row->sum_need; })
            ->addColumn('alreadycount', function($row) { return $row->collected_money_count; })
            ->addColumn('already', function($row) { return $row->collected_money; })
            ->addColumn('collected_flag', function($row) { return $row->collected_flag ? "<a href='?collected=1'><i style='color:green;' class='fa fa-check'><div style='display:none;'>1</div></a>" : "<a href='?collected=0'><i style='color:red;' class='fa fa-times'><div style='display:none;'>0</div></a>"; })
            ->addColumn('public', function($row) { return $row->public ? "<a href='?public=1'><i style='color:green;' class='fa fa-check'><div style='display:none;'>1</div></a>" : "<a href='?public=0'><i style='color:red;' class='fa fa-times'><div style='display:none;'>0</div></a>"; })
            ->addColumn('edit', function($row) { return '<a href="/admin/children/'.$row->id.'/edit"><i class="fa fa-pencil"></i></a> &nbsp; &nbsp;
                         <form style="display:none;"  method="POST" action="'.action('Admin\ChildrenController@destroy',['articles'=>$row->id]).'">
                             <input type="hidden" name="_method" value="delete"/>
                             <input type="hidden" name="_token" value="'.csrf_token().'"/>
                             <button type="submit"><i class="fa fa-trash"></i></button>
                         </form>'; })
            ->make(true);
    }

    public function index(Request $request)
    {
        $data = $request->only('collected','public');
        if (isset($data['collected'])) {
            $children=Children::where([['collected_flag', '=', $data['collected']]])->orderBy('id','DESC')->paginate(10);
        } elseif (isset($data['public'])) {
                $children=Children::where([['public', '=', $data['public']]])->orderBy('id','DESC')->paginate(10);
        } else {
            $children=Children::orderBy('id','DESC')->paginate(10);

        }

        return view('admin.children.index',['children'=>$children]);
    }

    public function create()
    {
        $locales =  \Config::get('app.locales');

        $albums = Album::pluck('title','id');
        $cities = Cities::pluck('name_ru','id');
        return view('admin.children.form',['albums'=>$albums, 'cities'=>$cities, 'locales' => $locales]);
    }

    public function store(Request $request)
    {
        $data = $request->only('name_ru');
        $data['public'] = 0 ;
        $data['user_id']=\Auth::id();
        $album['title'] =  $data['name_ru'];
       // var_dump( Album::create($album)->id); die();

        $data['album_id'] = Album::create($album)->id;
        $child_id = Children::create($data)->id;
        return redirect('admin/children/'.$child_id.'/edit');
    }

    public function edit($id)
    {

        $locales =  \Config::get('app.locales');
        $record=Children::findOrFail($id);
        $albums = Album::pluck('title','id');
        $cities = Cities::pluck('name_ru','id');

        return view('admin.children.form',['record'=>$record,'albums'=>$albums, 'cities'=>$cities, 'locales' => $locales]);
    }

    public function update(Request $request,$id)
    {
        $child=Children::findOrFail($id);
        $data = $request->only('preview','name_ru','name_ua','name_en','name_de','city_id','born','album_id','sum_need','details_ru','details_ua','details_en','details_de','public','images','collected_flag','documents');


        if (!(\Request::has('images'))) {
            $data['images'] =[];
        }
        if (!(\Request::has('documents'))) {
            $data['documents'] =[];
        }

        if(\Request::has('collected_flag')){
            $data['collected_flag'] =1;
        } else {
            $data['collected_flag'] =0;
        }

        $child->update($data);


        return back()->with('message', 'Страница изменена');
    }
    public function destroy($id)
    {
        $news=Children::findOrFail($id);
        $news->delete();
        return redirect('admin/children')->with('message','Страница удалена');
    }
}
