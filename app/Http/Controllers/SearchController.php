<?php

namespace App\Http\Controllers;

use App\Children;
use App\Specinfo;
use App\Staff;
use App\Stocks;
use App\News;
use Illuminate\Http\Request;
use App\Search;
class SearchController extends Controller
{
    public function search (Request $request) {
        $result = array();
        if ($request->has('text')) {
            $data = $request->only('text');
            if($request->ajax()) {
                $list = Search::searchAll($data['text'])->paginate(30);
            } else {
                $list = Search::searchAll($data['text'])->paginate(10);
            }

            foreach ($list as $item) {
                switch ($item->type) {
                    case ('children') : $child = Children::findorfail($item->id);
                                        $one['title'] = $child->name;
                                        $one['text']=$child->short;
                                        $one['img']=$child->preview_photo;
                                        $one['link']=$child->link;
                                        array_push ($result, $one);
                                        break;
                    case ('news') :     $news = News::findorfail($item->id);
                                        $one['title'] = $news->name;
                                        $one['text']=$news->short;
                                        $one['img']=$news->preview;
                                        $one['link']=$news->link;
                                        array_push ($result, $one);
                                        break;
                    case ('stocks') :   $stocks = Stocks::findorfail($item->id);
                                        $one['title'] = $stocks->name;
                                        $one['text']=$stocks->short;
                                        $one['img']=$stocks->preview;
                                        $one['link']=$stocks->link;
                                        array_push ($result, $one);
                                        break;
                    case('specinfo') :  $specinfo = Specinfo::findorfail($item->id);
                                        $one['title'] = $specinfo->title;
                                        $one['text']=$specinfo->short;
                                        $one['img']=$specinfo->preview;
                                        $one['link']=$specinfo->link;
                                        array_push ($result, $one);
                                        break;
                    case('staff') :     $staff = Staff::findorfail($item->id);
                                        $one['title'] = $staff->name;
                                        $one['text']=$staff->position;
                                        $one['img']=$staff->staff_preview;
                                        $one['link']=$staff->link;
                                        array_push ($result, $one);
                                        break;

                }
            }

        } else {
            $data['text'] = '';
        }

        $count = $list->total();

        if($request->ajax()) {
            return response()->json($result);
        } else {
            return view('pages.search',['text'=>  $data['text'], 'count' => $count,'result'=>$result,'list'=>$list]);
        }
    }
}
