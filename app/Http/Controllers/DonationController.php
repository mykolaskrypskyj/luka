<?php

namespace App\Http\Controllers;

use App\Children;
use App\Donate;
use App\Options;
use Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
class DonationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $record = Children::findOrFail($id);
        $payment = Options::getSettings('payment');
        $input = $request::all();
        \Log::info(print_r($payment,true));
        return view('donation.index',['child'=>$record, 'payment' => $payment, 'input' =>$input  ]);
    }

    public function donatesuccess (Request $request, $id) {
        $data = $request::only('data','signature');
        $donate = Donate::findorfail( $id);
        $donate['status'] = 1;

        $sign = base64_encode( sha1( Options::getKeyValue('liqpay','private_key') .
            $data['data'] .
            Options::getKeyValue('liqpay','private_key')
            , 1 ));
        $child = Children::findorfail($donate->children_id);

        if ($sign == $data['signature'] ) {
            $donate->update();

            Mail::send(array(),array(), function($message) use ($child)
            {

                $message->subject('Получено пожертвования на Liqpay');
                $message->setBody('Полученно пожертвование для '.$child->name_ru.' с канала Liqpay.');
                $message->to(\App\Options::getKeyValue('notification_email','email_child_notification'));
            });



            if ($child->collected_money >= $child->sum_need) {
                $flag = [
                    'collected_flag' =>1 ,
                    'public' => 0
                ];

                Mail::send(array(),array(), function($message) use ($child)
                {

                    $message->subject('Сума для ребенка собрана');
                    $message->setBody('Сума для '.$child->name_ru.' собрана. Вы можете отредактировать контент перейдя по ссылке: \n '.URL::to('/').'/admin/children/'.$child->id.'/edit');
                    $message->to(\App\Options::getKeyValue('notification_email','email_child_notification'));
                });

                $child->update($flag);
            }
        }
    }

    public function thankyou ()
    {
        return view('pages.thankyou');
    }
    public function test ()
    {
        Mail::raw('Сума для TEST собрана', function($message)
        {
            $message->subject('Сума для ребенка собрана');

            $message->to(\App\Options::getKeyValue('notification_email','email_child_notification'));
        });
        return view('pages.thankyou');
    }

}
