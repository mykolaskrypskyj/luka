<?php

namespace App\Http\Controllers;

use App\Specinfo;
use Illuminate\Http\Request;

class SpecinfoController extends Controller
{
    public function index()
    {
        $specinfo=Specinfo::where([['public','=',1]])->orderBy('id','DESC')->paginate(6);
        return view('specinfo.index',['specinfo'=>$specinfo]);
    }

    public function show($id)
    {
        $specinfo=Specinfo::findOrFail($id);
        return view('specinfo.show',['specinfo'=>$specinfo]);
    }
}
