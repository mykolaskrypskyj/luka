<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    protected $table="cities";
    protected $fillable=['name_ru','name_ua','name_en','name_de'];


    public function getNameAttribute(){
        $locale = \App::getLocale();
        $name = 'name_'.$locale;
        return $this->$name;
    }
}
