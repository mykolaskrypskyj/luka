<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsors extends Model
{
    protected $table="sponsors";
    protected $fillable=['title','photo_id','link','public','user_id','photo'];
    public function photo() {
        return $this->belongsTo('App\Photos', 'photo_id');
    }

    public function getPreviewAttribute(){
        $photo = $this->photo;
        if ($photo) {
            return $photo->icon_url;
        }
        return '/images/no-image.png';
    }
    public function getSponsorPreviewAttribute(){
        $photo = $this->photo;
        if ($photo) {
            return $photo->icon_url_sponsor_preview;
        }
        return '/images/no-image.png';
    }

    public function setPhotoAttribute($value){
            $image = Photos::findOrFail($value);
            $image->createSponsorPrewIcon('sponsor_preview',140,108);
            $this->attributes['photo_id'] = $value;



    }
}
