<?php

namespace App;

use Event;
use Illuminate\Database\Eloquent\Model;

class Specinfo extends Model
{
    protected $table="specinfo";
    protected $fillable=['title_ru','title_ua','title_en','title_de','video_ru','video_ua','video_en','video_de','details_ru','details_ua','details_en','details_de','public','user_id'];

    public static function boot()

    {

        parent::boot();

        static::created(function($specinfo){

            Event::fire('specinfo.created',$specinfo);

        });

        static::updated(function($specinfo){

            Event::fire('specinfo.updated',$specinfo);

        });

        static::deleted(function($specinfo){

            Event::fire('specinfo.deleted',$specinfo);

        });

    }

    public function getTitleAttribute(){
        $locale = \App::getLocale();
        $title = 'title_'.$locale;
        return $this->$title;
    }

    public function getDetailsAttribute(){
        $locale = \App::getLocale();
        $details = 'details_'.$locale;
        return $this->$details;
    }
    public function getLinkAttribute(){
        return '/specinfo/'.$this->id.'/'.$this->slug;
    }
    public function getVideoAttribute(){
        $locale = \App::getLocale();
        $video = 'video_'.$locale;
        return $this->$video;
    }

    public function getPreviewAttribute(){
        return '/images/no-image.png';
    }
    public function getShortAttribute() {
        return mb_substr(strip_tags($this->details), 0, 170);
    }

    public function getSlugAttribute(){
        $text = $this->title_ru;

        $trans = [
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'jo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'jj',
            'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f',
            'х' => 'kh', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'eh', 'ю' => 'ju', 'я' => 'ja',
        ];
        $text  = mb_strtolower( $text, 'UTF-8' ); // lowercase cyrillic letters too
        $text  = strtr( $text, $trans ); // transliterate cyrillic letters
        $text  = preg_replace( '/[^A-Za-z0-9 _.]/', '', $text );
        $text  = preg_replace( '/[ _.]+/', '-', trim( $text ) );
        $text  = trim( $text, '-' );
        return $text;
    }

}
