<?php

namespace App\Events;

use App\Search;
use App\Staff;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class StaffEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }


    public function StaffCreated(Staff $staff) {
        $data= array();
        $data['obj_id']=$staff->id;
        $data['obj_type']='staff';
        $data['public']=$staff->public;
        $data['priority']=5;

        $data['obj_content'] = $staff->name_ru." : ".$staff->details_ru." : ".$staff->position_ru." : ".$staff->name_ua." : ".$staff->details_ua." : ".$staff->position_ua." : ".$staff->name_en." : ".$staff->details_en." : ".$staff->position_en." : ".$staff->name_de." : ".$staff->details_de." : ".$staff->position_de." : ";
        $search = Search::create($data);
    }

    public function StaffUpdated(Staff $staff) {
        $search =Search::where([['obj_id','=', $staff->id],['obj_type','=','staff']])->first();
        $data= array();
        $data['obj_id']=$staff->id;
        $data['obj_type']='staff';
        $data['public']=$staff->public;
        $data['priority']=5;

        $data['obj_content'] = $staff->name_ru." : ".$staff->details_ru." : ".$staff->position_ru." : ".$staff->name_ua." : ".$staff->details_ua." : ".$staff->position_ua." : ".$staff->name_en." : ".$staff->details_en." : ".$staff->position_en." : ".$staff->name_de." : ".$staff->details_de." : ".$staff->position_de." : ";
        if ($search != null) {
            $search->update($data);
        } else {
            $search = Search::create($data);

        }
    }

    public function StaffDeleted(Staff $staff) {
        $search =Search::where([['obj_id','=', $staff->id],['obj_type','=','staff']])->first();
        $search->delete();
    }
}
