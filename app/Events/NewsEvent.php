<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\News;
use App\Search;

class NewsEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function NewsCreated(News $news) {
        $data= array();
        $data['obj_id']=$news->id;
        $data['obj_type']='news';
        $data['public']=$news->public;
        $data['priority']=2;
        $data['obj_content'] = $news->title_ru." : ".$news->content_ru." : ".$news->title_ua." : ".$news->content_ua." : ".$news->title_en." : ".$news->content_en." : ".$news->title_de." : ".$news->content_de." : ";
        $search = Search::create($data);
    }

    public function NewsUpdated(News $news) {
        $search =Search::where([['obj_id','=', $news->id],['obj_type','=','news']])->first();
//       var_dump($search); die();
        $data= array();
        $data['obj_id']=$news->id;
        $data['obj_type']='news';
        $data['public']=$news->public;
        $data['priority']=2;

        $data['obj_content'] = $news->title_ru." : ".$news->content_ru." : ".$news->title_ua." : ".$news->content_ua." : ".$news->title_en." : ".$news->content_en." : ".$news->title_de." : ".$news->content_de." : ";
        if ($search != null) {
            $search->update($data);
        } else {
            $search = Search::create($data);

        }
    }

    public function NewsDeleted(News $news) {
        $search =Search::where([['obj_id','=', $news->id],['obj_type','=','news']])->first();
        $search->delete();
    }




}
