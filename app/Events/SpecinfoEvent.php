<?php

namespace App\Events;

use App\Search;
use App\Specinfo;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SpecinfoEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }


    public function SpecinfoCreated(Specinfo $specinfo) {
        $data= array();
        $data['obj_id']=$specinfo->id;
        $data['obj_type']='specinfo';
        $data['public']=$specinfo->public;
        $data['priority']=4;

        $data['obj_content'] = $specinfo->title_ru." : ".$specinfo->details_ru." : ".$specinfo->title_ua." : ".$specinfo->details_ua." : ".$specinfo->title_en." : ".$specinfo->details_en." : ".$specinfo->title_de." : ".$specinfo->details_de." : ";
        $search = Search::create($data);
    }

    public function SpecinfoUpdated(Specinfo $specinfo) {
        $search =Search::where([['obj_id','=', $specinfo->id],['obj_type','=','specinfo']])->first();
        $data= array();
        $data['obj_id']=$specinfo->id;
        $data['obj_type']='specinfo';
        $data['public']=$specinfo->public;
        $data['priority']=4;

        $data['obj_content'] = $specinfo->title_ru." : ".$specinfo->details_ru." : ".$specinfo->title_ua." : ".$specinfo->details_ua." : ".$specinfo->title_en." : ".$specinfo->details_en." : ".$specinfo->title_de." : ".$specinfo->details_de." : ";
        if ($search != null) {
            $search->update($data);
        } else {
            $search = Search::create($data);

        }
    }

    public function SpecinfoDeleted(Specinfo $specinfo) {
        $search =Search::where([['obj_id','=', $specinfo->id],['obj_type','=','specinfo']])->first();
        $search->delete();
    }



}
