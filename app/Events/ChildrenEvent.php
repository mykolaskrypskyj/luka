<?php

namespace App\Events;

use App\Children;
use App\Search;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChildrenEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }


    public function ChildrenCreated(Children $children) {
        $data= array();
        $data['obj_id']=$children->id;
        $data['obj_type']='children';
        $data['public']=$children->public;
        $data['priority']=1;

        $data['obj_content'] = $children->name_ru." : ".$children->details_ru." : ".$children->name_ua." : ".$children->details_ua." : ".$children->name_en." : ".$children->details_en." : ".$children->name_de." : ".$children->details_de;
        $search = Search::create($data);
    }

    public function ChildrenUpdated(Children $children) {
        $search =Search::where([['obj_id','=', $children->id],['obj_type','=','children']])->first();
        $data= array();
        $data['obj_id']=$children->id;
        $data['obj_type']='children';
        $data['public']=$children->public;
        $data['priority']=1;

        $data['obj_content'] = $children->name_ru." : ".$children->details_ru." : ".$children->name_ua." : ".$children->details_ua." : ".$children->name_en." : ".$children->details_en." : ".$children->name_de." : ".$children->details_de;
        if ($search != null) {
            $search->update($data);
        } else {
            $search = Search::create($data);

        }
    }

    public function ChildrenDeleted(Children $children) {
        $search =Search::where([['obj_id','=', $children->id],['obj_type','=','children']])->first();
        $search->delete();
    }
}
