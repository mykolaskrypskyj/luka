<?php

namespace App\Events;

use App\Search;
use App\Stocks;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class StocksEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }


    public function StocksCreated(Stocks $stocks) {
        $data= array();
        $data['obj_id']=$stocks->id;
        $data['obj_type']='stocks';
        $data['public']=$stocks->public;
        $data['priority']=3;

        $data['obj_content'] = $stocks->title_ru." : ".$stocks->content_ru." : ".$stocks->title_ua." : ".$stocks->content_ua." : ".$stocks->title_en." : ".$stocks->content_en." : ".$stocks->title_de." : ".$stocks->content_de." : ";
        $search = Search::create($data);
    }

    public function StocksUpdated(Stocks $stocks) {
        $search =Search::where([['obj_id','=', $stocks->id],['obj_type','=','stocks']])->first();
        $data= array();
        $data['obj_id']=$stocks->id;
        $data['obj_type']='stocks';
        $data['public']=$stocks->public;
        $data['priority']=3;

        $data['obj_content'] = $stocks->title_ru." : ".$stocks->content_ru." : ".$stocks->title_ua." : ".$stocks->content_ua." : ".$stocks->title_en." : ".$stocks->content_en." : ".$stocks->title_de." : ".$stocks->content_de." : ";
        if ($search != null) {
            $search->update($data);
        } else {
            $search = Search::create($data);

        }
    }

    public function StocksDeleted(Stocks $stocks) {
        $search =Search::where([['obj_id','=', $stocks->id],['obj_type','=','stocks']])->first();
        $search->delete();
    }


}
