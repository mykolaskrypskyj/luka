<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Videos extends Model
{
    protected $table="videos";
    protected $fillable=['title','video_ru','video_ua','video_en','video_de','public','user_id'];

    public function getVideoAttribute(){
        $locale = \App::getLocale();
        $name = 'video_'.$locale;
        return $this->$name;
    }
}
