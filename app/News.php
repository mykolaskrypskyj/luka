<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Event;

class News extends Model
{
    protected $table="news";
    protected $fillable=['preview','preview_id','title_ru','content_ru','title_ua','content_ua','title_en','content_en','title_de','content_de','public','user_id','images'];


    public static function boot()

    {

        parent::boot();

        static::created(function($news){

            Event::fire('news.created',$news);

        });

        static::updated(function($news){

            Event::fire('news.updated',$news);

        });

        static::deleted(function($news){

            Event::fire('news.deleted',$news);

        });

    }

    public function previewph() {
        return $this->belongsTo('App\Photos', 'preview_id');
    }

    public function getPreviewAttribute(){
        $photo = $this->photos()->first();
        if ($photo) {
            return $photo->icon_url;
        }
        return '/images/no-image.png';
    }

    public function getPreviewPhotoAttribute()
    {
        $photo = $this->previewph;
        if ($photo) {
            return $photo->icon_url_news_preview;
        }
        return '/images/no-image.png';
    }

    public function setImagesAttribute($value){

        foreach ($value as $image_id) {
            $image = Photos::findOrFail($image_id);
//            $image->createPrewIcon('news_preview',278,120);
            $image->createPrewIcon('news_inside',900,350);
        }
        $this->photos()->detach();
        $this->photos()->attach($value);

    }

    public function setPreviewAttribute($value){
        if (isset($value['id']))  {
            $image = Photos::findOrFail($value['id']);
            $image->createPreviewCropIcon('news_preview',$value['detail_x'],$value['detail_y'],$value['detail_height'],$value['detail_width']);
            $this->attributes['preview_id'] = $value['id'];
        } else {
            $this->attributes['preview_id'] = null;
        }
    }


    public function getShortAttribute() {
        return mb_substr(strip_tags($this->details), 0, 170);
    }

    public function photos() {
        return $this->belongsToMany('App\Photos');
    }
    public function getLinkAttribute(){
        return '/news/'.$this->id.'/'.$this->slug;
    }

    public function getNameAttribute(){
        $locale = \App::getLocale();
        $name = 'title_'.$locale;
        return $this->$name;
    }

    public function getDetailsAttribute(){
        $locale = \App::getLocale();
        $details = 'content_'.$locale;
        return $this->$details;
    }

    public function getPhotoPreviewAttribute(){
        $photo = $this->photos()->first();
        if ($photo) {
            return $photo->icon_url_news_preview;
        }
        return '/images/no-image.png';
    }
    public function getPhotoInsideAttribute(){
        $photo = $this->photos()->first();
        if ($photo) {
            return $photo->icon_url_news_inside;
        }
        return '/images/no-image.png';
    }

    public function getSlugAttribute(){
        $text = $this->title_ru;

        $trans = [
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'jo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'jj',
            'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f',
            'х' => 'kh', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'eh', 'ю' => 'ju', 'я' => 'ja',
        ];
        $text  = mb_strtolower( $text, 'UTF-8' ); // lowercase cyrillic letters too
        $text  = strtr( $text, $trans ); // transliterate cyrillic letters
        $text  = preg_replace( '/[^A-Za-z0-9 _.]/', '', $text );
        $text  = preg_replace( '/[ _.]+/', '-', trim( $text ) );
        $text  = trim( $text, '-' );
        return $text;
    }

}
