<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Images;

class Album extends Model
{
    protected $table="albums";
    protected $fillable=['title'];
    
    public function getIconUrlAttribute(){
        if ($photo = $this->photos()->first()) {
            return $photo->icon_url;
        }
        return '/images/no-image.png';
    }    

    public function photos(){
        return $this->hasMany('App\Photos','album_id');
    }

    public function getlist() {
        $list = array();
        $count = 0;
        $photos = $this->photos() ;
        while ($count<$this->photos()->count()) {

                $list[$count]['id'] =  $photos->get($count)->id;
                $list[$count]['url'] =  $photos->get($count)->icon_url;
                $count++;
            }
        return $list;
    }
}
