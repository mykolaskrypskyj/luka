<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $table="pages";
    protected $fillable=['type','title_ru','content_ru','title_ua','content_ua','title_en','content_en','title_de','content_de','public','photo_id','user_id'];

    public function getfields() {
        switch ($this->type) {
            case ('takehelp') :
                return
                    [
                        ['title'=> 'Текст перед файлом', 'name'=>'text_before_file','type' =>'textarea'],
                        ['title'=> 'Анкета файл', 'name'=>'file','type' =>'file'],
                        ['title'=> 'Текст после файла', 'name'=>'text_after_file','type' =>'textarea']
                    ]; break;
            case ('team') :
                return
                    [
                        ['title'=> 'Текст', 'name'=>'text','type' =>'textarea'],
                    ]; break;

            case ('programs') :
                return
                    [
                        ['title'=> 'Текст', 'name'=>'text','type' =>'textarea'],
                    ]; break;

            case ('smshelp') :
                return
                    [
                        ['title'=> 'Текст', 'name'=>'text','type' =>'textarea'],
                    ]; break;

        }
    }

    public function photo() {
        return $this->belongsTo('App\Photos', 'photo_id');
    }

    public function getPreviewAttribute(){
//        $photo = $this->hasOne('App\Photos','id','photo_id')->first();
        $photo = $this->photo;
        if ($photo) {
            return $photo->icon_url;
        }
        return '/images/no-image.png';
    }

    public function getImageUrlAttribute(){
        $photo = $this->photo;
        if ($photo) {
            return $photo->url;
        }
        return '/images/no-image.png';
    }



    public function getTitleAttribute(){
        $locale = \App::getLocale();
        $name = 'title_'.$locale;
        return $this->$name;
    }

    public function getContentAttribute(){
        $locale = \App::getLocale();
        $content = 'content_'.$locale;
        $result = $this->$content;
        return json_decode($result);
    }
}
