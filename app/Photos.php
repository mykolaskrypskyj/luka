<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photos extends Model
{
    protected $table="photos";
    protected $fillable=['title'];
    public function getTypeAttribute(){
        switch($this->mime){
            case 'image/jpeg': $ret='jpg'; break;
            case 'image/gif': $ret='gif'; break;
            case 'image/png': $ret='png'; break;
            default: $ret='';
        }
        return $ret;
    }
    public function getUrlAttribute(){
        return '/images/album/'.$this->album_id.'/'.$this->id.'.'.$this->type;
    }
    public function getIconUrlAttribute(){
        return '/images/album/'.$this->album_id.'/'.$this->id.'_icon.png';
    }

    public function getIconUrlChildrenPreviewAttribute(){
        return '/images/album/'.$this->album_id.'/'.$this->id.'_children_preview.png';
    }
    public function getIconUrlChildrenInsideAttribute(){
        return '/images/album/'.$this->album_id.'/'.$this->id.'_children_inside.png';
    }
    public function getIconUrlNewsPreviewAttribute(){
        return '/images/album/'.$this->album_id.'/'.$this->id.'_news_preview.png';
    }
    public function getIconUrlNewsInsideAttribute(){
        return '/images/album/'.$this->album_id.'/'.$this->id.'_news_inside.png';
    }
    public function getIconUrlStaffPreviewAttribute(){
        return '/images/album/'.$this->album_id.'/'.$this->id.'_staff_preview.png';
    }
    public function getIconUrlStaffInsideAttribute(){
        return '/images/album/'.$this->album_id.'/'.$this->id.'_staff_inside.png';
    }
    public function getIconUrlSponsorPreviewAttribute(){
        return '/images/album/'.$this->album_id.'/'.$this->id.'_sponsor_preview.png';
    }
    public static function getPath($id) {
        $destinationPath='images/album/'.$id.'/';
        if(!file_exists($destinationPath)) mkdir($destinationPath, 0755, true);
        return $destinationPath;
    }
    public static function add($request) {
        
        if(!$request->hasFile('file')) return;
            $file=new self();
            $file->album_id = $request->get('album_id');
            $file->user_id = \Auth::id();
            $file->name = $request->file('file')->getClientOriginalName();
            $file->mime = $request->file('file')->getMimeType();
            $file->save();
            
            $path = self::getPath($file->album_id);
            $name = $file->id.'.'.$file->type;
            
            $tmpName = $request->file('file')->getRealPath();
            
            
            $request->file('file')->move($path, $name);
            
            $file->createIcon();
            return $file;
    }
    
    
    public function createIcon($w=190,$h=190){
        $this->createPrewIcon('icon',$w,$h);
        return $this;
    }

    public function createPreviewCropIcon($type,$w,$h,$height,$width){
        $field = 'icon_url_'.$type;
        if(file_exists(substr($this->$field,1)))   { @unlink(substr($this->$field, 1)); }

        $path = self::getPath($this->album_id);
        $filename=$this->id.'_'.$type.'.png';

        if(file_exists($path . $filename)) return $this;

        $img = \Image::make( substr($this->url, 1) )
            ->crop($width, $height, $w,$h)
            ->save($path . $filename, 90);
//            ->encode('png');


        return $this;
    }

    public function createInsideIcon(){
        $this->createPrewIcon('children_inside',350,350);
        return $this;
    }


    public function createPrewIcon($type,$w,$h){
        $path = self::getPath($this->album_id);
        $filename=$this->id.'_'.$type.'.png';

        if(file_exists($path . $filename)) return $this;

        $img = \Image::make( substr($this->url, 1) )
            ->resize($w, $h, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->resizeCanvas($w,$h,'center', false, 'ffffff')
            ->save($path . $filename, 90)
            ->encode('png');
        return $this;
    }

    public function createSponsorPrewIcon($type,$w,$h){
        $path = self::getPath($this->album_id);
        $filename=$this->id.'_'.$type.'.png';

        if(file_exists($path . $filename)) return $this;

        $img = \Image::make( substr($this->url, 1) )
            ->resize($w, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        $img->save($path . $filename, 90)
            ->encode('png');
        return $this;
    }

    
    public function del(){
        $this->clearFiles()->delete();
    }
    
    
    public function clearFiles(){
        if(file_exists(substr($this->url,1)))                         { @unlink(substr($this->url, 1)); }
        if(file_exists(substr($this->icon_url,1)))                    { @unlink(substr($this->icon_url, 1));}
        if(file_exists(substr($this->icon_url_children_preview,1)))   { @unlink(substr($this->icon_url_children_preview, 1)); }
        if(file_exists(substr($this->icon_url_children_inside,1)))    { @unlink(substr($this->icon_url_children_inside, 1)); }
        if(file_exists(substr($this->icon_url_news_preview,1)))       { @unlink(substr($this->icon_url_news_preview, 1)); }
        if(file_exists(substr($this->icon_url_news_inside,1)))        { @unlink(substr($this->icon_url_news_inside, 1)); }
        if(file_exists(substr($this->icon_url_staff_preview,1)))      { @unlink(substr($this->icon_url_staff_preview, 1)); }
        if(file_exists(substr($this->icon_url_staff_inside,1)))       { @unlink(substr($this->icon_url_staff_inside, 1)); }
        if(file_exists(substr($this->icon_url_sponsor_preview,1)))    { @unlink(substr($this->icon_url_sponsor_preview, 1)); }

        return $this;
    }
    public function getProperties() {
        return array(
            'id' => $this->id,
            'name' => $this->name,
            'url' => $this->url,
            'thumb_url' => $this->icon_url,
            'mime' => $this->mime,
            'album_id' => $this->album_id,
        );        
    }       
    
}
