<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
        'news.created' => [
            'App\Events\NewsEvent@NewsCreated',
        ],
        'news.updated' => [
            'App\Events\NewsEvent@NewsUpdated',
        ],
        'news.deleted' => [
            'App\Events\StocksEvent@NewsDeleted',
        ],
        'stocks.created' => [
            'App\Events\StocksEvent@StocksCreated',
        ],
        'stocks.updated' => [
            'App\Events\StocksEvent@StocksUpdated',
        ],
        'stocks.deleted' => [
            'App\Events\StocksEvent@StocksDeleted',
        ],
        'specinfo.created' => [
            'App\Events\SpecinfoEvent@SpecinfoCreated',
        ],
        'specinfo.updated' => [
            'App\Events\SpecinfoEvent@SpecinfoUpdated',
        ],
        'specinfo.deleted' => [
            'App\Events\SpecinfoEvent@SpecinfoDeleted',
        ],
        'staff.created' => [
            'App\Events\StaffEvent@StaffCreated',
        ],
        'staff.updated' => [
            'App\Events\StaffEvent@StaffUpdated',
        ],
        'staff.deleted' => [
            'App\Events\StaffEvent@StaffDeleted',
        ],
        'children.created' => [
            'App\Events\ChildrenEvent@ChildrenCreated',
        ],
        'children.updated' => [
            'App\Events\ChildrenEvent@ChildrenUpdated',
        ],
        'children.deleted' => [
            'App\Events\ChildrenEvent@ChildrenDeleted',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
