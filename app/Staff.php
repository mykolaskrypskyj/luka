<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Event;

class Staff extends Model
{
    protected $table="staff";
    protected $fillable=['preview', 'name_ru','details_ru','position_ru','name_ua','details_ua','position_ua','name_en','details_en','position_en','name_de','details_de','position_de','telephon','email','public','photo_id','user_id','photo'];

    public static function boot()

    {

        parent::boot();

        static::created(function($staff){

            Event::fire('staff.created',$staff);

        });

        static::updated(function($staff){

            Event::fire('staff.updated',$staff);

        });

        static::deleted(function($staff){

            Event::fire('staff.deleted',$staff);

        });

    }

    public function photo() {
        return $this->belongsTo('App\Photos', 'photo_id');
    }

    public function getPreviewAttribute(){
        $photo = $this->photo;
        if ($photo) {
            return $photo->icon_url;
        }
        return '/images/no-image.png';
    }
    public function getShortAttribute() {
        return mb_substr(strip_tags($this->details), 0, 170);
    }
    public function getLinkAttribute(){
        return '/staff/'.$this->id.'/'.$this->slug;
    }
    public function getNameAttribute(){
        $locale = \App::getLocale();
        $name = 'name_'.$locale;
        return $this->$name;
    }
    public function getPositionAttribute(){
        $locale = \App::getLocale();
        $position = 'position_'.$locale;
        return $this->$position;
    }

    public function getDetailsAttribute(){
        $locale = \App::getLocale();
        $details = 'details_'.$locale;
        return $this->$details;
    }

    public function getStaffPreviewAttribute(){
        $photo = $this->photo;
        if ($photo) {
            return $photo->icon_url_staff_preview;
        }
        return '/images/no-image.png';
    }

    public function getStaffInsideAttribute(){
        $photo = $this->photo;
        if ($photo) {
            return $photo->icon_url_staff_inside;
        }
        return '/images/no-image.png';
    }

    public function setPhotoAttribute($value){
        if (isset($value['id']))  {
            $image = Photos::findOrFail($value['photo_id']);
            $image->createPreviewCropIcon('staff_preview',$value['detail_x'],$value['detail_y'],$value['detail_height'],$value['detail_width']);
            $image->createPrewIcon('staff_inside',350,350);
            $this->attributes['photo_id'] = $value['photo_id'];
        } else {
            $this->attributes['preview_id'] = null;
        }
    }

    public function getSlugAttribute(){
        $text = $this->name_ru;

        $trans = [
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'jo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'jj',
            'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f',
            'х' => 'kh', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'eh', 'ю' => 'ju', 'я' => 'ja',
        ];
        $text  = mb_strtolower( $text, 'UTF-8' ); // lowercase cyrillic letters too
        $text  = strtr( $text, $trans ); // transliterate cyrillic letters
        $text  = preg_replace( '/[^A-Za-z0-9 _.]/', '', $text );
        $text  = preg_replace( '/[ _.]+/', '-', trim( $text ) );
        $text  = trim( $text, '-' );
        return $text;
    }
}
