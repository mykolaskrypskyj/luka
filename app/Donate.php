<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donate extends Model
{
    protected $table="donate";
    protected $fillable=['sum','children_id','channel','status'];


    public function getChildAttribute(){
        $child = $this->hasOne('App\Children','id','children_id')->first();
        return $child->name_ru;

    }
    public static function getAllMoney(){
        $all = Donate::where([['status','=','1']])->sum('sum');
        return $all;

    }

    public static function getCurentYearMoney(){
        $all = Donate::whereYear('created_at',date('Y'))->where([['status','=','1']])->sum('sum');
        return $all;

    }
}
