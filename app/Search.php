<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;


class Search extends Model
{
    protected $table="search";
    protected $fillable=['obj_id','obj_type','obj_content','public','priority'];

    public static function searchChildren ($text) {
        $result = Children::where([['name_ru', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['name_ua', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['name_en', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['name_de', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['details_ru', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['details_ua', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['details_en', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['details_de', 'like', '%' .$text . '%' ],['public','=','1']])
            ->get();

        return $result;
    }

    public static function searchNews ($text) {
        $result = News::where([['title_ru', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['title_ua', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['title_en', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['title_de', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['content_ru', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['content_ua', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['content_en', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['content_de', 'like', '%' .$text . '%' ],['public','=','1']])
            ->get();

        return $result;
    }

    public static function searchStocks ($text) {
        $result = Stocks::where([['title_ru', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['title_ua', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['title_en', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['title_de', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['content_ru', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['content_ua', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['content_en', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['content_de', 'like', '%' .$text . '%' ],['public','=','1']])
            ->get();

        return $result;
    }

    public static function searchSpecinfo ($text) {
        $result = Specinfo::where([['title_ru', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['title_ua', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['title_en', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['title_de', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['details_ru', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['details_ua', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['details_en', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['details_de', 'like', '%' .$text . '%' ],['public','=','1']])
            ->get();

        return $result;
    }
    public static function searchStaff ($text) {
        $result = Staff::where([['name_ru', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['name_ua', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['name_en', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['name_de', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['position_ru', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['position_ua', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['position_en', 'like', '%' .$text . '%' ],['public','=','1']])
            ->orWhere([['position_de', 'like', '%' .$text . '%' ],['public','=','1']])
            ->get();

        return $result;
    }

    public static function searchAll ($text)
    {
        if ($text == null) {
            $result = Search::select('obj_id as id','obj_type as type')
                ->where([['obj_content', 'IN', '1' ],['public','=','1']])
                ->orderBy('priority','ASC');
            return $result;
        } else {
            $result = Search::select('obj_id as id','obj_type as type')
                ->where([['obj_content', 'like', '%' .$text . '%' ],['public','=','1']])
                ->orderBy('priority','ASC');
    //            ->get();
            return $result;
        }
    }


}