<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Event;

class Children extends Model
{
    protected $table="children";
    protected $fillable=['preview','preview_id','name_ru','name_ua','name_en','name_de','city_id','album_id','sum_need','born','details_ru','details_ua','details_en','details_de','collected_flag','public','user_id','images','documents'];

    public static function boot()

    {

        parent::boot();

        static::created(function($children){

            Event::fire('children.created',$children);

        });

        static::updated(function($children){

            Event::fire('children.updated',$children);

        });

        static::deleted(function($children){

            Event::fire('children.deleted',$children);

        });

    }


    public function getDates(){
        return array('created_at','updated_at','born');

    }
    public function money() {
        return $this->hasMany('App\Donate');
    }
    public function photos() {
        return $this->belongsToMany('App\Photos');
    }
    public function previewph() {
        return $this->belongsTo('App\Photos', 'preview_id');
    }


    public function setPreviewAttribute($value){
        if (isset($value['id']))  {
            $image = Photos::findOrFail($value['id']);
            $image->createPreviewCropIcon('children_preview',$value['detail_x'],$value['detail_y'],$value['detail_height'],$value['detail_width']);
            $this->attributes['preview_id'] = $value['id'];
        } else {
            $this->attributes['preview_id'] = null;
        }
    }

    public function documents() {
        return $this->belongsToMany('App\Photos','children_docs');
    }

    public function getShortAttribute() {
        return mb_substr(strip_tags($this->details), 0, 170);
    }

    public function getCollectedMoneyAttribute(){
        return $this->money()->where([['status','=','1']])->sum('sum');
    }

    public function getCollectedMoneyCountAttribute(){
        return $this->money()->where([['status','=','1']])->count('sum');
    }

    public function getLackMoneyAttribute(){
        $lack = $this->sum_need - $this->collected_money;
        if($lack<0) return 0;
        return $lack;
    }    
    public function getPreviewPhotoAttribute(){
        $photo = $this->previewph;
        if ($photo) {
            return $photo->icon_url_children_preview;
        }
        return '/images/no-image.png';

    }


    public function getPhotoPreviewAttribute(){
        $photo = $this->photos()->first();
        if ($photo) {
            return $photo->icon_url_children_preview;
        }
        return '/images/no-image.png';
    }
    public function getPhotoInsideAttribute(){
        $photo = $this->photos()->first();
        if ($photo) {
            return $photo->icon_url_children_inside;
        }
        return '/images/no-image.png';
    }

    public function getNameAttribute(){
        $locale = \App::getLocale();
        $name = 'name_'.$locale;
        return $this->$name;
    }
    public function getDetailsAttribute(){
        $locale = \App::getLocale();
        $name = 'details_'.$locale;
        return $this->$name;
    }


    public function getLinkAttribute(){
        return '/children/'.$this->id.'/'.$this->slug;
    }

    public function getSlugAttribute(){
        $text = $this->name_ru;

        $trans = [
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'jo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'jj',
            'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f',
            'х' => 'kh', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'eh', 'ю' => 'ju', 'я' => 'ja',
        ];
        $text  = mb_strtolower( $text, 'UTF-8' ); // lowercase cyrillic letters too
        $text  = strtr( $text, $trans ); // transliterate cyrillic letters
        $text  = preg_replace( '/[^A-Za-z0-9 _.]/', '', $text );
        $text  = preg_replace( '/[ _.]+/', '-', trim( $text ) );
        $text  = trim( $text, '-' );
        return $text;
    }

    public function getCityAttribute(){
        $city =  Cities::findOrFail( $this->city_id);
        return $city->name;
    }
    
    public function setImagesAttribute($value){
        foreach ($value as $image_id) {
            $image = Photos::findOrFail($image_id);
//            createChildrenIcon()
            $image->createInsideIcon();
        }
            $this->photos()->detach();
            $this->photos()->attach($value);

    }

    public function setDocumentsAttribute($value){
        $this->documents()->detach();
        $this->documents()->attach($value);
    }

}
