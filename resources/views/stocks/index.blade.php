@extends('layouts.public')
@section('pageTitle'){{ trans('messages.stock') }} @stop

@section('content')
        <div class="breadcrumbs">
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>{{ trans('messages.home') }}</a></div>
            <img src="/img/line.png">
            <div><a href="#">{{ trans('messages.stock') }}</a></div>
            <img src="/img/line.png">
        </div>
        <div class="main-content">
            @foreach ($stocks as $stock)

            <div class="container">
                <div class="main-container">
                    <div class="info-container stock">
                        <div class="img-container"><img src="{{$stock->preview_photo}}"></div>
                        <p>{{$stock->name}}</p>
                    </div>
                    <div class="text-container">
                        <p>{{$stock->short}}</p>
                        <a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}{{$stock->link}}" class="button">{{ trans('messages.more') }}</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="pagination">
            {{ $stocks->links() }}
        </div>

@stop