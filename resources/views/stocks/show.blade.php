@extends('layouts.public')
@section('pageTitle'){{$stocks->name}} @stop

@section('content')
     <div class="breadcrumbs">
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>{{ trans('messages.home') }}</a></div>
            <img src="/img/line.png">
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}/promotions/">{{ trans('messages.stock') }}</a></div>
            <img src="/img/line.png">
            <div><a href="#">{{$stocks->name}}</a></div>
            <img src="/img/line.png">
        </div>
        <div class="main-content">
            <div class="content-item">
                <div id="gallery" class="gallery">
                    <div class="photos">
                        @foreach($stocks->photos as  $count => $photo)
                            <img src="{{$photo->icon_url_news_inside}}" alt="" class="{{$count+1 == 1 ? 'show' : ''}}">

                        @endforeach
                    </div>
                    <div class="buttons">
                    </div>
                    <div class="dots"></div>
                </div>
                <div class="content-item-text">
                    <h3>{{$stocks->name}}</h3>
                    {!!  $stocks->details !!}
                </div>
            </div>
        </div>
@stop