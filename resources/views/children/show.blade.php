@extends('layouts.public')
@section('pageTitle'){{$child->name}} @stop

@section('content')

        <div class="breadcrumbs">
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>{{ trans('messages.home') }}</a></div>
            <img src="/img/line.png">
            @if ($child->collected_flag == 0)
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}">{{ trans('messages.needhelp') }}</a></div>
            @else
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}/alredyhelp">{{ trans('messages.alredyhelp') }}</a></div>
            @endif
            <img src="/img/line.png">
            <div><a href="#">{{$child->name}}</a></div>
        </div>
        <div class="main-content">
            <div class="content-item">
                @if ($child->collected_flag == 0)
                <div class="top-item form">
                    <div class="left-block-form">
                        <form action="/children/donate" method="get" id="donate" target="_blank">
                            <input type="hidden" name="children_id" value="{{ $child->id }}">
                            <input type="hidden" name="lang" value="{{ App\Http\Middleware\Locale::getLocale() }}">
                            <input type="hidden" name="_token" value="@csrf">
                            <p><input type="number" name="sum" id="sum" inputmode="numeric" pattern="[0-9]*" placeholder="{{ trans('messages.sum') }}, {{ \App\Options::getKeyValue('currency', \App\Http\Middleware\Locale::getLocale().'_currency') }}" autocomplete="off"></p>
                            <p><input type="text" name="comment" placeholder="{{ trans('messages.namecoments') }}"></p>
                        </form>
                        <div id="liqpayform" style="display: none;"></div>
                    </div>
                    <div class="rigth-block">
                        <ul>
                            <li><a href="#" class="credit"  id="donate-submit"></a></li>
                            <li><a href="#" class="receipt invoice" data-id="{{$child->id}}"></a></li>
                            <li><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}/smshelp" class="smartphone"></a></li>
                        </ul>
                    </div>
                </div>
                @endif
                <div class="details-item">
                    <div id="gallery" class="slide-det">
                        <div class="photos">
                            @foreach($child->photos as  $count =>  $photo)
                            <img src="{{$photo->icon_url_children_inside}}" alt="" class="{{$count+1 == 1 ? 'show' : ''}}">

                            @endforeach
                        </div>
                        <div class="buttons">
                        </div>
                        <div class="dots"></div>
                    </div>
                    <div class="content-details">
                        <h3>{{$child->name}}</h3>
                        <div class="content-details-text">
                            <div class="l-bl">{{ trans('messages.program') }}</div>
                            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}/programs">{{ $program->title }}</a></div>
                        </div>
                        <div class="content-details-text">
                            <div class="l-bl">{{ trans('messages.from') }}</div>
                            <div>{{$child->city}}</div>
                        </div>
                        <div class="content-details-text">
                            <div class="l-bl">{{ trans('messages.oldest') }}</div>
                            @if ($child->born->age >=5)
                                <div>{{$child->born->age}} {{ trans_choice('messages.years',$child->born->age) }}</div>
                            @else
                                <div>{{$child->born->diff(Carbon::now())->format('%y.%m')}} {{ trans_choice('messages.years',$child->born->age) }}</div>
                            @endif
                        </div>
                        @if ($child->collected_flag == 0)

                        <div class="content-details-text">
                            <div class="l-bl">{{ trans('messages.need') }}</div>
                            <div>{{number_format($child->sum_need, 0, ',', ' ')}} {{ \App\Options::getKeyValue('currency', \App\Http\Middleware\Locale::getLocale().'_currency') }}</div>
                        </div>
                        @endif

                        <div class="content-details-text">
                            <div class="l-bl">{{ trans('messages.ishave') }}</div>
                            <div>{{number_format($child->collected_money, 0, ',', ' ')}} {{ \App\Options::getKeyValue('currency', \App\Http\Middleware\Locale::getLocale().'_currency') }}</div>
                        </div>
                        @if ($child->collected_flag == 0)

                        <div class="content-details-text">
                            <div class="l-bl">{{ trans('messages.nothave') }}</div>
                            <div>{{number_format($child->lack_money, 0, ',', ' ')}} {{ \App\Options::getKeyValue('currency', \App\Http\Middleware\Locale::getLocale().'_currency') }}</div>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="content-item-texts">
                    {!! $child->details  !!}
                </div>
                <div class="receipt-block">
                    @foreach($child->documents as $doc)
                    <div class="receipt-block-img"><img src="{{$doc->url}}"></div>
                    @endforeach
                </div>
            </div>
        </div>

@stop


@section('js')
    <script>
        $('#donate-submit').click(function(e){
            e.preventDefault();
            create_donate();

        });


        function create_donate () {
                $.ajax({
                    type: "GET",
                    url: '/children/donate' ,
                    data: $("#donate").serialize(),
                    success: function(data){
                        $("#liqpayform").append(data);
                        $("#liqpayform form").submit()

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
        };
    </script>

    <script>

        $('#sum').change(function() {
            var value = $(this).val();

            $(this).val(value.replace(/[^0-9]/g, ''));
        });
        $('#sum').keypress(function() {
            var value = $(this).val();

            $(this).val(value.replace(/[^0-9]/g, ''));
        });
    </script>

@stop