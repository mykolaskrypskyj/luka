@extends('layouts.public')

@section('content')

        <div class="breadcrumbs">
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>Главная</a></div>
            <img src="/img/line.png">
            <div><a href="/">Нужна помощь</a></div>
            <img src="/img/line.png">
            <div><a href="#">{{$child->name}}</a></div>
        </div>
        <div class="main-content">
            <div class="content-item">
                <div class="top-item">
                    <div class="left-block-form">
                        <form>
                            <p><input type="text" name="" placeholder="Сумма, грн"></p>
                            <p><input type="text" name="" placeholder="Ваше имя, комментарий, пожелания"></p>
                        </form>
                    </div>
                    <div class="rigth-block">
                        <ul>
                            <li><a href="#" class="credit"></a></li>
                            <li><a href="#" class="receipt"></a></li>
                            <li><a href="#" class="smartphone"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="details-item">
                    <div id="gallery" class="slide-det">
                        <div class="photos">
                            @foreach($child->photos as  $count =>  $photo)
                            <img src="{{$photo->icon_url_children_inside}}" alt="" class="{{$count+1 == 1 ? 'show' : ''}}">

                            @endforeach
                        </div>
                        <div class="buttons">
                            <img src="/img/back.png" class="prev">
                            <img src="/img/forward.png" class="next">
                        </div>
                        <div class="dots"></div>
                    </div>
                    <div class="content-details">
                        <h3>{{$child->name}}</h3>
                        <div class="content-details-text">
                            <div class="l-bl">Программа</div>
                            <div>Программа</div>
                        </div>
                        <div class="content-details-text">
                            <div class="l-bl">Откуда</div>
                            <div>{{$child->city}}</div>
                        </div>
                        <div class="content-details-text">
                            <div class="l-bl">Возраст</div>
                            <div>{{$child->born->age}}</div>
                        </div>
                        <div class="content-details-text">
                            <div class="l-bl">Необходимо</div>
                            <div>{{$child->sum_need}}</div>
                        </div>
                        <div class="content-details-text">
                            <div class="l-bl">Собрано</div>
                            <div></div>
                        </div>
                        <div class="content-details-text">
                            <div class="l-bl">Не хватает</div>
                            <div></div>
                        </div>
                    </div>
                </div>

                <div class="content-item-texts">
                    {!! $child->details  !!}
                </div>
                <div class="receipt-block">
                    @foreach($child->documents as $doc)
                    <div class="receipt-block-img"><img src="{{$doc->url}}"></div>
                    @endforeach
                </div>
            </div>
        </div>

@stop