@extends('layouts.public')
@section('pageTitle'){{$staff->name}} @stop

@section('content')

        <div class="breadcrumbs">
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>{{ trans('messages.home') }}</a></div>
            <img src="/img/line.png">
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}/staff">{{ trans('messages.ourteam') }}</a></div>
            <img src="/img/line.png">
            <div><a href="#">{{$staff->name}}</a></div>
        </div>
        <div class="main-content">
            <div class="content-item">
                <div class="team-details">
                    <div class="team-details-img" style="background: url('{{$staff->staff_inside}}') no-repeat center; background-size: cover;"></div>
                    <div class="team-details-text">
                        <h3>{{$staff->name}}</h3>
                        <p class="">{{$staff->position}}</p>
                        {!! $staff->details !!}
                        <h3>Контакты</h3>
                        <p><a href="tel:{{ $staff->telephon }}">{{ $staff->telephon }}</a> <br>
                        <a href="mailto:{{ $staff->email }}">{{ $staff->email }}</a><br></p>
                    </div>
                </div>
            </div>
        </div>


@stop