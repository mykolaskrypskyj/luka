@extends('layouts.public')
@section('pageTitle'){{ trans('messages.ourteam') }} @stop

@section('content')
        <div class="breadcrumbs">
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>{{ trans('messages.home') }}</a></div>
            <img src="/img/line.png">
            <div><a href="#">{{ trans('messages.ourteam') }}</a></div>
            <img src="/img/line.png">
        </div>
        <div class="main-content">
            <div class="content-item">
                <div class="team-block">
                    @foreach($staff as $member)
                    <a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}{{$member->link}}" class="team-item">
                        <div class="team-item-img">
                            <div class="team-item-img" style="background: url('{{$member->staff_preview}}') no-repeat; background-size: cover;"></div>
                        </div>
                        <div class="team-item-info">
                            <h3>{{$member->name}}</h3>
                            <p>{{$member->position}}</p>
                        </div>
                    </a>
                    @endforeach

                </div>
                <div class="content-item-img">
                    <img src="{{$page->image_url}}">
                </div>
                <div class="content-item-text">
                    <h3>{{$page->title}}</h3>
                    <?php $text='text_'.$locale; ?>
                    {!! $field->$text !!}
                </div>
            </div>
        </div>



@stop