@extends('layouts.public')
@section('pageTitle'){{ trans('messages.news') }} @stop

@section('content')
        <div class="breadcrumbs">
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>{{ trans('messages.home') }}</a></div>
            <img src="/img/line.png">
            <div><a href="#">{{ trans('messages.news') }}</a></div>
            <img src="/img/line.png">
        </div>
        <div class="main-content">
            @foreach ($news as $news_one)

            <div class="container">
                <div class="main-container">
                    <div class="info-container stock">
                        <div class="img-container"><img src="{{$news_one->preview_photo}}"></div>
                        <p>{{$news_one->name}}</p>
                    </div>
                    <div class="text-container">
                        <p>{{$news_one->short}}</p>
                        <a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}{{$news_one->link}}" class="button">{{ trans('messages.more') }}</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="pagination">
            {{ $news->links() }}
        </div>

@stop