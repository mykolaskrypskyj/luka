<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg preserveAspectRatio="xMidYMin meet" width="100%" height="100%" viewbox="0 0 910 700" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <defs>
    <style>
      <![CDATA[.p,.u{stroke:#000;stroke-width:3}.p{stroke-width:2}*{font-family:Arial;fill:#000;font-size:13.8px}.s{font-size:11.5px}]]>
    </style>
  </defs>
  <text id="Квитанція" transform="rotate(-90)">
    <tspan x="-240" y="77">Заява на переказ готівки</tspan>
    <tspan x="-500" y="77">Квитанція</tspan>
  </text>
  <path d="M0 9h909" class="p"/>
  <text x="185" y="47">Дата здійснення операції:</text>
  <text x="366" y="47"></text>
  <text x="295" y="69">Сума:</text>
  <text x="350" y="70">{{ isset($input['sum']) ? $input['sum'].' грн 00 коп.': '100 грн 00 коп'}}</text>
  <text x="273" y="96">Платник:</text>
  <text x="203" y="136">Місце проживання:</text>
  <path d="M338 132h571M181 50h157M181 77h157m0-27h571M338 77V50m0 27h571m-728 28h157m0 0V77m0 28h571m-728 55h157m0 0v-55m0 55h571" class="p"/>
  <text x="192" y="180">Отримувач:</text>
  <text x="291" y="173">Назва: {{$payment->name}}</text>
  <text x="291" y="189">{{$payment->name2}}</text>
  <path d="M909 176v-16m-622 16h622m0 16v-16m-728 16h106m0 0v-32m0 32h622" class="p"/>
  <text x="275" y="206">Код:</text>
  <text x="497" y="206">Розрахунковий рахунок:</text>
  <text x="796" y="206">МФО банку:</text>
  <?php
  $kod = str_split($payment->kod);
  $score = str_split($payment->score);
  $mfo = str_split($payment->mfo); 
  ?>
  
  @foreach($kod as $item)
  <text x="{{191 + $loop->index *27 }}" y="222">{{$item}}</text>
  @endforeach
  <path d="M181 209h217m-217 0h217m-217 0h28m0 16v-16m0 0h27m0 16v-16m0 0h27m0 16v-16m0 0h27m0 16v-16m0 0h27m0 16v-16m0 0h27m0 16v-16m0 0h27m0 16v-16m0 0h27" class="p"/>
  @foreach($score as $item)
  <text x="{{409 + $loop->index *26 }}" y="222">{{$item}}</text>
  @endforeach
  <path d="M401 209h361m-361 0h361m-361 0h25m0 16v-16m0 0h26m0 16v-16m0 0h26m0 16v-16m0 0h26m0 16v-16m0 0h26m0 16v-16m0 0h25m0 16v-16m0 0h26m0 16v-16m0 0h26m0 16v-16m0 0h26m0 16v-16m0 0h26m0 16v-16m0 0h26m0 16v-16m0 0h25m0 16v-16m0 0h26m0 16v-16m0 0h26" class="p"/>
  @foreach($mfo as $item)
  <text x="{{773 + $loop->index *24 }}" y="222">{{$item}}</text>
  @endforeach
  <path d="M764 209h145m-145 0h145m-145 0h24m0 16v-16m0 0h25m0 16v-16m0 0h24m0 16v-16m0 0h24m0 16v-16m0 0h24m0 16v-16m0 0h24m0 16v-16m-511 0v-17m3 17v-17m361 17v-17m2 17v-17m145 17v-17m-511 33v-16m3 16v-16m361 16v-16m2 16v-16" class="p"/>
  <text x="185" y="238">Призначення платежу:</text>
  <text x="348" y="238">Благодійний внесок ({{$child->name}})</text>
  <path d="M341 225h568m0 16v-16m-568 16h568m-728-16h160m-160 16h160m0 0v-16m0 16h568" class="p"/>
  <text x="185" y="268">Платник:</text>
  <text x="367" y="268">Контролер:</text>
  <text x="549" y="268">Бухгалтер:</text>
  <text x="731" y="268">Касир:</text>
  <path d="M363 296v-55m182 55v-55m182 55v-55" class="p"/>
  <text x="185" y="338">Дата здійснення операції:</text>
  <text x="366" y="338"></text>
  <text x="295" y="360">Сума:</text>
  <text x="350" y="361">{{ isset($input['sum']) ? $input['sum'].' грн 00 коп.': '100 грн 00 коп'}}</text>
  <text x="273" y="387">Платник:</text>
  <text x="203" y="427">Місце проживання:</text>
  <path d="M338 423h571m-728-82h157m-157 27h157m0-27h571m-571 27v-27m0 27h571m-728 28h157m0 0v-28m0 28h571m-728 55h157m0 0v-55m0 55h571" class="p"/>
  <text x="192" y="471">Отримувач:</text>
  <text x="291" y="464">Назва: {{$payment->name}}</text>
  <text x="291" y="480">{{$payment->name2}}</text>
  <path d="M909 467v-16m-622 16h622m0 16v-16m-728 16h106m0 0v-32m0 32h622" class="p"/>
  <text x="275" y="497">Код:</text>
  <text x="497" y="497">Розрахунковий рахунок:</text>
  <text x="796" y="497">МФО банку:</text>
  @foreach($kod as $item)
  <text x="{{191 + $loop->index *27 }}" y="513">{{$item}}</text>
  @endforeach
  <path d="M181 500h217m-217 0h217m-217 0h28m0 16v-16m0 0h27m0 16v-16m0 0h27m0 16v-16m0 0h27m0 16v-16m0 0h27m0 16v-16m0 0h27m0 16v-16m0 0h27m0 16v-16m0 0h27" class="p"/>
  @foreach($score as $item)
  <text x="{{409 + $loop->index *26 }}" y="513">{{$item}}</text>
  @endforeach
  <path d="M401 500h361m-361 0h361m-361 0h25m0 16v-16m0 0h26m0 16v-16m0 0h26m0 16v-16m0 0h26m0 16v-16m0 0h26m0 16v-16m0 0h25m0 16v-16m0 0h26m0 16v-16m0 0h26m0 16v-16m0 0h26m0 16v-16m0 0h26m0 16v-16m0 0h26m0 16v-16m0 0h25m0 16v-16m0 0h26m0 16v-16m0 0h26" class="p"/>
  @foreach($mfo as $item)
  <text x="{{773 + $loop->index *24 }}" y="513">{{$item}}</text>
  @endforeach
  <path d="M764 500h145m-145 0h145m-145 0h24m0 16v-16m0 0h25m0 16v-16m0 0h24m0 16v-16m0 0h24m0 16v-16m0 0h24m0 16v-16m0 0h24m0 16v-16m-511 0v-17m3 17v-17m361 17v-17m2 17v-17m145 17v-17m-511 33v-16m3 16v-16m361 16v-16m2 16v-16" class="p"/>
  <text x="185" y="529">Призначення платежу:</text>
  <text x="348" y="529">Благодійний внесок ({{$child->name}})</text>
  <path d="M341 516h568m0 16v-16m-568 16h568m-728-16h160m-160 16h160m0 0v-16m0 16h568" class="p"/>
  <text x="185" y="559">Платник:</text>
  <text x="367" y="559">Контролер:</text>
  <text x="549" y="559">Бухгалтер:</text>
  <text x="731" y="559">Касир:</text>
  <path d="M363 587v-55m182 55v-55m182 55v-55" class="p"/>
  <path d="M181.5 296.5v-260m-181 260h181m-181 28h181m0-28h728m-728 28h728m-728 263v-263" class="u"/>
  <path d="M0 618h909" class="p"/>
  <text x="12" y="655" class="s">Увага!</text>
  <text x="12" y="666" class="s">Згідно Законодавства України при перерахуванні готівкових коштів на суму 15 000 грн чи більше потрібна ідентифікація особи (паспорт).</text>
  <text x="12" y="677" class="s">При оплаті у відділеннях «Приватбанку» комісія не стягується. У відділеннях інших банківських установ може стягуватися комісія банку.</text>
</svg>
<script>if(confirm('Распечатать?'))window.print()</script>