@extends('layouts.public')
@section('pageTitle'){{ trans('messages.needhelp') }} @stop

@section('content')
    <div class="breadcrumbs">
        <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>{{ trans('messages.home') }}</a></div>
        <img src="/img/line.png">
        <div><a href="#">{{ trans('messages.needhelp') }} </a></div>
        <img src="/img/line.png">
    </div>
    <div class="main-content">
        @foreach($children as $child)
            <div class="container">
                <div class="help-container">
                    <p>{{ trans('messages.needsum') }}: <b>{{number_format($child->lack_money, 0, ',', ' ')}} {{ \App\Options::getKeyValue('currency', \App\Http\Middleware\Locale::getLocale().'_currency') }}</b></p>
                </div>
                <div class="main-container">
                    <div class="info-container">
                        <div class="img-container"><img src="{{$child->preview_photo}}"></div>
                        <p>{{$child->name}}</p>
                    </div>
                    <div class="text-container">
                        <p>{{$child->short}}</p>
                        <a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}{{$child->link}}" class="button">{{ trans('messages.help') }}</a>
                    </div>
                </div>
            </div>
            @endforeach

    </div>
    <div class="pagination">
        {{ $children->links() }}
    </div>
    </div>
    </div>

@stop