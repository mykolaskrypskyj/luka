@extends('layouts.public')
@section('pageTitle'){{ trans('messages.searchresult') }} @stop

@section('content')
     <div class="breadcrumbs">
         <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>{{ trans('messages.home') }}</a></div>
            <img src="/img/line.png">
            <div><a href="#">{{ trans('messages.searchresult') }}: <b>{{$text}}</b></a></div>
            <img src="/img/line.png">
        </div>
        <div class="main-content">
            <div class="search-block">
                <div class="search-block-item"><p>{{ trans('messages.findresult') }}: {{$count}}</p></div>
                @foreach ($result as $one)
                <div class="search-block-item">
                    <div class="search-img-item"><a href="{{url(App\Http\Middleware\Locale::getLocale())}}{{$one['link']}}"><img src="{{$one['img']}}"></a></div>
                    <div class="search-text-item">
                        <a href="{{url(App\Http\Middleware\Locale::getLocale())}}{{$one['link']}}">{{$one['title']}}</a>
                        <p>{{$one['text']}}</p>
                    </div>
                </div>
                @endforeach

                </div>
            <div class="pagination">
                {{ $list->appends(['text' => Request::get('text')])->links() }}
            </div>

        </div>

@stop