@extends('layouts.public')
@section('pageTitle'){{$page->title}} @stop

@section('content')


    <div class="breadcrumbs">
        <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>{{ trans('messages.home') }}</a></div>
        <img src="/img/line.png">
        <div><a href="#">{{$page->title}}</a></div>
        <img src="/img/line.png">
    </div>
    <div class="main-content">
        <div class="content-item">
            <div class="content-item-img">
                <img src="{{$page->image_url}}">
            </div>
            <div class="content-item-text">
                <h3>{{$page->title}}</h3>
                <?php $text='text_'.$locale; ?>
                {!! $field->$text !!}
            </div>
        </div>
    </div>

@stop