@extends('layouts.public')
@section('pageTitle'){{$page->title}} @stop

@section('content')

        <div class="breadcrumbs">
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>{{ trans('messages.home') }}</a></div>
            <img src="/img/line.png">
            <div><a href="#">{{ trans('messages.takehelp') }}</a></div>
            <img src="/img/line.png">
        </div>
        <div class="main-content">
            <div class="get_help">
                <h3>{{$page->title}}</h3>
                <?php $text_before_file='text_before_file_'.$locale; ?>
                {!! $field->$text_before_file !!}
                <div class="download-bl"><a href="/ankets/anketa_{{$locale}}.doc">{{ trans('messages.dowloadanket') }} <img src="/img/download_24px.png"></a></div>
                <?php $text_after_file='text_after_file_'.$locale; ?>
                {!! $field->$text_after_file !!}
                <div class="contact">
                    <div class="content-details-get">
                        <h3>Контакты</h3>
                        <div class="content-details-text">
                            <div class="l-bl">{{ \App\Options::getKeyValue('contacts','phone') }}</div>
                            <div>Адрес</div>
                        </div>
                        <div class="content-details-text">
                            <div class="l-bl"><a href="#">lukafund@gmail.com</a></div>
                            <div><a href="mailto:{{ \App\Options::getKeyValue('contacts','email') }}">{{ \App\Options::getKeyValue('contacts','email') }}</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@stop