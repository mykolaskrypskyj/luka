@extends('layouts.public')
@section('pageTitle'){{ trans('messages.thankyou_title') }} @stop

@section('content')


    <div class="breadcrumbs">
        <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>{{ trans('messages.home') }} </a></div>
        <img src="/img/line.png">
        <div><a href="#">{{ trans('messages.thankyou_title') }}</a></div>
        <img src="/img/line.png">
    </div>
    <div class="main-content">
        <div class="content-item" style="text-align: center;">
                <i class="fa fa-check-circle" style="color: #81b440;font-size: 120px;"></i>
            <div class="content-item-text">
                <h3>{{ trans('messages.thankyou_title') }}</h3>
                {{ trans('messages.thankyou_text') }}
            </div>
        </div>
    </div>

@stop