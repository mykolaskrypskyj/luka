@extends('layouts.public')
@section('pageTitle'){{$specinfo->title}} @stop

@section('content')
        <div class="breadcrumbs">
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>{{ trans('messages.home') }}</a></div>
            <img src="/img/line.png">
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}/specinfo">{{ trans('messages.specinfo') }}</a></div>
            <img src="/img/line.png">
            <div><a href="#">{{$specinfo->title}}</a></div>
        </div>
        <div class="main-content">
            <div class="content-item">
                <div class="video-containers">
                    <iframe width="675" height="400" src="https://www.youtube.com/embed/{{$specinfo->video}}?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
                <div class="content-item-text">
                    <h3>{{$specinfo->title}}</h3>
                    {!! $specinfo->details !!}
                </div>
            </div>
        </div>

@stop

@section('css')
    <style>
        .video-containers iframe {max-width: 100%;}
    </style>
    @stop
