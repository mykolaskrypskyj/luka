@extends('layouts.public')
@section('pageTitle'){{ trans('messages.alredyhelp') }} @stop
@section('content')
    <div class="breadcrumbs">
        <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>{{ trans('messages.home') }}</a></div>
        <img src="/img/line.png">
        <div><a href="#">{{ trans('messages.alredyhelp') }} </a></div>
        <img src="/img/line.png">
    </div>
    <div class="main-content">
        @foreach($children as $child)
            <div class="container">
                <div class="main-container">
                    <div class="info-container">
                        <div class="img-container"><img src="{{$child->preview_photo}}"></div>
                        <p>{{$child->name}}</p>
                    </div>
                    <div class="text-container">
                        <p>{{$child->short}}</p>
                        <a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}/alreadyhelp/{{$child->id}}/{{$child->slug}}" class="button">{{ trans('messages.more') }}</a>
                    </div>
                </div>
            </div>
            @endforeach

    </div>
    <div class="pagination">
        {{ $children->links() }}
    </div>
    </div>
    </div>

@stop