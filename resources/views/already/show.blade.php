@extends('layouts.public')
@section('pageTitle'){{$child->name}} @stop

@section('content')

        <div class="breadcrumbs">
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}"><i class="fa fa-home" aria-hidden="true"></i>{{ trans('messages.home') }}</a></div>
            <img src="/img/line.png">
            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}/alreadyhelp">{{ trans('messages.alredyhelp') }}</a></div>
            <img src="/img/line.png">
            <div><a href="#">{{$child->name}}</a></div>
        </div>
        <div class="main-content">
            <div class="content-item">
                <div class="details-item">
                    <div id="gallery" class="slide-det">
                        <div class="photos">
                            @foreach($child->photos as  $count =>  $photo)
                            <img src="{{$photo->icon_url_children_inside}}" alt="" class="{{$count+1 == 1 ? 'show' : ''}}">

                            @endforeach
                        </div>
                        <div class="buttons">
                        </div>
                        <div class="dots"></div>
                    </div>
                    <div class="content-details">
                        <h3>{{$child->name}}</h3>
                        <div class="content-details-text">
                            <div class="l-bl">{{ trans('messages.program') }}</div>
                            <div><a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}/programs">{{ $program->title }}</a></div>
                        </div>
                        <div class="content-details-text">
                            <div class="l-bl">{{ trans('messages.from') }}</div>
                            <div>{{$child->city}}</div>
                        </div>
                        <div class="content-details-text">
                            <div class="l-bl">{{ trans('messages.oldest') }}</div>
                            @if ($child->born->age >=5)
                                <div>{{$child->born->age}} {{ trans_choice('messages.years',$child->born->age) }}</div>
                            @else
                                <div>{{$child->born->diff(Carbon::now())->format('%y.%m')}} {{ trans_choice('messages.years',$child->born->age) }}</div>
                            @endif                        </div>
                        <div class="content-details-text">
                            <div class="l-bl">{{ trans('messages.ishave') }}</div>
                            <div>{{number_format($child->collected_money, 0, ',', ' ')}} {{ \App\Options::getKeyValue('currency', \App\Http\Middleware\Locale::getLocale().'_currency') }}</div>
                        </div>

                    </div>
                </div>

                <div class="content-item-texts">
                    {!! $child->details  !!}
                </div>
                <div class="receipt-block">
                    @foreach($child->documents as $doc)
                    <div class="receipt-block-img"><img src="{{$doc->url}}"></div>
                    @endforeach
                </div>
            </div>
        </div>

@stop

