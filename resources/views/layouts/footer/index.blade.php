<footer>
    <div class="footer-top width clearfix">
        <nav class="nav-footer">
            <ul class="nav__list">
                <li class="nav__item{{ ((Request::segment(1) == 'takehelp')||(Request::segment(2) == 'takehelp')) ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/takehelp">{{ trans('messages.takehelp') }}</a>
                </li>
                <li class="nav__item{{ (Request::segment(1) == null)||((Request::segment(1) == \App\Http\Middleware\Locale::getLocale()) && (Request::segment(2) == null)) ? ' nav__item--current' : ''}} ">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/">{{ trans('messages.needhelp') }}</a>
                </li>
                <li class="nav__item{{ ((Request::segment(1) == 'alreadyhelp')||(Request::segment(2) == 'alredyhelp'))  ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/alreadyhelp">{{ trans('messages.alredyhelp') }}</a>
                </li>
                <li class="nav__item{{ ((Request::segment(1) == 'programs')||(Request::segment(2) == 'programs')) ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/programs">{{ trans('messages.programs') }}</a>
                </li>
                <li class="nav__item{{ ((Request::segment(1) == 'videos')||(Request::segment(2) == 'videos')) ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/videos">{{ trans('messages.video') }}</a>
                </li>
                <li class="nav__item{{ ((Request::segment(1) == 'news')||(Request::segment(2) == 'news')) ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/news/">{{ trans('messages.news') }}</a>
                </li>
                <li class="nav__item{{ ((Request::segment(1) == 'promotions')||(Request::segment(2) == 'stocks'))  ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/promotions">{{ trans('messages.stock') }}</a>
                </li>
                <li class="nav__item{{ ((Request::segment(1) == 'staff')||(Request::segment(2) == 'staff'))  ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/staff">{{ trans('messages.ourteam') }}</a>
                </li>
                <li class="nav__item{{ ((Request::segment(1) == 'specinfo')||(Request::segment(2) == 'specinfo')) ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/specinfo">{{ trans('messages.specinfo') }}</a>
                </li>
            </ul>
        </nav>
        <div class="social-contact">
            <div class="contact-block">
                <p>{{ \App\Options::getKeyValue('contacts','phone') }}</p>
                <a href="mailto:{{ \App\Options::getKeyValue('contacts','email') }}">{{ \App\Options::getKeyValue('contacts','email') }}</a>
            </div>
            <div class="social-icon">
                <div class="yt-icon icon">
                    <a href="{{ \App\Options::getKeyValue('social_network','youtube_link') }}"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                </div>
                <div class="fb-icon icon">
                    <a href="{{ \App\Options::getKeyValue('social_network','facebook_link') }}"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom bg-color-green">
        <span>&copy; 2018 lukafund</span>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script type="text/javascript" src="/js/lung-popup.js"></script>
<script type="text/javascript" src="/js/script.js"></script>

<script>
    $('#toggle-search').keyup(function(){
        $value=$(this).val();
        if ($value.length > 2) {
            $.ajax({

                type : 'get',

                url : '{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}/search',

                data:{
                    _token: '{{ csrf_token() }}',

                    'text':$value
                },

                success:function(data){
                    $('#search-result').empty();
                    $('#message-search').show();
                    if (data.length < 1 ) {
                        //$("#search-result").append('<div class="clear-text" id="nothing"><p>{{ trans('messages.nothingfound') }}</p></div>');
                        $('#nothing').show();
                    } else {
                        $('#nothing').hide();
                        $.each(data, function(i, result){
                            $("#search-result").append('<a href="{{url(App\Http\Middleware\Locale::getLocale())}}'+ result.link +'" class="search-block-item">' +
                                    ' <div class="search-item-img"><img src="' + result.img + '"></div> ' +
                                    ' <div class="search-item-text"> ' +
                                    ' <h4>' + result.title + '</h4> ' +
                                    ' <p>' + result.text + '</p> ' +
                                    ' </div>' +
                                    ' </a>');
                        });
                        $("#search-result").append('<div class="clear-text" id="clear"><p>{{ trans('messages.clear') }}</p></div>');

                        $('#message-search').show();
                    }
                }

            });

        } else {
            $('#search-result').empty();
            $('#message-search').hide();

        }

    })
    $(document).delegate('#clear', 'click',function() {
//    $('#clear').click(function() {

        $('#toggle-search').val("");
        $('#search-result').empty();
        $('#message-search').hide();
        $('#nothing').show();

    });
</script>
<script>
    document.querySelector('input#sum').onkeypress = function(e) {
        var e = e || event;
        if (e.ctrlKey || e.altKey || e.metaKey) return;
        var chr = getChar(e);
        if (chr == null) return;
        if (chr < '0' || chr > '9')
        { return false; }
    }
    function getChar(event) {
        if (event.which == null)
        { if (event.keyCode < 32) return null; return String.fromCharCode(event.keyCode); }
        if (event.which != 0 && event.charCode != 0)
        { if (event.which < 32) return null; return String.fromCharCode(event.which); }
        return null;
    }
</script>