<div class="left-block-text"><p>{{ trans('messages.allsponsors') }}</p></div>
<div class="noleft-block">
    <?php $list = \App\Sponsors::where([['public','=',1]])->get(); ?>
        @foreach($list as $one)
            <div class="left-block-img"><a href="{{$one->link}}"><img src="{{$one->sponsor_preview}}"></a></div>
        @endforeach

</div>
