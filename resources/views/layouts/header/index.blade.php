<header>
    <div class="header-top width">
        <div class="logo-block clearfix">
            <div class="logo">
                <a href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'/' }}{{ App\Http\Middleware\Locale::getLocale()}}"></a>
            </div>
            <div class="unit-collection">
                <p>{{ trans('messages.collected') }}:</p>
                <a href="" class="button">{{ trans('messages.on') }} {{ date('Y') }} {{ trans('messages.year') }}: {{number_format(\App\Donate::getCurentYearMoney(), 0, ',', ' ')}} {{ \App\Options::getKeyValue('currency', \App\Http\Middleware\Locale::getLocale().'_currency') }}</a>
                <a href="" class="button">{{ trans('messages.alltime') }}: {{number_format(\App\Donate::getAllMoney(), 0, ',', ' ')}} {{ \App\Options::getKeyValue('currency', \App\Http\Middleware\Locale::getLocale().'_currency') }}</a>
            </div>
        </div>
        <div class="logo-block-right">
            <div class="luka">
                <a href="{{ trans('messages.linkluka') }}" target="_blank" title="{{ trans('messages.sainluka') }}"></a>
            </div>
            <div class="social-contact">
                <div class="contact-block">
                    <p>{{ \App\Options::getKeyValue('contacts','phone') }}</p>
                    <a href="mailto:{{ \App\Options::getKeyValue('contacts','email') }}">{{ \App\Options::getKeyValue('contacts','email') }}</a>
                </div>
                <div class="social-icon">
                    <div class="yt-icon icon">
                        <a href="{{ \App\Options::getKeyValue('social_network','youtube_link') }}"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                    </div>
                    <div class="fb-icon icon">
                        <a href="{{ \App\Options::getKeyValue('social_network','facebook_link') }}"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <div class="lang-block">
                <div class="lang-active" id="toggle-link">
                    <?php $locale = App\Http\Middleware\Locale::getLocale();
                        switch ($locale) {
                            case '': $locale_name = 'Русский'; $flag = '/img/Rus18px.png';  break;
                            case 'ua': $locale_name = 'Українська'; $flag = '/img/Ukraine_18px.png'; break;
                            case 'en': $locale_name = 'English'; $flag = '/img/Great-Britain_18px.png'; break;
                            case 'de': $locale_name = 'Deutsch'; $flag = '/img/germany_18.png'; break;
                        };
                        echo '<a href="#"><img src="'.$flag.'"><span>'.$locale_name.'</span></a>';
                    ?>
                </div>
                <div class="lang-noactive" id="message">
                    <?php
                    $locales = explode(',', \App\Options::getKeyValue('language','entered_lang'));
                    if (($locale !='')&&(in_array('ru',$locales))) {
                        echo '<a href="/setlocale/ru"><img src="/img/Rus18px.png"><span>Русский</span></a>';
                        }
                    if (($locale !='ua')&&(in_array('ua',$locales)))  {
                        echo '<a href="/setlocale/ua"><img src="/img/Ukraine_18px.png"><span>Українська</span></a>';
                        }
                    if (($locale !='en')&&(in_array('en',$locales)))  {
                        echo '<a href="/setlocale/en"><img src="/img/Great-Britain_18px.png"><span>English</span></a>';
                        }
                    if (($locale !='de')&&(in_array('de',$locales)))  {
                        echo '<a href="/setlocale/de"><img src="/img/germany_18.png"><span>Deutsch</span></a>';
                        } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom bg-color-green">
        <div class="width">
            <div class="menu-icon">
                <a href="#" rel="mobile_nav"><i class="fa fa-bars" aria-hidden="true"></i></a>
            </div>
            <nav class="nav desktop">
                <ul class="nav__list">
                    <li class="nav__item{{ ((Request::segment(1) == 'takehelp')||(Request::segment(2) == 'takehelp')) ? ' nav__item--current' : ''}}">
                        <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/takehelp">{{ trans('messages.takehelp') }}</a>
                    </li>
                    <li class="nav__item{{ (Request::segment(1) == null)||((Request::segment(1) == \App\Http\Middleware\Locale::getLocale()) && (Request::segment(2) == null))|| ((Request::segment(1) == 'children')||(Request::segment(2) == 'children')) ? ' nav__item--current' : ''}} ">
                        <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/">{{ trans('messages.needhelp') }}</a>
                    </li>
                    <li class="nav__item{{ ((Request::segment(1) == 'alreadyhelp')||(Request::segment(2) == 'alreadyhelp'))  ? ' nav__item--current' : ''}}">
                        <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/alreadyhelp">{{ trans('messages.alredyhelp') }}</a>
                    </li>
                    <li class="nav__item{{ ((Request::segment(1) == 'programs')||(Request::segment(2) == 'programs')) ? ' nav__item--current' : ''}}">
                        <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/programs">{{ trans('messages.programs') }}</a>
                    </li>
                    <li class="nav__item{{ ((Request::segment(1) == 'videos')||(Request::segment(2) == 'videos')) ? ' nav__item--current' : ''}}">
                        <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/videos">{{ trans('messages.video') }}</a>
                    </li>
                    <li class="nav__item{{ ((Request::segment(1) == 'news')||(Request::segment(2) == 'news')) ? ' nav__item--current' : ''}}">
                        <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/news/">{{ trans('messages.news') }}</a>
                    </li>
                    <li class="nav__item{{ ((Request::segment(1) == 'promotions')||(Request::segment(2) == 'stocks'))  ? ' nav__item--current' : ''}}">
                        <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/promotions">{{ trans('messages.stock') }}</a>
                    </li>
                    <li class="nav__item{{ ((Request::segment(1) == 'staff')||(Request::segment(2) == 'staff'))  ? ' nav__item--current' : ''}}">
                        <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/staff">{{ trans('messages.ourteam') }}</a>
                    </li>
                    <li class="nav__item{{ ((Request::segment(1) == 'specinfo')||(Request::segment(2) == 'specinfo')) ? ' nav__item--current' : ''}}">
                        <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/specinfo">{{ trans('messages.specinfo') }}</a>
                    </li>
                    <div class="search-form">
                        <form id="" action="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{App\Http\Middleware\Locale::getLocale()}}/search">
                            <input type="text" autocomplete="off" id="toggle-search" name="text" placeholder="{{ trans('messages.search') }}">
                            <button><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                        <div class="popup-search" id="message-search">
                            <div class="search-block" id="search-result">

                            </div>
                            <div class="clear-text" id="nothing"><p>{{ trans('messages.nothingfound') }}</p></div>
                        </div>
                    </div>
                </ul>
            </nav>
        </div>
        <nav class="nav mobile">
            <ul class="nav__list">
                <li class="nav__item{{ ((Request::segment(1) == 'takehelp')||(Request::segment(2) == 'takehelp')) ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/takehelp">{{ trans('messages.takehelp') }}</a>
                </li>
                <li class="nav__item{{ (Request::segment(1) == null)||((Request::segment(1) == \App\Http\Middleware\Locale::getLocale()) && (Request::segment(2) == null)) ? ' nav__item--current' : ''}} ">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/">{{ trans('messages.needhelp') }}</a>
                </li>
                <li class="nav__item{{ ((Request::segment(1) == 'alreadyhelp')||(Request::segment(2) == 'alreadyhelp'))  ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/alreadyhelp">{{ trans('messages.alredyhelp') }}</a>
                </li>
                <li class="nav__item{{ ((Request::segment(1) == 'programs')||(Request::segment(2) == 'programs')) ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/programs">{{ trans('messages.programs') }}</a>
                </li>
                <li class="nav__item{{ ((Request::segment(1) == 'videos')||(Request::segment(2) == 'videos')) ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/videos">{{ trans('messages.video') }}</a>
                </li>
                <li class="nav__item{{ ((Request::segment(1) == 'news')||(Request::segment(2) == 'news')) ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/news/">{{ trans('messages.news') }}</a>
                </li>
                <li class="nav__item{{ ((Request::segment(1) == 'promotions')||(Request::segment(2) == 'stocks'))  ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/promotions">{{ trans('messages.stock') }}</a>
                </li>
                <li class="nav__item{{ ((Request::segment(1) == 'staff')||(Request::segment(2) == 'staff'))  ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/staff">{{ trans('messages.ourteam') }}</a>
                </li>
                <li class="nav__item{{ ((Request::segment(1) == 'specinfo')||(Request::segment(2) == 'specinfo')) ? ' nav__item--current' : ''}}">
                    <a class="nav__link" href="{{(App\Http\Middleware\Locale::getLocale() !='') ? '/':'' }}{{ App\Http\Middleware\Locale::getLocale()}}/specinfo">{{ trans('messages.specinfo') }}</a>
                </li>
            </ul>
        </nav>
    </div>

</header>
