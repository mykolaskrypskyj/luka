<!DOCTYPE html>
<html lang="ru">
    <head>
    @include('layouts.head.index')
    @yield('head','')
    @yield('css','')

    </head>
    <body>
    @include('layouts.header.index')
    <main>
        <div class="bg-color-grey">
            <div class="content width clearfix">
                <div class="left-block">
                @include('layouts.sidebar')
                </div>
                <div class="content-block">
                @yield('content','')
                </div>
            </div>
        </div>
    </main>



    @include('layouts.footer.index')

    @yield('footer','')

    @yield('js','')
    </body>
</html>