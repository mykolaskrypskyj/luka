<meta charset="UTF-8">
<title>@yield('pageTitle')</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
<link rel="apple-touch-icon" sizes="57x57" href="/img/favicons/favicon_57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/img/favicons/favicon_60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/img/favicons/favicon_72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/img/favicons/favicon_76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/img/favicons/favicon_114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/img/favicons/favicon_120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/img/favicons/favicon_144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/img/favicons/favicon_152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/img/favicons/favicon_180.png">
@include('layouts.head.css')

