@extends('adminlte::page')
@section('title', 'Добавить видео')
@section('content_header')
    @if( isset($record) ) <!-- Is for Edit/update -->
    <h1>Редактировать</h1>
    @else <!-- ...For Create/store -->
    <h1>Добавить</h1>
    @endif
@stop
@section('content')
    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif

    <div class="box box-default">
        <div class="box-header with-border">
        </div>
        <div class="box-body">
            @if( isset($record) ) <!-- Is for Edit/update -->
            {{ Form::model( $record, ['url' => 'admin/videos/'.$record->id, 'method' => 'put', 'role' => 'form'] ) }}
            @else <!-- ...For Create/store -->
            {{ Form::open( array('url' => 'admin/videos/')) }}
            @endif
            <div class="form-group">
                <label> Название :</label>
                {{ Form::text('title',null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=>'title' ]) }}
            </div>

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" style="text-transform: uppercase;">
                    @foreach ($locales as $count => $locale)
                        <li class="{{$count == 0 ? ' active' : ''}}">
                            <a href="#tab_{{$count}}" data-toggle="tab" aria-expanded="true">{{$locale}}</a>
                        </li>

                    @endforeach
                </ul>
            </div>
            <div class="tab-content">

                @foreach ($locales as $count => $locale)
                    <div class="tab-pane {{$count == 0 ? ' active' : ''}}" id="tab_{{$count}}">
                        <div class="form-group">
                            <label> Видео :</label>
                            {{ Form::text('video_full_'.$locale,null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'fullurl_'.$locale ]) }}
                            {{ Form::hidden('video_'.$locale,null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'video_'.$locale ]) }}
                        </div>

                    </div>
                @endforeach
            </div>
                    <div class="form-group">
                        <label>Опубликовать?</label>

                        {{Form::select('public',['1' => 'Да', '0' => 'Нет'])}}
                    </div>
                    @if( !isset($record) )
                        {{ Form::button('Создать', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @else
                        {{ Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @endif

                    {{ Form::close() }}
        </div>
    </div>
@endsection

@section('js')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script> CKEDITOR.timestamp= Date.now(); </script>

    <script> $(function () {


            $("form").submit( function(e) {
                var title = $("#title").val();

                if( title== '') {
                    $("#title").css({ 'border-color': "red" });
                    e.preventDefault();
                }

                var video_ru = $("#video_ru").val();

                if( video_ru== '') {
                    $("#video_ru").css({ 'border-color': "red" });
                    e.preventDefault();
                    $(".nav-tabs .active").removeClass('active');
                    $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                    $(".tab-content .active").removeClass('active');
                    $(".tab-content #tab_0").addClass('active');

                }

            });

            });
            $('#title').on('click',function(e) {
                $("#title").css({'border-color': ""});
            });

            $('#video_ru').on('click',function(e) {
                $("#video_ru").css({'border-color': ""});
            });



    </script>
    <script>
        $.urlParam = function(name){
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec($('#fullurl_ru').val());
            if (results==null){
                return null;
            }
            else{
                return decodeURI(results[1]) || 0;
            }
        }
        $(document).on('focusout', '#fullurl_ru', function(e) {
            $('#video_ru').val($.urlParam('v'));
            var inputValue = $('#video_ru').val();
            $.ajax({
                method: "GET",
                url: "https://www.googleapis.com/youtube/v3/videos",
                data: {
                    key: "AIzaSyA2DxOqpWrGe3O4QIHex44N3G-aUXFURF8",
                    part: "snippet",
                    id: inputValue
                }
            }).done(function( response ) {
                var title;
                if (response.items && response.items.length) {
                    title = response.items[0].snippet.title
                    $('#title').val(title);
                } else {
                    alert('Не удалось получить название видео. Проверьте код видео!');
                }

            });
        });

        $(document).on('focusout', '#fullurl_ua', function(e) {
            $('#video_ua').val($.urlParam('v'));
        });
        $(document).on('focusout', '#fullurl_en', function(e) {
            $('#video_en').val($.urlParam('v'));
        });
        $(document).on('focusout', '#fullurl_de', function(e) {
            $('#video_de').val($.urlParam('v'));
        });
    </script>

    <script>
        $(document).ready(function () {
            var input_ru = $('#video_ru').val();
            if (input_ru != '') {
                $('#fullurl_ru').val('https://www.youtube.com/watch?v=' + input_ru);
            }
            var input_ua = $('#video_ua').val();
            if (input_ua != '') {
                $('#fullurl_ua').val('https://www.youtube.com/watch?v=' + input_ua);
            }
            var input_en = $('#video_en').val();
            if (input_en != '') {
                $('#fullurl_en').val('https://www.youtube.com/watch?v=' + input_en);
            }
            var input_de = $('#video_de').val();
            if (input_de != '') {
                $('#fullurl_de').val('https://www.youtube.com/watch?v=' + input_de);
            }
        });
    </script>

@stop

