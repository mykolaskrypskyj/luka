@extends('adminlte::page')
@section('title', 'Видео')
@section('content_header')
    <h1>Видео</h1>
    <div class="breadcrumb"><button type="button" onclick="location.href='/admin/videos/create'" class="btn btn-block btn-primary">Добавить</button> </div>
@stop
@section('content')

    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif
    <div class="">
        <div class="row">
            <div class="col-xs-12">
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <td>id</td>
                        <td>Название</td>
                        <td>Действие</td>
                    </tr>
                </thead>

                @foreach ($videos as $video)
                <tr>
                    <td>{{$video->id}}</td>
                    <td>{{$video->title}}</td>
                    <td style="display:inline-flex;">
                         <a href="/admin/videos/{{$video->id}}/edit"><i class="fa fa-pencil"></i></a> &nbsp; &nbsp;
                         <form style="display:none;"  method="POST" action="{{action('Admin\VideosController@destroy',['articles'=>$video->id])}}">
                             <input type="hidden" name="_method" value="delete"/>
                             <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                             <button type="submit"><i class="fa fa-trash"></i></button>
                         </form>
                    </td>
                </tr>
                @endforeach
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div id="table-4_paginate" class="dataTables_paginate paging_simple_numbers">
                    {{ $videos->links() }}
            </div>
        </div>
    </div>
@endsection


@section('js')
    <style>
        button {
            border:0;
            background:none;
            color: #3c8dbc;
        }
    </style>
@stop