@extends('adminlte::page')
@section('title', 'Создать город')
@section('content_header')
    @if( isset($record) ) <!-- Is for Edit/update -->
    <h1>Редактировать</h1>
    @else <!-- ...For Create/store -->
    <h1>Создать</h1>
    @endif
@stop
@section('content')
    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif

    <div class="box box-default">
        <div class="box-header with-border">
        </div>
        <div class="box-body">
            @if( isset($record) ) <!-- Is for Edit/update -->
            {{ Form::model( $record, ['url' => 'admin/cities/'.$record->id, 'method' => 'put', 'role' => 'form'] ) }}
            @else <!-- ...For Create/store -->
            {{ Form::open( array('url' => 'admin/cities/')) }}
            @endif

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" style="text-transform: uppercase;">
                    @foreach ($locales as $count => $locale)
                        <li class="{{$count == 0 ? ' active' : ''}}">
                            <a href="#tab_{{$count}}" data-toggle="tab" aria-expanded="true">{{$locale}}</a>
                        </li>

                    @endforeach
                </ul>
            </div>
            <div class="tab-content">

                @foreach ($locales as $count => $locale)
                    <div class="tab-pane {{$count == 0 ? ' active' : ''}}" id="tab_{{$count}}">
                        <div class="form-group">
                            <label>Название города:</label>
                            {{ Form::text('name_'.$locale,null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'name_'.$locale ]) }}
                        </div>
                    </div>
                @endforeach
            </div>
                    @if( !isset($record) )
                        {{ Form::button('Создать', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @else
                        {{ Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @endif

                    {{ Form::close() }}
        </div>
    </div>
@endsection

@section('js')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

    <script> $(function () {

            $("form").submit( function(e) {
                var name_ru = $("#name_ru").val();
                var name_ua = $("#name_ua").val();
                var name_en = $("#name_en").val();
                var name_de = $("#name_de").val();

                if( name_de== '') {
                    $("#name_de").css({ 'border-color': "red" });
                    e.preventDefault();
                    $(".nav-tabs .active").removeClass('active');
                    $(".nav-tabs a[href$='#tab_3']").parents("li").addClass('active');
                    $(".tab-content .active").removeClass('active');
                    $(".tab-content #tab_3").addClass('active');

                }
                 if( name_en== '') {
                    $("#name_en").css({ 'border-color': "red" });
                    e.preventDefault();
                    $(".nav-tabs .active").removeClass('active');
                    $(".nav-tabs a[href$='#tab_2']").parents("li").addClass('active');
                    $(".tab-content .active").removeClass('active');
                    $(".tab-content #tab_2").addClass('active');

                }
                if( name_ua== '') {
                    $("#name_ua").css({ 'border-color': "red" });
                    e.preventDefault();
                    $(".nav-tabs .active").removeClass('active');
                    $(".nav-tabs a[href$='#tab_1']").parents("li").addClass('active');
                    $(".tab-content .active").removeClass('active');
                    $(".tab-content #tab_1").addClass('active');

                }
                if( name_ru== '') {
                    $("#name_ru").css({ 'border-color': "red" });
                    e.preventDefault();
                    $(".nav-tabs .active").removeClass('active');
                    $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                    $(".tab-content .active").removeClass('active');
                    $(".tab-content #tab_0").addClass('active');

                }
            });
            $('#name_ru').on('click',function(e) {
                $("#name_ru").css({'border-color': ""});
            });
            $('#name_ua').on('click',function(e) {
                $("#name_ua").css({'border-color': ""});
            });
            $('#name_en').on('click',function(e) {
                $("#name_en").css({'border-color': ""});
            });
            $('#name_de').on('click',function(e) {
                $("#name_de").css({'border-color': ""});
            });
        })

    </script>

@stop

