@extends('adminlte::page')
@section('title', 'Создать спонсора')
@section('content_header')
    @if( isset($record) ) <!-- Is for Edit/update -->
    <h1>Редактировать</h1>
    @else <!-- ...For Create/store -->
    <h1>Создать</h1>
    @endif
@stop
@section('content')
    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif

    <div class="box box-default">
        <div class="box-header with-border">
        </div>
        <div class="box-body">
            @if( isset($record) ) <!-- Is for Edit/update -->
            {{ Form::model( $record, ['url' => 'admin/sponsors/'.$record->id, 'method' => 'put', 'role' => 'form'] ) }}
            @else <!-- ...For Create/store -->
            {{ Form::open( array('url' => 'admin/sponsors/')) }}
            @endif

                <div class="form-group">
                    <label> Превью:</label>
                    @if( isset($record) ) <!-- Is for Edit/update -->
                    <div class="content" style="min-height: 0px;">
                        <div id="gallery" class="row">
                            <div class="col-sm-3 col-xs-4 image-thumb" >
                                <img src="{{$record->preview}}" class="selected_image" >
                                <input type="hidden" name="photo" value="{{$record->photo_id}}">
                            </div>
                        </div>
                    </div>
                    @else <!-- ...For Create/store -->
                    <div class="content" style="min-height: 0px;">
                        <div id="gallery" class="row"></div>
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label> Выберите альбом:</label>
                    {{Form::select('albums', $albums, null, ['class'=>'form-control', 'id'=> 'selected_album' ]  )}}
                    <label> Выберите изображения:</label>
                    <div class="content" style="min-height: 0px;">
                        <div id="photos" class="row"></div>
                    </div>
                </div>
                        <div class="form-group">
                            <label> Название :</label>
                            {{ Form::text('title',null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'title' ]) }}
                        </div>
                        <div class="form-group">
                            <label> Ссылка :</label>
                            {{ Form::text('link',null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'link' ]) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Опубликовать?</label>

                        {{Form::select('public',['1' => 'Да', '0' => 'Нет'])}}
                    </div>
                    @if( !isset($record) )
                        {{ Form::button('Создать', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @else
                        {{ Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @endif

                    {{ Form::close() }}
        </div>
    </div>
@endsection

@section('js')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script> CKEDITOR.timestamp= Date.now(); </script>

    <script> $(function () {

            $("form").submit( function(e) {
                var title = $("#title").val();
                var link = $("#link").val();

                if( title== '') {
                    $("#title").css({ 'border-color': "red" });
                    e.preventDefault();
                }
                if( link== '') {
                    $("#link").css({ 'border-color': "red" });
                    e.preventDefault();
                }
                var preview = $('#gallery input[name=photo]').val();
                if( !preview ) {
                    alert( 'В страницы должна быть миниатюра. Добавте ее пожалуйста' );
                    e.preventDefault();
                }

            });
            $('#title').on('click',function(e) {
                $("#title").css({'border-color': ""});
            });
            $('#link').on('click',function(e) {
                $("#link").css({'border-color': ""});
            });
        })

    </script>

        <script> $('#selected_album').on('change',function(e){
                    e.preventDefault();
                    var albId = $('#selected_album option:selected').val();
                    load_photo(albId);

        });
        $(document).ready(function () {

            var albId = $('#selected_album :first').val();
            load_photo(albId);
        });


            function load_photo (albId) {
                $.ajax({
                    type: "GET",
                    url: '/admin/album/'+ albId ,
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: albId,
                    },
                    success: function(data){
                        $("#photos").empty();
                        $.each(data.response.photos, function(idx, photos){
                            $("#photos").append('<div class="col-sm-1 col-xs-4"> <a href="#" class="select_image" data-id="'+photos.id + '" data-icon="'+ photos.thumb_url + '"> <img style="width:80px;" src="' +photos.thumb_url + '"> </a> </div>');
                        });
                        console.log("ajaxdata",data);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        </script>
        <script>
            $(document).on('click', '.select_image', function(e) {
                e.preventDefault();
                $('#gallery').html('<div class="col-sm-3 col-xs-4 image-thumb""><img src="'+ $(this).data("icon") +'" class="selected_image" >' +
                        '<input type="hidden" name="photo" value="'+ $(this).data("id") +'"> </div>' );
            });

            $(document).on('click', '.image-thumb', function(e) {
                e.preventDefault();
                var imgId = $(this).data("id");
                $(this).remove();
            });
        </script>
@stop

