@extends('adminlte::page')
@section('title', 'Создать/редактировать сотруднки')
@section('css')
    <link href="/css/cropper.min.css" rel="stylesheet">
    <style>
        #gallery_preview_img{
            max-height: 420px;
        }

    </style>
    @stop
@section('content_header')
    @if( isset($record) ) <!-- Is for Edit/update -->
    <h1>Редактировать</h1>
    @else <!-- ...For Create/store -->
    <h1>Создать</h1>
    @endif
@stop
@section('content')
    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif

    <div class="box box-default">
        <div class="box-header with-border">
        </div>
        <div class="box-body">
            @if( isset($record) ) <!-- Is for Edit/update -->
            {{ Form::model( $record, ['url' => 'admin/staff/'.$record->id, 'method' => 'put', 'role' => 'form'] ) }}
            @else <!-- ...For Create/store -->
            {{ Form::open( array('url' => 'admin/staff/')) }}
            @endif

                <div class="form-group">
                    <label> Превью:</label>
                    @if( isset($record) ) <!-- Is for Edit/update -->
                    <div class="content" style="min-height: 0px;">
                        <div id="gallery_preview" class="row">
                            <img src="{{$record->staff_preview}}" id="gallery_preview_img" class="selected_preview" >
                            <input type="hidden" name="photo[photo_id]" id="preview_id" value="{{$record->photo_id}}">
                            <input type="hidden" name="photo[detail_x]" id="detail_x">
                            <input type="hidden" name="photo[detail_y]" id="detail_y">
                            <input type="hidden" name="photo[detail_width]" id="detail_width">
                            <input type="hidden" name="photo[detail_height]" id="detail_height">
                        </div>
                    </div>
                    @else <!-- ...For Create/store -->
                    <div class="content" style="min-height: 0px;">
                        <div id="gallery_preview" class="row">
                            <img src="" id="gallery_preview_img" class="selected_preview" >
                            <input type="hidden" name="photo[photo_id]" id="preview_id">
                            <input type="hidden" name="photo[detail_x]" id="detail_x">
                            <input type="hidden" name="photo[detail_y]" id="detail_y">
                            <input type="hidden" name="photo[detail_width]" id="detail_width">
                            <input type="hidden" name="photo[detail_height]" id="detail_height">
                        </div>
                    </div>
                    @endif

                </div>
                <div class="form-group">
                    <label> Выберите альбом:</label>
                    {{Form::select('albums', $albums, null, ['class'=>'form-control', 'id'=> 'selected_album' ]  )}}
                    <label> Выберите изображения:</label>
                    <div class="content" style="min-height: 0px;">
                        <div id="photos" class="row"></div>
                    </div>
                </div>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" style="text-transform: uppercase;">
                    @foreach ($locales as $count => $locale)
                        <li class="{{$count == 0 ? ' active' : ''}}">
                            <a href="#tab_{{$count}}" data-toggle="tab" aria-expanded="true">{{$locale}}</a>
                        </li>

                    @endforeach
                </ul>
            </div>
            <div class="tab-content">

                @foreach ($locales as $count => $locale)
                    <div class="tab-pane {{$count == 0 ? ' active' : ''}}" id="tab_{{$count}}">
                        <div class="form-group">
                            <label> ФИО сотрудника :</label>
                            {{ Form::text('name_'.$locale,null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'name_'.$locale ]) }}
                        </div>
                        <div class="form-group">
                            <label> Должность сотрудника :</label>
                            {{ Form::text('position_'.$locale,null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'position_'.$locale ]) }}
                        </div>
                        <div class="form-group">
                            <label> Описание сотрудника:</label>
                            <div class="box-body pad">
                                {{ Form::textarea('details_'.$locale, null,['class'=>'form-control','id'=>'editor'.$count,'placeholder'=>'Enter ... ','rows'=>'10','cols'=>'80' ]) }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="form-group">
                <label>Телефон (формат: +380501234567):</label>
                {{ Form::text('telephon',null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=>'telephon','pattern'=>'^\+?\d{0,13}']) }}
            </div>
            <div class="form-group">
                <label>Email :</label>
                {{ Form::email('email',null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=>'email']) }}
            </div>

                    <div class="form-group">
                        <label>Опубликовать?</label>

                        {{Form::select('public',['1' => 'Да', '0' => 'Нет'], null, [ 'id'=> 'public_select' ])}}
                    </div>
                    @if( !isset($record) )
                        {{ Form::button('Создать', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @else
                        {{ Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @endif

                    {{ Form::close() }}
        </div>
    </div>
@endsection

@section('js')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script> CKEDITOR.timestamp= Date.now(); </script>

    <script> $(function () {

            CKEDITOR.replace('editor0');
            CKEDITOR.replace('editor1');
            CKEDITOR.replace('editor2');
            CKEDITOR.replace('editor3');


            $("form").submit( function(e) {
                var name_ru = $("#name_ru").val();

                if( name_ru== '') {
                    $("#name_ru").css({ 'border-color': "red" });
                    e.preventDefault();
                    $(".nav-tabs .active").removeClass('active');
                    $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                    $(".tab-content .active").removeClass('active');
                    $(".tab-content #tab_0").addClass('active');

                }
                if ($('#public_select option:checked').val() == '1') {

                    var position_ru = $("#position_ru").val();

                    if (position_ru == '') {
                        $("#position_ru").css({'border-color': "red"});
                        e.preventDefault();
                        $(".nav-tabs .active").removeClass('active');
                        $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                        $(".tab-content .active").removeClass('active');
                        $(".tab-content #tab_0").addClass('active');

                    }

                    var telephon = $("#telephon").val();

                    if (telephon == '') {
                        $("#telephon").css({'border-color': "red"});
                        e.preventDefault();
                        $(".nav-tabs .active").removeClass('active');
                        $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                        $(".tab-content .active").removeClass('active');
                        $(".tab-content #tab_0").addClass('active');

                    }

                    if (!telephon) {
                        alert('Телефон должен быть заполнен!');
                    }

                    var email = $("#email").val();

                    if (email == '') {
                        $("#email").css({'border-color': "red"});
                        e.preventDefault();
                        $(".nav-tabs .active").removeClass('active');
                        $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                        $(".tab-content .active").removeClass('active');
                        $(".tab-content #tab_0").addClass('active');

                    }

                    if (!email) {
                        alert('Email должен быть заполнен!');
                    }

                    var messageLength_ru = CKEDITOR.instances['editor0'].getData().replace(/<[^>]*>/gi, '').length;
                    if (!messageLength_ru) {
                        $("#cke_editor0").css({'border-color': "red"});
                        $(".nav-tabs .active").removeClass('active');
                        $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                        $(".tab-content .active").removeClass('active');
                        $(".tab-content #tab_0").addClass('active');
                        e.preventDefault();
                    }


                    if (!messageLength_ru) {
                        alert('Название, должность или текст новости не может быть пустой');
                    }

                        var preview = $('#gallery input[name=preview[photo_id]]').val();
                    if (!preview) {
                        alert('В страницы должна быть миниатюра. Добавте ее пожалуйста');
                        e.preventDefault();
                    }
                }
            });
            $('#name_ru').on('click',function(e) {
                $("#name_ru").css({'border-color': ""});
            });
            $('#position_ru').on('click',function(e) {
                $("#position_ru").css({'border-color': ""});
            });

            $('#email').on('click',function(e) {
                $("#email").css({'border-color': ""});
            });
            $('#telephon').on('click',function(e) {
                $("#telephon").css({'border-color': ""});
            });

            CKEDITOR.instances['editor0'].on('change', function() {
                $("#cke_editor0").css({'border-color': ""});
            });

        })

    </script>

        <script> $('#selected_album').on('change',function(e){
                    e.preventDefault();
                    var albId = $('#selected_album option:selected').val();
                    load_photo(albId);

        });
        $(document).ready(function () {

            var albId = $('#selected_album :first').val();
            load_photo(albId);
        });


            function load_photo (albId) {
                $.ajax({
                    type: "GET",
                    url: '/admin/album/'+ albId ,
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: albId,
                    },
                    success: function(data){
                        $("#photos").empty();
                        $.each(data.response.photos, function(idx, photos){
                            $("#photos").append('<div class="col-sm-1 col-xs-4"> <a href="#" class="select_image" data-id="'+photos.id + '" data-icon="'+ photos.thumb_url + '"  data-url="'+ photos.url + '"> <img style="width:80px;" src="' +photos.thumb_url + '"> </a> </div>');
                        });
                        console.log("ajaxdata",data);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        </script>
        <script>
            var $image = $("#gallery_preview_img");

            $(document).on('click', '.select_image', function(e) {
                e.preventDefault();

                $('#gallery_preview_img').attr('src', $(this).data("url"));
                $('#preview_id').val($(this).data("id"));
                console.log($image);

//                $('#gallery').html('<div class="col-sm-3 col-xs-4 image-thumb""><img src="'+ $(this).data("icon") +'" class="selected_image" >' +
//                        '<input type="hidden" name="photo" value="'+ $(this).data("id") +'"> </div>' );

                originalData = {};
                $image.cropper({
                    viewMode: 1,
                    aspectRatio: 60/60,
                    resizable: true,
                    zoomable: false,
                    rotatable: false,
                    multiple: true,
                    crop: function(event) {
                        $("#detail_x").val(Math.round(event.detail.x));
                        $("#detail_y").val(Math.round(event.detail.y));
                        $("#detail_width").val(Math.round(event.detail.width));
                        $("#detail_height").val(Math.round(event.detail.height));
                    },
                    dragend: function(data) {
                        originalData = $image.cropper("getCroppedCanvas");
                        console.log(originalData.toDataURL());
                        $('.data-url').text(originalData.toDataURL());
                    }
                });

                var cropper = $image.data('cropper');
//            console.log(cropper);
                cropper.replace($(this).data("url"));
            });

            $(document).on('click', '.image-thumb', function(e) {
                e.preventDefault();
                var imgId = $(this).data("id");
                $(this).remove();
            });
        </script>
    <script src="/js/cropper.min.js"></script>
    {{--<script>--}}

        {{--$(document).on('click', '.selected_preview', function(e) {--}}
            {{--e.preventDefault();--}}
{{--//            console.log($(this).data());--}}
{{--//            $('#gallery').attr('src', $(this).data("url"));--}}
{{--//            $('#preview_id').val($(this).data("id"));--}}
            {{--console.log($image);--}}
           {{----}}
        {{--});--}}
    {{--</script>--}}

@stop

