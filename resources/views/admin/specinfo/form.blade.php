@extends('adminlte::page')
@section('title', 'Добавить ')
@section('content_header')
    @if( isset($record) ) <!-- Is for Edit/update -->
    <h1>Редактировать</h1>
    @else <!-- ...For Create/store -->
    <h1>Добавить</h1>
    @endif
@stop
@section('content')
    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif

    <div class="box box-default">
        <div class="box-header with-border">
        </div>
        <div class="box-body">
            @if( isset($record) ) <!-- Is for Edit/update -->
            {{ Form::model( $record, ['url' => 'admin/specinfo/'.$record->id, 'method' => 'put', 'role' => 'form'] ) }}
            @else <!-- ...For Create/store -->
            {{ Form::open( array('url' => 'admin/specinfo/')) }}
            @endif

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" style="text-transform: uppercase;">
                    @foreach ($locales as $count => $locale)
                        <li class="{{$count == 0 ? ' active' : ''}}">
                            <a href="#tab_{{$count}}" data-toggle="tab" aria-expanded="true">{{$locale}}</a>
                        </li>

                    @endforeach
                </ul>
            </div>
            <div class="tab-content">

                @foreach ($locales as $count => $locale)
                    <div class="tab-pane {{$count == 0 ? ' active' : ''}}" id="tab_{{$count}}">
                        <div class="form-group">
                            <label> Название :</label>
                            {{ Form::text('title_'.$locale,null,['class'=>'form-control','placeholder'=>'Enter ... ' ,'id' => 'title_'.$locale]) }}
                        </div>
                        <div class="form-group">
                            <label> Видео :</label>
                            {{ Form::text('video_full_'.$locale,null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'fullurl_'.$locale ]) }}

                            {{ Form::hidden('video_'.$locale,null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'video_'.$locale ]) }}
                        </div>
                        <div class="form-group">
                            <label> Описание видео:</label>
                            <div class="box-body pad">
                                {{ Form::textarea('details_'.$locale, null,['class'=>'form-control','id'=>'editor'.$count,'placeholder'=>'Enter ... ','rows'=>'10','cols'=>'80' ]) }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
                    <div class="form-group">
                        <label>Опубликовать?</label>

                        {{Form::select('public',['1' => 'Да', '0' => 'Нет'], null, [ 'id'=> 'public_select' ])}}
                    </div>
                    @if( !isset($record) )
                        {{ Form::button('Создать', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @else
                        {{ Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @endif

                    {{ Form::close() }}
        </div>
    </div>
@endsection

@section('js')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script> CKEDITOR.timestamp= Date.now(); </script>

    <script> $(function () {

            CKEDITOR.replace('editor0');
            CKEDITOR.replace('editor1');
            CKEDITOR.replace('editor2');
            CKEDITOR.replace('editor3');


            $("form").submit( function(e) {


                var title_ru = $("#title_ru").val();

                if( title_ru== '') {
                    $("#title_ru").css({ 'border-color': "red" });
                    e.preventDefault();
                }
                if ($('#public_select option:checked').val() == '1') {

                    var video_ru = $("#video_ru").val();

                    if (video_ru == '') {
                        $("#video_ru").css({'border-color': "red"});
                        e.preventDefault();
                        $(".nav-tabs .active").removeClass('active');
                        $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                        $(".tab-content .active").removeClass('active');
                        $(".tab-content #tab_0").addClass('active');

                    }


                    var messageLength_ru = CKEDITOR.instances['editor0'].getData().replace(/<[^>]*>/gi, '').length;
                    if (!messageLength_ru) {
                        $("#cke_editor0").css({'border-color': "red"});
                        $(".nav-tabs .active").removeClass('active');
                        $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                        $(".tab-content .active").removeClass('active');
                        $(".tab-content #tab_0").addClass('active');
                        e.preventDefault();
                    }


                    if (!messageLength_ru) {
                        alert('Название или описание не может быть пустым');
                    }
                }
            });
            $('#title_ru').on('click',function(e) {
                $("#title_ru").css({'border-color': ""});
            });

            $('#video_ru').on('click',function(e) {
                $("#video_ru").css({'border-color': ""});
            });

            CKEDITOR.instances['editor0'].on('change', function() {
                $("#cke_editor0").css({'border-color': ""});
            });

        })

    </script>


    <script>
        $.urlParam = function(name){
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec($('#fullurl_ru').val());
            if (results==null){
                return null;
            }
            else{
                return decodeURI(results[1]) || 0;
            }
        }
        $(document).on('focusout', '#fullurl_ru', function(e) {
            $('#video_ru').val($.urlParam('v'));
        });

        $(document).on('focusout', '#fullurl_ua', function(e) {
            $('#video_ua').val($.urlParam('v'));
        });
        $(document).on('focusout', '#fullurl_en', function(e) {
            $('#video_en').val($.urlParam('v'));
        });
        $(document).on('focusout', '#fullurl_de', function(e) {
            $('#video_de').val($.urlParam('v'));
        });
    </script>

    <script>
        $(document).ready(function () {
            var input_ru = $('#video_ru').val();
            if (input_ru != '') {
                $('#fullurl_ru').val('https://www.youtube.com/watch?v=' + input_ru);
            }
            var input_ua = $('#video_ua').val();
            if (input_ua != '') {
                $('#fullurl_ua').val('https://www.youtube.com/watch?v=' + input_ua);
            }
            var input_en = $('#video_en').val();
            if (input_en != '') {
                $('#fullurl_en').val('https://www.youtube.com/watch?v=' + input_en);
            }
            var input_de = $('#video_de').val();
            if (input_de != '') {
                $('#fullurl_de').val('https://www.youtube.com/watch?v=' + input_de);
            }
        });
    </script>



@stop

