@extends('adminlte::page')
@section('title', 'Редактировать опцию валюты')
@section('content_header')
    <h1>Опции</h1>
@stop
@section('content')

    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif
    <div class="box box-default">
        <div class="box-header with-border">
        </div>
        <div class="box-body">
            {{ Form::model( $option, ['url' => 'admin/options/'.$option->key, 'method' => 'put', 'role' => 'form'] ) }}
            {{ Form::hidden($option->key,null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'name_' ]) }}
            <div class="form-group">
                <label>Язык RU:</label>
                {{Form::select('data[_currency]', ['₴'=> '₴','грн.'=> 'грн.' ], null, ['class'=>'form-control', 'id'=> 'selected_album' ]  )}}
            </div>
            <div class="form-group">
                <label>Язык UA:</label>
                {{Form::select('data[ua_currency]', ['₴'=> '₴','грн.'=> 'грн.' ], null, ['class'=>'form-control', 'id'=> 'selected_album' ]  )}}
            </div>
            <div class="form-group">
                <label>Язык EN:</label>
                {{Form::select('data[en_currency]', ['₴'=> '₴','UAH'=> 'UAH' ], null, ['class'=>'form-control', 'id'=> 'selected_album' ]  )}}
            </div>
            <div class="form-group">
                <label>Язык DE:</label>
                {{Form::select('data[de_currency]', ['₴'=> '₴','UAH'=> 'UAH' ], null, ['class'=>'form-control', 'id'=> 'selected_album' ]  )}}
            </div>

            {{ Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
            {{ Form::close() }}
        </div>

    </div>
@endsection


@section('css')
    <style>
        button {
            border:0;
            background:none;
            color: #3c8dbc;
        }
    </style>
@stop

@section('js')
    <script>
        $(function(){
            $('.creatorValue').on('click',function(e){

                var name = $('div.tab-pane.active input[name=name]').val();
                if (name != '') {
                    var str = '<div class="form-group">';
                    str += '<label style="display: block;">'+name+'</label>';
                    str += '<input class="form-control" type="text" name="data['+name+']" style="width:90%;display: inline-block;margin-right: 15px;">';
                    str += '<a style="display:none;" class="del val" href="javascript:;"><i class="fa fa-remove"></i></a>';
                    str += '</div> ';
                }
//        $('div.tab-pane.active div.add-input').append(str);
                $(this).closest('div.tab-pane.active').find('form div.fields').append(str);
//        return false;
            });
            $('body').on('click','a.del.val', function(){
                $(this).parents('div.form-group').html('');
            });


        });
    </script>
@stop