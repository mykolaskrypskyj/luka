@extends('adminlte::page')
@section('title', 'Редактировать опции')
@section('content_header')
<h1>Опции</h1>
@stop
@section('content')

    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif
    <div class="box box-default">
        <div class="box-header with-border">
        </div>
        <div class="box-body">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs" style="text-transform: uppercase;">
                <li class="active">
                    <a href="#tab_0" style="display:none;" data-toggle="tab" aria-expanded="true">Создать групу</a>
                </li>
            @foreach ($options as $count => $option)
                @if ($option->key == 'currency')  @continue;
                    @endif
                <li class="{{$count == 0 ? ' active' : ''}}">
                    <a href="#tab_{{$count+1}}" data-toggle="tab" aria-expanded="true">{{$option->key}}</a>
                </li>

            @endforeach
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" style="display:none;" id="tab_0">
                {{ Form::open( array('url' => 'admin/options/addgroup')) }}

                <div class="form-group">
                    <label> Создать новую группу :</label>
                    {{ Form::text('key',null,['class'=>'form-control','placeholder'=>'Введите имя группы','id'=> 'name_' ]) }}
                </div>
                {{ Form::button('Создать', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                {{ Form::close() }}

            </div>
        @foreach ($options as $count => $option)
                @if ($option->key == 'currency')  @continue;
                @endif
                <div class="tab-pane {{$count == 0 ? ' active' : ''}}" id="tab_{{$count+1}}">

                    {{ Form::model( $option, ['url' => 'admin/options/'.$option->key, 'method' => 'put', 'role' => 'form'] ) }}
                    {{ Form::hidden($option->key,null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'name_' ]) }}
                    <div class="fields">
                    @if ($option->data != null)
                        @foreach ($option->data as $key =>$value)
                            <div class="form-group">
                                <label style="display: block;"> {{$key}}:</label>
                                {{ Form::text('data['.$key.']',$value,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'name_'.$key,'style'=>'width:90%;display: inline-block;margin-right: 15px;' ]) }}
                                <a style="display:none;" class="del val" href="javascript:;"><i class="fa fa-remove"></i></a>
                            </div>
                        @endforeach
                    @endif
                    </div>
                    {{ Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    {{ Form::close() }}
                    <br/><br/><br/>
                    <div class="form-group" style="display:none;">
                        <label> Создать поле:</label>
                        <input type="text" name="name"  id="new" class="form-control">
                    </div>
                    <button style="display:none;" class="btn btn-block btn-primary creatorValue">Создать поле</button>
                </div>
            @endforeach
        </div>
        </div>
    </div>
@endsection


@section('css')
    <style>
        button {
            border:0;
            background:none;
            color: #3c8dbc;
        }
    </style>
@stop

@section('js')
    <script>
        $('#name_all_lang').prop('readonly', true);

        $(function(){
        $('.creatorValue').on('click',function(e){

        var name = $('div.tab-pane.active input[name=name]').val();
            if (name != '') {
                var str = '<div class="form-group">';
                    str += '<label style="display: block;">'+name+'</label>';
                        str += '<input class="form-control" type="text" name="data['+name+']" style="width:90%;display: inline-block;margin-right: 15px;">';
                        str += '<a style="display:none;" class="del val" href="javascript:;"><i class="fa fa-remove"></i></a>';
                    str += '</div> ';
            }
//        $('div.tab-pane.active div.add-input').append(str);
            $(this).closest('div.tab-pane.active').find('form div.fields').append(str);
//        return false;
        });
        $('body').on('click','a.del.val', function(){
        $(this).parents('div.form-group').html('');
        });


        });
    </script>
@stop