@extends('adminlte::page')
@section('title', 'Редактировать страницу')
@section('content_header')
    @if( isset($record) ) <!-- Is for Edit/update -->
    <h1>Редактировать</h1>
    @else <!-- ...For Create/store -->
    <h1>Создать</h1>
    @endif
@stop
@section('content')
    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif

    <div class="box box-default">
        <div class="box-header with-border">
        </div>
        <div class="box-body">
            @if( isset($record) ) <!-- Is for Edit/update -->
            {{ Form::model( $record, ['url' => 'admin/pages/'.$record->id, 'method' => 'put', 'role' => 'form', 'files' => true] ) }}
            @else <!-- ...For Create/store -->
            {{ Form::open( array('url' => 'admin/pages/')) }}
            @endif

            @if ($record->type !='takehelp')
            <div class="form-group">
                <label> Превью:</label>
                @if( isset($record) ) <!-- Is for Edit/update -->
                <div class="content" style="min-height: 0px;">
                    <div id="gallery" class="row">
                        <div class="col-sm-3 col-xs-4 image-thumb" >
                            <img src="{{$record->preview}}" class="selected_image" >
                            <input type="hidden" name="photo_id" value="{{$record->photo_id}}">
                        </div>
                    </div>
                </div>
                @else <!-- ...For Create/store -->
                <div class="content" style="min-height: 0px;">
                    <div id="gallery" class="row"></div>
                </div>
                @endif
            </div>
            <div class="form-group">
                <label> Выберите альбом:</label>
                {{Form::select('albums', $albums, null, ['class'=>'form-control', 'id'=> 'selected_album' ]  )}}
                <label> Выберите изображения:</label>
                <div class="content" style="min-height: 0px;">
                    <div id="photos" class="row"></div>
                </div>
            </div>

            @endif
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" style="text-transform: uppercase;">
                    @foreach ($locales as $count => $locale)
                        <li class="{{$count == 0 ? ' active' : ''}}">
                            <a href="#tab_{{$count}}" data-toggle="tab" aria-expanded="true">{{$locale}}</a>
                        </li>

                    @endforeach
                </ul>
            </div>
            <div class="tab-content">

                @foreach ($locales as $count => $locale)
                    <div class="tab-pane {{$count == 0 ? ' active' : ''}}" id="tab_{{$count}}">
                        <div class="form-group">
                            <label> Название страницы :</label>
                            {{ Form::text('title_'.$locale,null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'title_'.$locale ]) }}
                        </div>
                        @foreach ( $record->getfields() as $count_inside  =>$field)
                            <div class="form-group">
                                <label>{{$field['title']}}:</label>
                                <div class="box-body pad">
                                    @switch( $field['type'] )
                                    @case('textarea')

                                    {{ Form::textarea($field['name'].'_'.$locale, $fields[$locale][$field['name'].'_'.$locale],['class'=>'form-control','id'=>'editor'.$locale.$count_inside,'placeholder'=>'Enter ... ','rows'=>'10','cols'=>'80' ]) }}
                                    @break

                                    @case('file')
                                    {{ Form::file($field['name'].'_'.$locale,  ['accept'=>'.doc', 'style' =>'width: 25%;display: inline-block;margin-right: 15px;' ]) }}

                                        @if(file_exists( public_path().'/ankets/anketa_'.$locale.'.doc' ))
                                            <a href="/ankets/anketa_<?php echo $locale; ?>.doc"><i class="fa fa-file-word-o"></i>  Скачать файл anketa_<?php echo$locale; ?>.doc </a>
                                        @else
                                            Пожалуйста загрузите файл
                                        @endif
                                    <br><br><i>Допустима загрузка только файлов с расширением .doc:</i>

                                    @break

                                    @case('image')


                                    {{ Form::text($field['name'].'_'.$locale, null,['class'=>'form-control','placeholder'=>'Enter ... ','rows'=>'10','cols'=>'80' ]) }}
                                    @break

                                    @default
                                    <span>Something went wrong, please contact with admin</span>
                                    @endswitch


                                </div>
                            </div>
                        @endforeach

                    </div>
                @endforeach
            </div>

                    @if( !isset($record) )
                        {{ Form::button('Создать', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @else
                        {{ Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @endif

                    {{ Form::close() }}
        </div>
    </div>
@endsection

@section('js')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script> CKEDITOR.timestamp= Date.now(); </script>

    <script> $(function () {

            CKEDITOR.replace('editorru0');
            CKEDITOR.replace('editorua0');
            CKEDITOR.replace('editoren0');
            CKEDITOR.replace('editorde0');
            @if ($record->type =='takehelp')
            CKEDITOR.replace('editorru2');
            CKEDITOR.replace('editorua2');
            CKEDITOR.replace('editoren2');
            CKEDITOR.replace('editorde2');

            @endif

            $("form").submit( function(e) {
                var title_ru = $("#title_ru").val();

                if( title_ru== '') {
                    $("#title_ru").css({ 'border-color': "red" });
                    e.preventDefault();
                    $(".nav-tabs .active").removeClass('active');
                    $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                    $(".tab-content .active").removeClass('active');
                    $(".tab-content #tab_0").addClass('active');

                }
                var messageLength_ru = CKEDITOR.instances['editorru0'].getData().replace(/<[^>]*>/gi, '').length;
                if( !messageLength_ru ) {
                    $("#cke_editorru0").css({ 'border-color': "red" });
                    $(".nav-tabs .active").removeClass('active');
                    $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                    $(".tab-content .active").removeClass('active');
                    $(".tab-content #tab_0").addClass('active');
                    e.preventDefault();
                }
            @if ($record->type =='takehelp')

            var messageLength_ru2 = CKEDITOR.instances['editorru2'].getData().replace(/<[^>]*>/gi, '').length;
                if( !messageLength_ru2 ) {
                    $("#cke_editorru2").css({ 'border-color': "red" });
                    $(".nav-tabs .active").removeClass('active');
                    $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                    $(".tab-content .active").removeClass('active');
                    $(".tab-content #tab_0").addClass('active');
                    e.preventDefault();
                }
            @endif


               if(( !messageLength_ru )||( !messageLength_ru2 )) {
                    alert( 'Название или текст новости не может быть пустой' );
                }
            });
            $('#title_ru').on('click',function(e) {
                $("#title_ru").css({'border-color': ""});
            });

            CKEDITOR.instances['editorru0'].on('change', function() {
                $("#cke_editorru0").css({'border-color': ""});
            });
            @if ($record->type =='takehelp')

            CKEDITOR.instances['editorru2'].on('change', function() {
                $("#cke_editorru2").css({'border-color': ""});
            });
            @endif
        })

    </script>

        <script> $('#selected_album').on('change',function(e){
                    e.preventDefault();
                    var albId = $('#selected_album option:selected').val();
                    load_photo(albId);

        });
        $(document).ready(function () {

            var albId = $('#selected_album :first').val();
            load_photo(albId);
        });


            function load_photo (albId) {
                $.ajax({
                    type: "GET",
                    url: '/admin/album/'+ albId ,
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: albId,
                    },
                    success: function(data){
                        $("#photos").empty();
                        $.each(data.response.photos, function(idx, photos){
                            $("#photos").append('<div class="col-sm-1 col-xs-4"> <a href="#" class="select_image" data-id="'+photos.id + '" data-icon="'+ photos.thumb_url + '"> <img style="width:80px;" src="' +photos.thumb_url + '"> </a> </div>');
                        });
                        console.log("ajaxdata",data);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        </script>
        <script>
            $(document).on('click', '.select_image', function(e) {
                e.preventDefault();
                $('#gallery').html('<div class="col-sm-3 col-xs-4 image-thumb""><img src="'+ $(this).data("icon") +'" class="selected_image" >' +
                        '<input type="hidden" name="photo_id" value="'+ $(this).data("id") +'"> </div>' );
            });

            $(document).on('click', '.image-thumb', function(e) {
                e.preventDefault();
                var imgId = $(this).data("id");
                $(this).remove();
            });
        </script>
@stop

