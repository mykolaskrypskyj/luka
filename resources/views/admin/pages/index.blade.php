@extends('adminlte::page')
@section('title', 'Страницы')
@section('content_header')
    <h1>Страницы</h1>
@stop
@section('content')

    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif
    <div class="">
        <div class="row">
            <div class="col-xs-12">
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <td>id</td>
                        <td>Тип</td>
                        <td>Название</td>
                        <td>Действие</td>
                    </tr>
                </thead>

                @foreach ($pages as $page)
                <tr>
                    <td>{{$page->id}}</td>
                    <td>{{$page->type}}</td>
                    <td>{{$page->title_ru}}</td>
                    <td style="display:inline-flex;">
                         <a href="/admin/pages/{{$page->id}}/edit"><i class="fa fa-pencil"></i></a> &nbsp; &nbsp;
                         <form style="display:none;"  method="POST" action="{{action('Admin\PagesController@destroy',['articles'=>$page->id])}}">
                             <input type="hidden" name="_method" value="delete"/>
                             <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                             <button type="submit"><i class="fa fa-trash"></i></button>
                         </form>
                    </td>
                </tr>
                @endforeach
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div id="table-4_paginate" class="dataTables_paginate paging_simple_numbers">
                    {{ $pages->links() }}
            </div>
        </div>
    </div>
@endsection


@section('js')
    <style>
        button {
            border:0;
            background:none;
            color: #3c8dbc;
        }
    </style>
@stop