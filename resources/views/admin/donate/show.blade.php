@extends('adminlte::page')
@section('title', 'Создать ')
@section('content_header')
@if( isset($record) ) <!-- Is for Edit/update -->
<h1>Просмотр пожертвования</h1>
@else <!-- ...For Create/store -->
<h1>Создать</h1>
@endif
@stop
@section('content')
    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif

    <div class="box box-default">
        <div class="box-header with-border">
        </div>
        <div class="box-body">
            <div class="form-group">
                <label> Назначение:</label>
                <p>{{$children->name}}</p>
            </div>
            <div class="form-group">
                <label> Сума :</label>
                <p>{{$record->sum}}</p>
            </div>
            @if( isset($record) ) <!-- Is for Edit/update -->
            <div class="form-group">
                <label>Канал поступления:</label>
                <p>{{$record->channel}}</p>
            </div>
            <div class="form-group">
                <label>Время поступления:</label>
                <p>{{$record->created_at}}</p>
            </div>
            @else <!-- ...For Create/store -->
            @endif


            <div class="form-group">
                <label>Статус:</label>
                <p>{{$record->status}}</p>

            </div>




        </div>
    </div>
@endsection
