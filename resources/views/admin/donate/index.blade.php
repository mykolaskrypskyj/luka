@extends('adminlte::page')
@section('title', 'Пожертвования')
@section('content_header')
    <h1>Пожертвования</h1>
    <div class="breadcrumb"><button type="button" onclick="location.href='/admin/donate/create'" class="btn btn-block btn-primary">Создать</button> </div>
@stop
@section('content')

    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif
    <div class="">
        <div class="row">
            <div class="col-xs-12">
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <td>id</td>
                        <td>Назначение</td>
                        <td>Сума</td>
                        <td>Канал</td>
                        <td>Статус</td>
                        <td>Дата</td>
                        <td>Действие</td>
                    </tr>
                </thead>

                </table>
            </div>
        </div>
    </div>


@endsection


@section('css')
    <style>
        button {
            border:0;
            background:none;
            color: #3c8dbc;
        }
    </style>
@stop

@section('js')
    <script>
        $(document).ready(function () {
            $('#example1').dataTable({
                processing: true,
                order: [ [0, 'desc'] ],
                scrollY:        "700px",
                scrollCollapse: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Поиск"
                },
                autowidth: false,
                lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
                ajax: '{!! route('get.donate.data') !!}',

                columns: [
                    { data: 'id'},
                    { data: 'child'},
                    { data: 'sum'},
                    { data: 'channel'},
                    { data: 'status'},
                    { data: 'created_at'},
                    { data: 'edit', name: 'edit', orderable: false, searchable: false }
                ]
            });
        });
    </script>
@stop
