@extends('adminlte::page')
@section('title', 'Создать ')
@section('content_header')
    @if( isset($record) ) <!-- Is for Edit/update -->
    <h1>Редактировать</h1>
    @else <!-- ...For Create/store -->
    <h1>Создать</h1>
    @endif
@stop
@section('content')
    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif

    <div class="box box-default">
        <div class="box-header with-border">
        </div>
        <div class="box-body">
            @if( isset($record) ) <!-- Is for Edit/update -->
            {{ Form::model( $record, ['url' => 'admin/donate/'.$record->id, 'method' => 'put', 'role' => 'form'] ) }}
            @else <!-- ...For Create/store -->
            {{ Form::open( array('url' => 'admin/donate/')) }}
            @endif
            <div class="form-group">
                <label> Выберите назначение:</label>
                {{Form::select('children_id', $children, null, ['class'=>'form-control', 'id'=> 'selected_album' ]  )}}
            </div>
            <div class="form-group">
                <label> Сума :</label>
                {{ Form::number('sum',null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'sum','required' ]) }}
            </div>
            @if( isset($record) ) <!-- Is for Edit/update -->
            <div class="form-group">
                <label>Канал поступления:</label>
                <p>{{$record->channel}}</p>
            </div>
            <div class="form-group">
                <label>Время поступления:</label>
                <p>{{$record->created_at}}</p>
            </div>
            @else <!-- ...For Create/store -->
            @endif


            <div class="form-group">
                <label>Статус:</label>

                {{Form::select('status',['1' => 'Да', '0' => 'Нет'])}}
            </div>


                    @if( !isset($record) )
                        {{ Form::button('Создать', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @else
                        {{ Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @endif

                    {{ Form::close() }}
        </div>
    </div>
@endsection

@section('js')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script> CKEDITOR.timestamp= Date.now(); </script>

    <script> $(function () {

            CKEDITOR.replace('editor0');
            CKEDITOR.replace('editor1');
            CKEDITOR.replace('editor2');
            CKEDITOR.replace('editor3');


            $("form").submit( function(e) {
                var title_ru = $("#title_ru").val();

                if( title_ru== '') {
                    $("#title_ru").css({ 'border-color': "red" });
                    e.preventDefault();
                    $(".nav-tabs .active").removeClass('active');
                    $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                    $(".tab-content .active").removeClass('active');
                    $(".tab-content #tab_0").addClass('active');

                }
                var messageLength_ru = CKEDITOR.instances['editor0'].getData().replace(/<[^>]*>/gi, '').length;
                if( !messageLength_ru ) {
                    $("#cke_editor0").css({ 'border-color': "red" });
                    $(".nav-tabs .active").removeClass('active');
                    $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                    $(".tab-content .active").removeClass('active');
                    $(".tab-content #tab_0").addClass('active');
                    e.preventDefault();
                }


                if( !messageLength_ru ) {
                    alert( 'Название или текст новости не может быть пустой' );
                }

                var preview = $('#gallery input[name="images[]"]').val();
                if( !preview ) {
                    alert( 'В страницы должна быть миниатюра. Добавте ее пожалуйста' );
                    e.preventDefault();
                }
            });
            $('#title_ru').on('click',function(e) {
                $("#title_ru").css({'border-color': ""});
            });

            CKEDITOR.instances['editor0'].on('change', function() {
                $("#cke_editor0").css({'border-color': ""});
            });

        })

    </script>

        <script> $('#selected_album').on('change',function(e){
                    e.preventDefault();
                    var albId = $('#selected_album option:selected').val();
                    load_photo(albId);

        });
        $(document).ready(function () {

            var albId = $('#selected_album :first').val();
            load_photo(albId);
        });


            function load_photo (albId) {
                $.ajax({
                    type: "GET",
                    url: '/admin/album/'+ albId ,
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: albId,
                    },
                    success: function(data){
                        $("#photos").empty();
                        $.each(data.response.photos, function(idx, photos){
                            $("#photos").append('<div class="col-sm-1 col-xs-4"> <a href="#" class="select_image" data-id="'+photos.id + '" data-icon="'+ photos.thumb_url + '"> <img style="width:80px;" src="' +photos.thumb_url + '"> </a> </div>');
                        });
                        console.log("ajaxdata",data);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        </script>
        <script>
            $(document).on('click', '.select_image', function(e) {
                e.preventDefault();
                $('#gallery').append('<div class="col-sm-3 col-xs-4 image-thumb""><img src="'+ $(this).data("icon") +'" class="selected_image" >' +
                        '<input type="hidden" name="images[]" value="'+ $(this).data("id") +'"> </div>' );
            });

            $(document).on('click', '.image-thumb', function(e) {
                e.preventDefault();
                var imgId = $(this).data("id");
                $(this).remove();
            });
        </script>
@stop

