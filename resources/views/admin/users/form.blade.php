@extends('adminlte::page')
@section('title', 'Создать/редактировать пользователя')
@section('content_header')
    @if( isset($record) ) <!-- Is for Edit/update -->
    <h1>Редактировать</h1>
    @else <!-- ...For Create/store -->
    <h1>Создать</h1>
    @endif
@stop
@section('content')
    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif

    <div class="box box-default">
        <div class="box-header with-border">
        </div>
        <div class="box-body">
            @if( isset($record) ) <!-- Is for Edit/update -->
            {{ Form::model( $record, ['url' => 'admin/users/'.$record->id, 'method' => 'put', 'role' => 'form'] ) }}
            @else <!-- ...For Create/store -->
            {{ Form::open( array('url' => 'admin/users/')) }}
            @endif
            <div class="form-group">
                <label> Имя:</label>
                {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter ... ', 'required']) }}
            </div>
            <div class="form-group">
                <label>Email :</label>
                @if( isset($record) ) <!-- Is for Edit/update -->
                <p>{{ $record->email}}</p>
                @else <!-- ...For Create/store -->
                {{ Form::email('email',null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=>'email','required']) }}
                @endif
            </div>

            <div class="form-group">
                <label> Пароль:</label>
                @if( isset($record) ) <!-- Is for Edit/update -->
                {{ Form::password('password_form', ['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'password_form'  ]) }}
                @else <!-- ...For Create/store -->
                {{ Form::password('password_form', ['class'=>'form-control','placeholder'=>'Enter ... ','required','id'=> 'password_form'  ]) }}
                @endif
            </div>
            <div class="form-group">
                <label> Повторите пароль:</label>
                @if( isset($record) ) <!-- Is for Edit/update -->
                {{ Form::password('password_repeat', ['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'password_repeat'  ]) }}
                @else <!-- ...For Create/store -->
                {{ Form::password('password_repeat', ['class'=>'form-control','placeholder'=>'Enter ... ','required','id'=> 'password_repeat'  ]) }}
                @endif

            </div>
                    @if( !isset($record) )
                        {{ Form::button('Создать', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @else
                        {{ Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @endif

                    {{ Form::close() }}
        </div>
    </div>
@endsection

@section('js')
    <script> $(function () {

            $("form").submit( function(e) {
                var password_form = $("#password_form").val();
                var password_repeat = $("#password_repeat").val();

                if( password_form != password_repeat) {
                    e.preventDefault();
                    $("#password_form").css({ 'border-color': "red" });
                    $("#password_repeat").css({ 'border-color': "red" });
                    $("#password_form").val("");
                    $("#password_repeat").val("");
                    alert('Пароли не совпадают');
                }
                if((( password_form.length <6 )&&(password_form.length >1 )) || (( password_repeat.length <6)&&( password_repeat.length >1)) {
                    e.preventDefault();
                    $("#password_form").css({ 'border-color': "red" });
                    $("#password_repeat").css({ 'border-color': "red" });
                    $("#password_form").val("");
                    $("#password_repeat").val("");
                    alert('Пароль должен быть не меньше 6 символов');
                }
            });
            $('#password_form').on('click',function(e) {
                $("#password_form").css({'border-color': ""});
            });
            $('#password_repeat').on('click',function(e) {
                $("#password_repeat").css({'border-color': ""});
            });

        });

    </script>
@stop

