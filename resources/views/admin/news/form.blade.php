@extends('adminlte::page')
@section('title', 'Создать новость')
@section('css')
    <link href="/css/cropper.min.css" rel="stylesheet">
    <style>
        #gallery_preview_img{
            max-height: 250px;
        }

    </style>
    @stop

@section('content_header')
    @if( isset($record) ) <!-- Is for Edit/update -->
    <h1>Редактировать</h1>
    @else <!-- ...For Create/store -->
    <h1>Создать</h1>
    @endif
@stop
@section('content')
    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif

    <div class="box box-default">
        <div class="box-header with-border">
        </div>
        <div class="box-body">
            @if( isset($record) ) <!-- Is for Edit/update -->
            {{ Form::model( $record, ['url' => 'admin/news/'.$record->id, 'method' => 'put', 'role' => 'form'] ) }}
            @else <!-- ...For Create/store -->
            {{ Form::open( array('url' => 'admin/news/')) }}
            @endif

            <div class="form-group">
                <label> Превью:</label>
                @if( isset($record) ) <!-- Is for Edit/update -->
                <div class="content" style="min-height: 0px;">
                    <div id="gallery_preview" class="row">
                        <img src="{{$record->preview_photo}}" id="gallery_preview_img" class="selected_preview" >
                        <input type="hidden" name="preview[id]" id="preview_id" value="{{$record->preview_id}}">
                        <input type="hidden" name="preview[detail_x]" id="detail_x">
                        <input type="hidden" name="preview[detail_y]" id="detail_y">
                        <input type="hidden" name="preview[detail_width]" id="detail_width">
                        <input type="hidden" name="preview[detail_height]" id="detail_height">
                    </div>
                </div>
                @else <!-- ...For Create/store -->
                <div class="content" style="min-height: 0px;">
                    <div id="gallery_preview" class="row">
                        <img src="" id="gallery_preview_img" class="selected_preview" >
                        <input type="hidden" name="preview[id]" id="preview_id">
                        <input type="hidden" name="preview[detail_x]" id="detail_x">
                        <input type="hidden" name="preview[detail_y]" id="detail_y">
                        <input type="hidden" name="preview[detail_width]" id="detail_width">
                        <input type="hidden" name="preview[detail_height]" id="detail_height">
                    </div>
                </div>
                @endif
            </div>
            <div class="form-group">
                <label> Выберите альбом:</label><br>
                {{Form::select('albums', $albums, null, ['class'=>'form-control', 'id'=> 'selected_preview_alb' ]  )}}<br>
                <label> Выберите изображения:</label>
                <div class="content" style="min-height: 0px;">
                    <div id="prew-photos" class="row"></div>
                </div>
            </div>
            <hr>


            <div class="form-group">
                    <label> Галерея:</label>
                    @if( isset($record) ) <!-- Is for Edit/update -->
                    <div class="content" style="min-height: 0px;">
                        <div id="gallery" class="row">
                    <?php $list = $record->photos;?>
                             @foreach($list as $item)
                            <div class="col-sm-3 col-xs-4 image-thumb" >
                                <img src="{{$item->icon_url}}" class="selected_image" >
                                <input type="hidden" name="images[]" value="{{$item->id}}">
                            </div>

                            @endforeach
                        </div>
                    </div>
                    @else <!-- ...For Create/store -->
                    <div class="content" style="min-height: 0px;">
                        <div id="gallery" class="row"></div>
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label> Выберите альбом:</label>
                    {{Form::select('albums', $albums, null, ['class'=>'form-control', 'id'=> 'selected_album' ]  )}}
                    <label> Выберите изображения:</label>
                    <div class="content" style="min-height: 0px;">
                        <div id="photos" class="row"></div>
                    </div>
                </div>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" style="text-transform: uppercase;">
                    @foreach ($locales as $count => $locale)
                        <li class="{{$count == 0 ? ' active' : ''}}">
                            <a href="#tab_{{$count}}" data-toggle="tab" aria-expanded="true">{{$locale}}</a>
                        </li>

                    @endforeach
                </ul>
            </div>
            <div class="tab-content">

                @foreach ($locales as $count => $locale)
                    <div class="tab-pane {{$count == 0 ? ' active' : ''}}" id="tab_{{$count}}">
                        <div class="form-group">
                            <label> Название новости :</label>
                            {{ Form::text('title_'.$locale,null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'title_'.$locale ]) }}
                        </div>
                        <div class="form-group">
                            <label> Полный текст новости:</label>
                            <div class="box-body pad">
                                {{ Form::textarea('content_'.$locale, null,['class'=>'form-control','id'=>'editor'.$count,'placeholder'=>'Enter ... ','rows'=>'10','cols'=>'80' ]) }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
                    <div class="form-group">
                        <label>Опубликовать?</label>

                        {{Form::select('public',['1' => 'Да', '0' => 'Нет'], null, [ 'id'=> 'public_select' ])}}
                    </div>
                    @if( !isset($record) )
                        {{ Form::button('Создать', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @else
                        {{ Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @endif

                    {{ Form::close() }}
        </div>
    </div>
@endsection

@section('js')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script> CKEDITOR.timestamp= Date.now(); </script>

    <script> $(function () {

            CKEDITOR.replace('editor0');
            CKEDITOR.replace('editor1');
            CKEDITOR.replace('editor2');
            CKEDITOR.replace('editor3');


            $("form").submit( function(e) {
                var title_ru = $("#title_ru").val();

                if( title_ru== '') {
                    $("#title_ru").css({ 'border-color': "red" });
                    e.preventDefault();
                    $(".nav-tabs .active").removeClass('active');
                    $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                    $(".tab-content .active").removeClass('active');
                    $(".tab-content #tab_0").addClass('active');

                }
                if ($('#public_select option:checked').val() == '1') {

                    var messageLength_ru = CKEDITOR.instances['editor0'].getData().replace(/<[^>]*>/gi, '').length;
                    if (!messageLength_ru) {
                        $("#cke_editor0").css({'border-color': "red"});
                        $(".nav-tabs .active").removeClass('active');
                        $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                        $(".tab-content .active").removeClass('active');
                        $(".tab-content #tab_0").addClass('active');
                        e.preventDefault();
                    }


                    if (!messageLength_ru) {
                        alert('Название или текст новости не может быть пустой');
                    }

                    var preview = $('#gallery input[name="images[]"]').val();
                    if (!preview) {
                        alert('В страницы должна быть миниатюра. Добавте ее пожалуйста');
                        e.preventDefault();
                    }

                }
            });
            $('#title_ru').on('click',function(e) {
                $("#title_ru").css({'border-color': ""});
            });

            CKEDITOR.instances['editor0'].on('change', function() {
                $("#cke_editor0").css({'border-color': ""});
            });

        })

    </script>

        <script> $('#selected_album').on('change',function(e){
                    e.preventDefault();
                    var albId = $('#selected_album option:selected').val();
                    load_photo(albId);

        });
        $(document).ready(function () {

            var albId = $('#selected_album :first').val();
            load_photo(albId);
        });


            function load_photo (albId) {
                $.ajax({
                    type: "GET",
                    url: '/admin/album/'+ albId ,
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: albId,
                    },
                    success: function(data){
                        $("#photos").empty();
                        $.each(data.response.photos, function(idx, photos){
                            $("#photos").append('<div class="col-sm-1 col-xs-4"> <a href="#" class="select_image" data-id="'+photos.id + '" data-icon="'+ photos.thumb_url + '"> <img style="width:80px;" src="' +photos.thumb_url + '"> </a> </div>');
                        });
                        console.log("ajaxdata",data);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        </script>
        <script>
            $(document).on('click', '.select_image', function(e) {
                e.preventDefault();
                if ($('#gallery .image-thumb').length <11) {
                    $('#gallery').append('<div class="col-sm-3 col-xs-4 image-thumb""><img src="' + $(this).data("icon") + '" class="selected_image" >' +
                            '<input type="hidden" name="images[]" value="' + $(this).data("id") + '"> </div>');
                } else {
                    alert( 'Нельзя добавить более 11 изображений в слайдер' );
                }
            });

            $(document).on('click', '.image-thumb', function(e) {
                e.preventDefault();
                var imgId = $(this).data("id");
                $(this).remove();
            });
        </script>

    <script src="/js/cropper.min.js"></script>

    <script> $('#selected_preview_alb').on('change',function(e){
            e.preventDefault();
            var albIdPrew = $('#selected_preview_alb option:selected').val();
            load_preview(albIdPrew);

        });
        $(document).ready(function () {

            var albIdPrew = $('#selected_preview_alb :selected').val();
            load_preview(albIdPrew);
        });


        function load_preview (albIdPrew) {
            $.ajax({
                type: "GET",
                url: '/admin/album/'+ albIdPrew ,
                data: {
                    _token: '{{ csrf_token() }}',
                    id: albIdPrew,
                },
                success: function(data){
                    $("#prew-photos").empty();
                    $.each(data.response.photos, function(idx, photos){
                        $("#prew-photos").append('<div class="col-sm-1 col-xs-4"> <a href="#" class="select_preview" data-id="'+photos.id + '" data-icon="'+ photos.thumb_url + '" data-url="'+ photos.url + '"> <img style="width:80px;" src="' +photos.thumb_url + '"> </a> </div>');
                    });
                    console.log("ajaxdata",data);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        }
    </script>
    <script>
        var $image = $("#gallery_preview_img");

        $(document).on('click', '.select_preview', function(e) {
            e.preventDefault();
            $('#modal-default').show();
//            console.log($(this).data());
            $('#gallery_preview_img').attr('src', $(this).data("url"));
            $('#preview_id').val($(this).data("id"));
            console.log($image);
            originalData = {};
            $image.cropper({
                viewMode: 1,
                aspectRatio: 278/120,
                resizable: true,
                zoomable: false,
                rotatable: false,
                multiple: true,
                crop: function(event) {
                    $("#detail_x").val(Math.round(event.detail.x));
                    $("#detail_y").val(Math.round(event.detail.y));
                    $("#detail_width").val(Math.round(event.detail.width));
                    $("#detail_height").val(Math.round(event.detail.height));
                    console.log(event.detail.x);
                    console.log(event.detail.y);
                    console.log(event.detail.width);
                    console.log(event.detail.height);
                    console.log(event.detail.scaleX);
                    console.log(event.detail.scaleY);
                },
                dragend: function(data) {
                    originalData = $image.cropper("getCroppedCanvas");
                    console.log(originalData.toDataURL());
                    $('.data-url').text(originalData.toDataURL());
                }
            });

            var cropper = $image.data('cropper');
//            console.log(cropper);
            cropper.replace($(this).data("url"));
        });
    </script>

@stop

