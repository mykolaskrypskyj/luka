@extends('adminlte::page')
@section('title', 'Менеджер изображений')
@section('css')
    <link rel="stylesheet" href="/css/gallery.css">
    <link rel="stylesheet" href="/css/lightbox.min.css">

@stop
@section('content_header')
    <h1 >Альбом - {{$record->title}}</h1>
@stop
@section('content')
	<div class="gallery-env">
		<div class="row">
<?php $list = $record->photos;?>                            
@foreach($list as $item)
				<div class="col-sm-2 col-xs-4" data-tag="1d">
					<article class="image-thumb">
						<a href="{{$item->url}}" class="image example-image-link" data-lightbox="example-1">
							<img class="example-image" src="{{$item->icon_url}}" alt="{{$item->name}}" title="{{$item->name}}"/>
						</a>
						<div class="image-options">
							<!--a href="#" class="edit"><i class="entypo-pencil"></i></a-->
							<a href="/admin/photos/delete/{{$item->id}}" class="delete ajax" data-id="{{$item->id}}"><i class="fa fa-trash"></i></a>
						</div>
					</article>
				</div>
@endforeach
				
		</div>
		
	</div>
		
		
		
		<hr />
		
		<h3>
			Upload More Images
			<br />
			<small>The upload script will generate random response status (error or success), files are not uploaded.</small>
		</h3>
		
		<br />
			{{ Form::open( ['url' => '/admin/photos','class'=>'dropzone dz-min','id'=>'dropzone_example']) }}
			
			<input name="album_id" type="hidden" value="{{$record->id}}"/>
			<div class="fallback">
				<input name="file" type="file" multiple accept="image/jpeg,image/png,image/gif"/>
			</div>
                        {{ Form::close() }}
		
		<div id="dze_info" class="hidden">
			
			<br />
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="panel-title">Dropzone Uploaded Images Info</div>
				</div>
				
				<table class="table table-bordered">
					<thead>
						<tr>
							<th width="40%">File name</th>
							<th width="15%">Size</th>
							<th width="15%">Type</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>
						<tr>
							<td colspan="4"></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
                

<!-- Album Cover Settings Modal -->
<div class="modal fade custom-width" id="album-cover-options">
	<div class="modal-dialog" style="width: 65%;">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit: <strong>Album Title</strong></h4>
			</div>
			
			<div class="modal-body">
				
				<div class="row">
					<div class="col-sm-6">
										
						<div class="row">
							<div class="col-md-12">
								
								<h4 class="margin-top-none">Crop Image</h4>
								
								<div class="croppable-image">
									<img src="/assets/images/sample-crop.jpg" class="img-responsive" />
								</div>
								
							</div>
						</div>
						
					</div>
					
					<div class="col-sm-6">
					
						<div class="row">
							<div class="col-md-12">
								
								<div class="form-group">
									<label for="field-1" class="control-label">Title</label>
									
									<input type="text" class="form-control" id="field-1" placeholder="Enter album title">
								</div>	
								
							</div>
						</div>
					
						<div class="row">
							<div class="col-md-12">
								
								<div class="form-group">
									<label for="field-1" class="control-label">Tags</label>
									
									<input type="text" class="form-control" id="field-3" placeholder="Tags">
								</div>	
								
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								
								<div class="form-group">
									<label for="field-1" class="control-label">Description</label>
									
									<textarea class="form-control autogrow" id="field-2" placeholder="Enter album description" style="min-height: 120px;"></textarea>
								</div>	
								
							</div>
						</div>
						
					</div>
				</div>
				
				
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-icon">
					<i class="entypo-check"></i>
					Apply Changes
				</button>
			</div>
		</div>
	</div>
</div>


<!-- Album Image Settings Modal -->
<div class="modal fade" id="album-image-options">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="gallery-image-edit-env">
				<img src="/assets/images/sample-crop-2.png" class="img-responsive" />
				
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			
			<div class="modal-body">
			
					
						<div class="row">
							<div class="col-md-12">
								
								<div class="form-group">
									<label for="field-1" class="control-label">Title</label>
									
									<input type="text" class="form-control" id="field-1" placeholder="Enter image title">
								</div>	
								
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								
								<div class="form-group">
									<label for="field-1" class="control-label">Description</label>
									
									<textarea class="form-control autogrow" id="field-2" placeholder="Enter image description" style="min-height: 80px;"></textarea>
								</div>	
								
							</div>
						</div>
						
				
				
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-icon">
					<i class="entypo-check"></i>
					Apply Changes
				</button>
			</div>
		</div>
	</div>
</div>
                
@stop

@section('js')
<script type="text/javascript">
		jQuery(document).ready(function($)
		{
			$(".gallery-env").on("click", ".image-thumb .image-options a.delete", function(ev)
			{
				ev.preventDefault();
				
				
				var $image = $(this).closest('[data-tag]');
					
				var t = new TimelineLite({
					onComplete: function()
					{
						$image.slideUp(function()
						{
							$image.remove();
						});
					}
				});
				
				$image.addClass('no-animation');
				
				t.append( TweenMax.to($image, .2, {css: {scale: 0.95}}) );
				t.append( TweenMax.to($image, .5, {css: {autoAlpha: 0, transform: "translateX(100px) scale(.95)"}}) );
				
			}).on("click", ".image-thumb .image-options a.edit", function(ev)
			{
				ev.preventDefault();
				
				// This will open sample modal
				$("#album-image-options").modal('show');
				
				// Sample Crop Instance
				var image_to_crop = $("#album-image-options img"),
					img_load = new Image();
				
				img_load.src = image_to_crop.attr('src');
				img_load.onload = function()
				{
					if(image_to_crop.data('loaded'))
						return false;
						
					image_to_crop.data('loaded', true);
					
					image_to_crop.Jcrop({
						//boxWidth: $("#album-image-options").outerWidth(),
						boxWidth: 580,
						boxHeight: 385,
						onSelect: function(cords)
						{
							// you can use these vars to save cropping of the image coordinates
							var h = cords.h,
								w = cords.w,
								
								x1 = cords.x,
								x2 = cords.x2,
								
								y1 = cords.w,
								y2 = cords.y2;
							
						}
					}, function()
					{
						var jcrop = this;
						
						jcrop.animateTo([600, 400, 100, 150]);
					});
				}
			});

			$('.delete').click(function(e){
				e.preventDefault();
				var imgId = $(this).data("id");

				$.ajax({
					type: "POST",
					url: '/admin/photos/'+ imgId,
					data: {
						_token: '{{ csrf_token() }}',
						_method:'delete'
					},
					success: function(data){
						console.log("ajaxdata",data);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log(JSON.stringify(jqXHR));
						console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
					}
				});
			});


			// Sample Filtering
			var all_items = $("div[data-tag]"),
				categories_links = $(".image-categories a");
			
			categories_links.click(function(ev)
			{
				ev.preventDefault();
				
				var $this = $(this),
					filter = $this.data('filter');
				
				categories_links.removeClass('active');
				$this.addClass('active');
				
				all_items.addClass('not-in-filter').filter('[data-tag="' + filter + '"]').removeClass('not-in-filter');
				
				if(filter == 'all' || filter == '*')
				{
					all_items.removeClass('not-in-filter');
					return;
				}
			});
			
		});
		Dropzone.options.dropzone_example = {
			acceptedFiles: "image/jpeg,image/png,image/gif"
		}
		</script>
        <script src="/assets/js/gsap/TweenMax.min.js" id="script-resource-1"></script>
        <link rel="stylesheet" href="/assets/js/jcrop/jquery.Jcrop.min.css">
		<link rel="stylesheet" href="/assets/js/dropzone/dropzone.css">
        <script src="/assets/js/jcrop/jquery.Jcrop.min.js"></script>
		<script src="/js/lightbox-plus-jquery.min.js"></script>
		<script src="/assets/js/dropzone/dropzone.js"></script>








@endsection
