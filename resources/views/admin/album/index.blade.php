@extends('adminlte::page')
@section('title', 'Менеджер изображений')
@section('css')
    <link rel="stylesheet" href="/css/gallery.css">

@stop
@section('content_header')
    <h1 >Галерея</h1>
    <div class="breadcrumb"><a type="button" href="/admin/album/create" class="btn btn-block btn-primary">Создать альбом</a> </div>
@stop
@section('content')

    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif

    <div class="">
        <div class="row">
            <div class="col-xs-12">
                <hr>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <td>id</td>
                        <td>Миниатюра</td>
                        <td>Название</td>
                        <td>Действие</td>
                    </tr>
                    </thead>


                </table>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $('#example1').dataTable({
                processing: true,
                order: [ [0, 'desc'] ],
                scrollY:        "700px",
                scrollCollapse: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Поиск"
                },
                autowidth: false,
                lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
                ajax: '{!! route('get.album.data') !!}',

                columns: [
                    { data: 'id', width: '10%'},
                    { data: 'img', width: '20%'},
                    { data: 'name', width: '40%'},
                    { data: 'edit', name: 'edit', orderable: false, searchable: false }
                ]
            });
        });
    </script>
@stop
