@extends('adminlte::page')
@section('title', 'Создать альбом')

@section('content_header')
    <h1>Создать альбом</h1>
    <div class="breadcrumb"><a type="button" href="/admin/album" class="btn btn-block btn-primary ">Назад</a> </div>
@stop

@section('content')
        <div class="box box-default">
            <div class="box-header with-border">
            </div>
            <div class="box-body">
		@if( isset($record) ) <!-- Is for Edit/update -->
			{{ Form::model( $record, ['url' => 'admin/album/'.$record->id, 'method' => 'put', 'role' => 'form'] ) }}
		@else <!-- ...For Create/store -->
			{{ Form::open( array('url' => 'admin/album')) }}
		@endif
                    <div class="form-group">
                        <label> Название альбома:</label>
                        {{ Form::text('title',null,['class'=>'form-control','placeholder'=>'Enter ... ','required'  ]) }}
                    </div>
		@if( !isset($record) ) 
			{{ Form::button('Создать', ['type' => 'submit', 'class' => 'btn btn-block btn-primary ']) }}
		@else
			{{ Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-block btn-primary ']) }}
		@endif	

		{{ Form::close() }}
            </div>
@endsection
