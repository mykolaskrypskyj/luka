@extends('adminlte::page')
@section('title', 'Создать страницу пожертвований')
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="/css/cropper.min.css" rel="stylesheet">
<style>
    #gallery_preview_img{
        max-height: 420px;
    }

</style>
@stop
@section('content_header')
    @if( isset($record) ) <!-- Is for Edit/update -->
    <h1>Редактировать страницу ребенка</h1>
    @else <!-- ...For Create/store -->
    <h1>Создать страницу ребенка</h1>
    @endif
@stop
@section('content')
    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif

    <div class="box box-default">
        <div class="box-header with-border">
         </div>
        <div class="box-body">
            @if( isset($record) ) <!-- Is for Edit/update -->
            {{ Form::model( $record, ['url' => 'admin/children/'.$record->id, 'method' => 'put', 'role' => 'form'] ) }}
            @else <!-- ...For Create/store -->
            {{ Form::open( array('url' => 'admin/children/')) }}
            @endif

                <div class="form-group">
                    <label> Превью:</label>
                    @if( isset($record) ) <!-- Is for Edit/update -->
                    <div class="content" style="min-height: 0px;">
                        <div id="gallery_preview" class="row">
                            <img src="{{$record->preview_photo}}" id="gallery_preview_img" class="selected_preview" >
                            <input type="hidden" name="preview[id]" id="preview_id">
                            <input type="hidden" name="preview[detail_x]" id="detail_x">
                            <input type="hidden" name="preview[detail_y]" id="detail_y">
                            <input type="hidden" name="preview[detail_width]" id="detail_width">
                            <input type="hidden" name="preview[detail_height]" id="detail_height">
                        </div>
                    </div>
                    @else <!-- ...For Create/store -->
                    <div class="content" style="min-height: 0px;">
                        <div id="gallery_preview" class="row">
                            <img src="{{$item->preview_photo}}" id="gallery_preview_img" class="selected_preview" >
                            <input type="hidden" name="preview[id]" value="{{$item->id}}">
                            <input type="hidden" name="preview[detail_x]" id="detail_x">
                            <input type="hidden" name="preview[detail_y]" id="detail_y">
                            <input type="hidden" name="preview[detail_width]" id="detail_width">
                            <input type="hidden" name="preview[detail_height]" id="detail_height">
                        </div>
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label> Выберите альбом:</label><br>
                    {{Form::select('albums', $albums, $record->album_id, ['class'=>'form-control', 'id'=> 'selected_preview_alb' ,'style'=>'width:60%; display: inline-block;margin-right:15px;']  )}} <a style="padding-right:10px" href="#" id="refresh-select-prev"><i class="fa fa-refresh"></i></a>   <a target="_blank" href="/admin/album/{{$record->album_id}}">К прикрепленному альбому</a><br>
                    <label> Выберите изображения:</label>
                    <div class="content" style="min-height: 0px;">
                        <div id="prew-photos" class="row"></div>
                    </div>
                </div>
<hr>


                <div class="form-group">
                    <label> Галлерея:</label>
                    @if( isset($record) ) <!-- Is for Edit/update -->
                    <div class="content" style="min-height: 0px;">
                        <div id="gallery" class="row">
                    <?php $list = $record->photos;?>
                             @foreach($list as $item)
                            <div class="col-sm-3 col-xs-4 image-thumb" >
                                <img src="{{$item->icon_url}}" class="selected_image" >
                                <input type="hidden" name="images[]" value="{{$item->id}}">
                            </div>

                            @endforeach
                        </div>
                    </div>
                    @else <!-- ...For Create/store -->
                    <div class="content" style="min-height: 0px;">
                        <div id="gallery" class="row"></div>
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <label> Выберите альбом:</label><br>
                    {{Form::select('albums', $albums, $record->album_id, ['class'=>'form-control', 'id'=> 'selected_album' ,'style'=>'width:60%; display: inline-block;margin-right:15px;']  )}} <a style="padding-right:10px" href="#" id="refresh-select"><i class="fa fa-refresh"></i></a>   <a target="_blank" href="/admin/album/{{$record->album_id}}">К прикрепленному альбому</a><br>
                    <label> Выберите изображения:</label>
                    <div class="content" style="min-height: 0px;">
                        <div id="photos" class="row"></div>
                    </div>
                </div>

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" style="text-transform: uppercase;">
                    @foreach ($locales as $count => $locale)
                        <li class="{{$count == 0 ? ' active' : ''}}">
                            <a href="#tab_{{$count}}" data-toggle="tab" aria-expanded="true">{{$locale}}</a>
                        </li>

                    @endforeach
                </ul>
            </div>
            <div class="tab-content">

                @foreach ($locales as $count => $locale)
                    <div class="tab-pane  {{$count == 0 ? ' active' : ''}}" id="tab_{{$count}}" id="tab_{{$count}}">
                        <div class="form-group">
                            <label> ФИО :</label>
                            {{ Form::text('name_'.$locale,null,['class'=>'form-control','placeholder'=>'Enter ... ','id'=> 'name_'.$locale ]) }}
                        </div>

                        <div class="form-group">
                            <label> Детальное описание:</label>
                            <div class="box-body pad">
                                {{ Form::textarea('details_'.$locale, null,['class'=>'form-control','id'=>'editor'.$count,'placeholder'=>'Enter ... ','rows'=>'10','cols'=>'80' ]) }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="form-group">
                <label>Город:</label>
                {{Form::select('city_id', $cities, null, ['class'=>'form-control']  )}}
            </div>
            <div class="form-group">
                <label> Дата рождения :</label>
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {{ Form::text('born', isset($record->born) ? $record->born->toDateString() : null ,['class'=>'form-control pull-right','id'=> 'datepicker']) }}
                </div>

            </div>
            <div class="form-group">
                <label> Суму которую надо собрать :</label>
                {{ Form::text('sum_need',null,['class'=>'form-control','placeholder'=>'Enter ... ', 'id'=>'sum_need']) }}
            </div>
            <div class="form-group">
                <label> Cума уже собрана :</label>
                 {{Form::checkbox('collected_flag', null, $record->collected_flag, ['data-toggle' => 'toggle']) }}
            </div>

            <div class="form-group">
                <label> Документы:</label>
                @if( isset($record) ) <!-- Is for Edit/update -->
                <div class="content" style="min-height: 0px;">
                    <div id="gallery_doc" class="row">
                        <?php $list = $record->documents;?>
                        @foreach($list as $item)
                            <div class="col-sm-3 col-xs-4 image-thumb" >
                                <img src="{{$item->icon_url}}" class="selected_doc" >
                                <input type="hidden" name="documents[]" value="{{$item->id}}">
                            </div>

                        @endforeach
                    </div>
                </div>
                @else <!-- ...For Create/store -->
                <div class="content" style="min-height: 0px;">
                    <div id="gallery_doc" class="row"></div>
                </div>
                @endif
            </div>
            <div class="form-group">
                <label> Выберите альбом:</label><br>
                {{Form::select('albums', $albums, $record->album_id, ['class'=>'form-control', 'id'=> 'selected_documents' ,'style'=>'width:60%; display: inline-block;margin-right:15px;']  )}} <a style="padding-right:10px" href="#" id="refresh-select-doc"><i class="fa fa-refresh"></i></a> <a target="_blank" href="/admin/album/{{$record->album_id}}">К прикрепленному альбому</a><br>
                <label> Выберите изображения:</label>
                <div class="content" style="min-height: 0px;">
                    <div id="documents" class="row"></div>
                </div>
            </div>

                    <div class="form-group">
                        <label>Опубликовать?</label>

                        {{Form::select('public',['1' => 'Да', '0' => 'Нет'], null, [ 'id'=> 'public_select' ])}}
                    </div>
                    @if( !isset($record) )
                        {{ Form::button('Создать', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @else
                        {{ Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-block btn-primary']) }}
                    @endif

                    {{ Form::close() }}
        </div>
    </div>

@endsection

@section('js')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script> CKEDITOR.timestamp= Date.now(); </script>

    <script> $(function () {
            CKEDITOR.replace('editor0');
            CKEDITOR.replace('editor1');
            CKEDITOR.replace('editor2');
            CKEDITOR.replace('editor3');


            $("form").submit( function(e) {
                var name_ru = $("#name_ru").val();
                if( name_ru== '') {
                    $("#name_ru").css({ 'border-color': "red" });
                    e.preventDefault();
                    $(".nav-tabs .active").removeClass('active');
                    $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                    $(".tab-content .active").removeClass('active');
                    $(".tab-content #tab_0").addClass('active');

                }

                if ($('#public_select option:checked').val() == '1') {

                var messageLength_ru = CKEDITOR.instances['editor0'].getData().replace(/<[^>]*>/gi, '').length;
                    if( !messageLength_ru ) {
                        $("#cke_editor0").css({ 'border-color': "red" });
                        $(".nav-tabs .active").removeClass('active');
                        $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                        $(".tab-content .active").removeClass('active');
                        $(".tab-content #tab_0").addClass('active');
                        e.preventDefault();
                    }
                    if( !messageLength_ru ) {
                        alert( 'Название или текст новости не может быть пустой' );
                    }
                    var preview = $('#gallery input[name="images[]"]').val();
                    if( !preview ) {
                        alert( 'В страницы должна быть миниатюра. Добавте ее пожалуйста' );
                        e.preventDefault();
                    }

                    var datepicker = $("#datepicker").val();
                    if( datepicker== '') {
                        $("#datepicker").css({ 'border-color': "red" });
                        e.preventDefault();
                        $(".nav-tabs .active").removeClass('active');
                        $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                        $(".tab-content .active").removeClass('active');
                        $(".tab-content #tab_0").addClass('active');

                    }
                    if( !datepicker ) {
                        alert( 'Дата рождения не может быть пустой' );
                    }
                    var sum_need = $("#sum_need").val();
                    if( sum_need== '') {
                        $("#sum_need").css({ 'border-color': "red" });
                        e.preventDefault();
                        $(".nav-tabs .active").removeClass('active');
                        $(".nav-tabs a[href$='#tab_0']").parents("li").addClass('active');
                        $(".tab-content .active").removeClass('active');
                        $(".tab-content #tab_0").addClass('active');

                    }
                    if( !sum_need ) {
                        alert( 'Нужная сума не может быть пустой' );
                    }

                }



            });
            $('#name_ru').on('click',function(e) {
                $("#name_ru").css({'border-color': ""});
            });
            $('#datepicker').on('click',function(e) {
                $("#datepicker").css({'border-color': ""});
            });
            $('#sum_need').on('click',function(e) {
                $("#sum_need").css({'border-color': ""});
            });

           CKEDITOR.instances['editor0'].on('change', function() {
                $("#cke_editor0").css({'border-color': ""});
            });

        })

    </script>

        <script> $('#selected_album').on('change',function(e){
                    e.preventDefault();
                    var albId = $('#selected_album option:selected').val();
                    load_photo(albId);

        });
        $(document).ready(function () {

            var albId = $('#selected_album :selected').val();
            load_photo(albId);
        });


            function load_photo (albId) {
                $.ajax({
                    type: "GET",
                    url: '/admin/album/'+ albId ,
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: albId,
                    },
                    success: function(data){
                        $("#photos").empty();
                        $.each(data.response.photos, function(idx, photos){
                            $("#photos").append('<div class="col-sm-1 col-xs-4"> <a href="#" class="select_image" data-id="'+photos.id + '" data-icon="'+ photos.thumb_url + '"> <img style="width:80px;" src="' +photos.thumb_url + '"> </a> </div>');
                        });
                        console.log("ajaxdata",data);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        </script>
        <script>
            $(document).on('click', '.select_image', function(e) {
                e.preventDefault();
                if ($('#gallery .image-thumb').length <11) {
                    $('#gallery').append('<div class="col-sm-3 col-xs-4 image-thumb""><img src="' + $(this).data("icon") + '" class="selected_image" >' +
                            '<input type="hidden" name="images[]" value="' + $(this).data("id") + '"> </div>');
                } else {
                    alert( 'Нельзя добавить более 11 изображений в слайдер' );
                }
                  });

            $(document).on('click', '.image-thumb', function(e) {
                e.preventDefault();
                var imgId = $(this).data("id");
                $(this).remove();
            });
        </script>

    <script> $('#selected_documents').on('change',function(e){
                    e.preventDefault();
                    var albIdDoc = $('#selected_documents option:selected').val();
                    load_documents(albIdDoc);

        });
        $(document).ready(function () {

            var albIdDoc = $('#selected_documents :selected').val();
            load_documents(albIdDoc);
        });


            function load_documents (albIdDoc) {
                $.ajax({
                    type: "GET",
                    url: '/admin/album/'+ albIdDoc ,
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: albIdDoc,
                    },
                    success: function(data){
                        $("#documents").empty();
                        $.each(data.response.photos, function(idx, photos){
                            $("#documents").append('<div class="col-sm-1 col-xs-4"> <a href="#" class="select_doc" data-id="'+photos.id + '" data-icon="'+ photos.thumb_url + '"> <img style="width:80px;" src="' +photos.thumb_url + '"> </a> </div>');
                        });
                        console.log("ajaxdata",data);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        </script>
        <script>
            $(document).on('click', '.select_doc', function(e) {
                e.preventDefault();
                $('#gallery_doc').append('<div class="col-sm-3 col-xs-4 image-thumb""><img src="'+ $(this).data("icon") +'" class="selected_image" >' +
                        '<input type="hidden" name="documents[]" value="'+ $(this).data("id") +'"> </div>' );
            });

            $(document).on('click', '.image-thumb', function(e) {
                e.preventDefault();
                var imgId = $(this).data("id");
                $(this).remove();
            });
            $(function(){
                $('#datepicker').datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    toggleActive: true
                });
            });
        </script>
        <script>
            $(document).on('click', '#refresh-select', function(e) {
                $('#selected_album').trigger('change');
            });
            $(document).on('click', '#refresh-select-doc', function(e) {
                $('#selected_documents').trigger('change');
            });
            $(document).on('click', '#refresh-select-prev', function(e) {
                $('#selected_preview_alb').trigger('change');
            });
        </script>


    <script>

        $('#sum_need').keypress(function(event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

    </script>
    <script src="/js/cropper.min.js"></script>

    <script> $('#selected_preview_alb').on('change',function(e){
            e.preventDefault();
            var albIdPrew = $('#selected_preview_alb option:selected').val();
            load_preview(albIdPrew);

        });
        $(document).ready(function () {

            var albIdPrew = $('#selected_preview_alb :selected').val();
            load_preview(albIdPrew);
        });


        function load_preview (albIdPrew) {
            $.ajax({
                type: "GET",
                url: '/admin/album/'+ albIdPrew ,
                data: {
                    _token: '{{ csrf_token() }}',
                    id: albIdPrew,
                },
                success: function(data){
                    $("#prew-photos").empty();
                    $.each(data.response.photos, function(idx, photos){
                        $("#prew-photos").append('<div class="col-sm-1 col-xs-4"> <a href="#" class="select_preview" data-id="'+photos.id + '" data-icon="'+ photos.thumb_url + '" data-url="'+ photos.url + '"> <img style="width:80px;" src="' +photos.thumb_url + '"> </a> </div>');
                    });
                    console.log("ajaxdata",data);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        }
    </script>
    <script>
        var $image = $("#gallery_preview_img");

        $(document).on('click', '.select_preview', function(e) {
            e.preventDefault();
            $('#modal-default').show();
//            console.log($(this).data());
            $('#gallery_preview_img').attr('src', $(this).data("url"));
            $('#preview_id').val($(this).data("id"));
                console.log($image);
                originalData = {};
                $image.cropper({
                    viewMode: 1,
                    aspectRatio: 278/150,
                    resizable: true,
                    zoomable: false,
                    rotatable: false,
                    multiple: true,
                    crop: function(event) {
                        $("#detail_x").val(Math.round(event.detail.x));
                        $("#detail_y").val(Math.round(event.detail.y));
                        $("#detail_width").val(Math.round(event.detail.width));
                        $("#detail_height").val(Math.round(event.detail.height));
                        console.log(event.detail.x);
                        console.log(event.detail.y);
                        console.log(event.detail.width);
                        console.log(event.detail.height);
                        console.log(event.detail.scaleX);
                        console.log(event.detail.scaleY);
                    },
                    dragend: function(data) {
                        originalData = $image.cropper("getCroppedCanvas");
                        console.log(originalData.toDataURL());
                        $('.data-url').text(originalData.toDataURL());
                    }
                });

            var cropper = $image.data('cropper');
//            console.log(cropper);
            cropper.replace($(this).data("url"));
        });





    </script>



@stop

