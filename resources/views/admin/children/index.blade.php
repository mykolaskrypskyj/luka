@extends('adminlte::page')
@section('title', 'Дети')
@section('content_header')
    <h1>Дети</h1>
    <div class="breadcrumb">
    {{ Form::open( array('url' => 'admin/children/', 'style'=> 'display: inline-flex; width:500px;')) }}
        {{ Form::text('name_ru',null,['class'=>'form-control','placeholder'=>'Введите ФИО','id'=> 'name_ru', 'required', 'style'=>'width:69%' ]) }}

        {{ Form::button('Создать', ['type' => 'submit', 'class' => 'btn btn-block btn-primary', 'style'=>'width:29%']) }}

    {{ Form::close() }}
    </div>


@stop
@section('content')

    @if(Session::has('message'))
        <div class="callout callout-info">
            {{Session::get('message')}}
        </div>
    @endif
    <div class="">
        <div class="row">
            <div class="col-xs-12">
                <hr>
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <td>id</td>
                        <td>Миниатюра</td>
                        <td style="width:40%;">Имя</td>
                        <td>Нужно собрать</td>
                        <td>Количество платежей</td>
                        <td>Собрано денег</td>
                        <td>Деньги собраны</td>
                        <td>Опубликовано</td>
                        <td>Действие</td>
                    </tr>
                </thead>


                </table>
            </div>
        </div>
    </div>

@endsection


@section('css')
    <style>
        button {
            border:0;
            background:none;
            color: #3c8dbc;
        }
    </style>
@stop

@section('js')
    <script>
        $(document).ready(function () {
            $('#example1').dataTable({
                processing: true,
                order: [ [0, 'desc'] ],
                scrollY:        "700px",
                scrollCollapse: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Поиск"
                },
                autowidth: false,
                lengthMenu: [[50, 250, 500, -1], [50, 250, 500, "All"]],
                ajax: '{!! route('get.children.data') !!}',

                columns: [
                    { data: 'id'},
                    { data: 'img'},
                    { data: 'name'},
                    { data: 'need'},
                    { data: 'alreadycount'},
                    { data: 'already'},
                    { data: 'collected_flag', name: 'collected_flag', searchable: false },
                    { data: 'public', name: 'public', searchable: false },
                    { data: 'edit', name: 'edit', orderable: false, searchable: false }
                ]
            });
        });
    </script>
@stop
