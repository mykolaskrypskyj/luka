<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'admin',
            'email'=>'admin@luka.dev',
            'password'=>bcrypt('admin'),
        ]);
        DB::table('cities')->insert([
            'name_ru'=>'Одесса',
            'name_ua'=>'Одеса',
            'name_en'=>'Odessa',
            'name_de'=>'Odessa',
        ]);
    }
}
