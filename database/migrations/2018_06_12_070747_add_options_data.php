<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOptionsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            'phone' => '+38 (000) 000 0 000',
            'email' => 'lukafund@gmail.com',

        ];
        App\Options::create(['key' => 'contacts', 'data' => $data]);

        $data = [
            'facebook_link' => '#',
            'youtube_link' => '#',

        ];
        App\Options::create(['key' => 'social_network', 'data' => $data]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        App\Options::where('key','social_network')->delete();
        App\Options::where('key','contacts')->delete();

    }
}
