<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeContentField4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staff', function($table)
        {
            $table->dropForeign('staff_photo_id_foreign');
            $table->integer('photo_id')->unsigned()->nullable()->change();
            $table->foreign('photo_id')->references('id')->on('photos')->onDelete('set null')->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
