<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLiqpayOptionsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            'public_key' => 'i87801715867',
            'private_key' => 'B0FoeCkNXxJXIkLRE1LpA1yCyS2PfD194qHk6HHJ',

        ];
        App\Options::create(['key' => 'liqpay', 'data' => $data]);

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        App\Options::where('key','liqpay')->delete();

    }
}
