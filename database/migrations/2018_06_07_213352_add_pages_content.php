<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPagesContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            'title_ru' => 'Получить помощь',
            'content_ru' => '{"text_before_file_ru":"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit blanditiis consequatur libero totam ipsa deleniti magni. Ut delectus, perspiciatis maiores et iure quibusdam quas quam cumque laboriosam molestiae officia inventore consequuntur quisquam excepturi, alias labore optio sint natus molestias. Blanditiis repellendus praesentium beatae nisi aliquid quo labore omnis ipsam nam aperiam quia neque, maiores veritatis dolores! Natus tempore nihil quisquam quibusdam aliquam nulla recusandae adipisci, minima repudiandae autem laudantium. Alias labore optio sint natus molestias. Blanditiis repellendus praesentium beatae nisi aliquid quo labore omnis ipsam nam aperiam quia neque, maiores veritatis dolores! Natus tempore nihil quisquam quibusdam aliquam nulla recusandae adipisci, minima repudiandae autem laudantium. Natus tempore nihil quisquam quibusdam aliquam nulla recusandae adipisci, minima repudiandae autem laudantium. Alias labore optio sint natus molestias. Blanditiis repellendus praesentium beatae nisi aliquid quo labore omnis ipsam nam aperiam quia neque, maiores veritatis dolores! Natus tempore nihil quisquam quibusdam aliquam nulla recusandae adipisci, minima repudiandae autem laudantium.<\/p>","text_after_file_ru":"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit blanditiis consequatur libero totam ipsa deleniti magni. Ut delectus, perspiciatis maiores et iure quibusdam quas quam cumque laboriosam molestiae officia inventore consequuntur quisquam excepturi, alias labore optio sint natus molestias. Blanditiis repellendus praesentium beatae nisi aliquid quo labore omnis ipsam nam aperiam quia neque, maiores veritatis dolores! Natus tempore nihil quisquam quibusdam aliquam nulla recusandae adipisci, minima repudiandae autem laudantium. Alias labore optio sint natus molestias. Blanditiis repellendus praesentium beatae nisi aliquid quo labore omnis ipsam nam aperiam quia neque, maiores veritatis dolores! Natus tempore nihil quisquam quibusdam aliquam nulla recusandae adipisci, minima repudiandae autem laudantium. Natus tempore nihil quisquam quibusdam aliquam nulla recusandae adipisci, minima repudiandae autem laudantium. Alias labore optio sint natus molestias. Blanditiis repellendus praesentium beatae nisi aliquid quo labore omnis ipsam nam aperiam quia neque, maiores veritatis dolores! Natus tempore nihil quisquam quibusdam aliquam nulla recusandae adipisci, minima repudiandae autem laudantium.<\/p>","file_ru":"\/ankets\/anketa_ru.doc"}',
            'content_ua' => '{"text_before_file_ua":"<p><\/p>","text_after_file_ua":"<p><\/p>","file_ua":"\/ankets\/anketa_ua.doc"}',
            'content_en' => '{"text_before_file_en":"<p><\/p>","text_after_file_en":"<p><\/p>","file_en":"\/ankets\/anketa_en.doc"}',
            'content_de' => '{"text_before_file_de":"<p><\/p>","text_after_file_de":"<p><\/p>","file_de":"\/ankets\/anketa_de.doc"}',
        ];
        DB::table('pages')
            ->where('type','takehelp')
            ->update($data);

        $data = [
            'title_ru' => 'Наша команда',
            'content_ru' => '{"text_ru":"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit blanditiis consequatur libero totam ipsa deleniti magni. Ut delectus, perspiciatis maiores et iure quibusdam quas quam cumque laboriosam molestiae officia inventore consequuntur quisquam excepturi, alias labore optio sint natus molestias. Blanditiis repellendus praesentium beatae nisi aliquid quo labore omnis ipsam nam aperiam quia neque, maiores veritatis dolores! Natus tempore nihil quisquam quibusdam aliquam nulla recusandae adipisci, minima repudiandae autem laudantium. Amet laudantium mollitia voluptatem assumenda ducimus officiis aliquid odio ullam sequi eos blanditiis voluptates numquam vero soluta aut, dolor doloribus obcaecati perspiciatis eaque quis! Est labore dicta dolorum illo expedita possimus! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit blanditiis consequatur libero totam ipsa deleniti magni. Ut delectus, perspiciatis maiores et iure quibusdam quas quam cumque laboriosam molestiae officia inventore consequuntur quisquam excepturi, alias labore optio sint natus molestias. Blanditiis repellendus praesentium beatae nisi aliquid quo labore omnis ipsam nam aperiam quia neque, maiores veritatis dolores<\/p>\r\n\r\n<ul>\r\n<\/ul>"}',
            'content_ua' => '{"text_ua":"<p><\/p>"}',
            'content_en' => '{"text_en":"<p><\/p>"}',
            'content_de' => '{"text_de":"<p><\/p>"}',
        ];
        DB::table('pages')
            ->where('type','team')
            ->update($data);

        $data = [
            'title_ru' => 'Наша команда',
            'content_ru' => '{"text_ru":"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit blanditiis consequatur libero totam ipsa deleniti magni. Ut delectus, perspiciatis maiores et iure quibusdam quas quam cumque laboriosam molestiae officia inventore consequuntur quisquam excepturi, alias labore optio sint natus molestias. Blanditiis repellendus praesentium beatae nisi aliquid quo labore omnis ipsam nam aperiam quia neque, maiores veritatis dolores! Natus tempore nihil quisquam quibusdam aliquam nulla recusandae adipisci, minima repudiandae autem laudantium. Amet laudantium mollitia voluptatem assumenda ducimus officiis aliquid odio ullam sequi eos blanditiis voluptates numquam vero soluta aut, dolor doloribus obcaecati perspiciatis eaque quis! Est labore dicta dolorum illo expedita possimus! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit blanditiis consequatur libero totam ipsa deleniti magni. Ut delectus, perspiciatis maiores et iure quibusdam quas quam cumque laboriosam molestiae officia inventore consequuntur quisquam excepturi, alias labore optio sint natus molestias. Blanditiis repellendus praesentium beatae nisi aliquid quo labore omnis ipsam nam aperiam quia neque, maiores veritatis dolores<\/p>\r\n\r\n<ul>\r\n<\/ul>"}',
            'content_ua' => '{"text_ua":"<p><\/p>"}',
            'content_en' => '{"text_en":"<p><\/p>"}',
            'content_de' => '{"text_de":"<p><\/p>"}',
        ];
        DB::table('pages')
            ->where('type','programs')
            ->update($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {


    }
}
