<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildrenPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('children_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('children_id')->unsigned();
            $table->integer('photos_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('children_photos', function($table) {
            $table->foreign('children_id')->references('id')->on('children')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('photos_id')->references('id')->on('photos')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('children_photos', function (Blueprint $table) {
            $table->dropForeign('children_photos_children_id_foreign');
            $table->dropForeign('children_photos_photos_id_foreign');
        });

        Schema::dropIfExists('children_photos');
    }
}
