<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrencyKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            '_currency' => 'грн.',
            'ua_currency' => 'грн.',
            'en_currency' => 'UAH',
            'de_currency' => 'UAH',

        ];
        App\Options::create(['key' => 'currency', 'data' => $data]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        App\Options::where('key','currency')->delete();
    }
}
