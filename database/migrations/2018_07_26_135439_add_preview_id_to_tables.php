<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPreviewIdToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->integer('preview_id')->nullable()->default(null)->unsigned();
            $table->foreign('preview_id')->references('id')->on('photos')->onDelete('set null')->onUpdate('no action');

        });

        Schema::table('stocks', function (Blueprint $table) {
            $table->integer('preview_id')->nullable()->default(null)->unsigned();
            $table->foreign('preview_id')->references('id')->on('photos')->onDelete('set null')->onUpdate('no action');

        });

        Schema::table('staff', function (Blueprint $table) {
            $table->integer('preview_id')->nullable()->default(null)->unsigned();
            $table->foreign('preview_id')->references('id')->on('photos')->onDelete('set null')->onUpdate('no action');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('preview_id');
            $table->dropForeign('news_preview_id_foreign');

        });

        Schema::table('stocks', function (Blueprint $table) {
            $table->dropColumn('preview_id');
            $table->dropForeign('stocks_preview_id_foreign');

        });

        Schema::table('staff', function (Blueprint $table) {
            $table->dropColumn('preview_id');
            $table->dropForeign('staff_preview_id_foreign');

        });
    }
}
