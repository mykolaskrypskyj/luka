<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('children', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ru',100);
            $table->string('name_ua',100)->nullable();
            $table->string('name_en',100)->nullable();
            $table->string('name_de',100)->nullable();
            $table->integer('city_id')->unsigned()->nullable();;
            $table->float('sum_need')->nullable();
            $table->date('born')->nullable();
            $table->text('short_ru')->nullable();
            $table->text('short_ua')->nullable();
            $table->text('short_en')->nullable();
            $table->text('short_de')->nullable();
            $table->text('details_ru')->nullable();
            $table->text('details_ua')->nullable();
            $table->text('details_en')->nullable();
            $table->text('details_de')->nullable();
            $table->integer('album_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('public');
            $table->timestamps();
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('album_id')->references('id')->on('albums');
            $table->foreign('user_id')->references('id')->on('users');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('children', function (Blueprint $table) {
            $table->dropForeign('children_city_id_foreign');
            $table->dropForeign('children_user_id_foreign');
            $table->dropForeign('children_album_id_foreign');
        });
        Schema::dropIfExists('children');
    }
}
