<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeContentField2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stocks', function($table)
        {
            $table->text('content_ru')->nullable()->change();
        });
        Schema::table('specinfo', function($table)
        {
            $table->text('video_ru')->nullable()->change();
            $table->text('details_ru')->nullable()->change();
        });
        Schema::table('staff', function($table)
        {
            $table->text('contacts')->nullable()->change();
            $table->text('details_ru')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stocks', function($table)
        {
            $table->text('content_ru')->change();
        });
        Schema::table('specinfo', function($table)
        {
            $table->text('video_ru')->change();
            $table->text('details_ru')->change();
        });
        Schema::table('staff', function($table)
        {
            $table->text('contacts')->change();
            $table->text('details_ru')->change();
        });
    }
}
