<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donate', function (Blueprint $table) {
            $table->increments('id');
            $table->float('sum');
            $table->integer('children_id')->unsigned();
            $table->string('channel');
            $table->integer('status');
            $table->timestamps();
            $table->foreign('children_id')->references('id')->on('children');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('donate', function (Blueprint $table) {
            $table->dropForeign('donate_children_id_foreign');
        });

        Schema::dropIfExists('donate');
    }
}
