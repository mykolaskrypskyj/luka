<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLangOptionsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            'all_lang' => 'ru,ua,en,de',
            'entered_lang' => 'ru,ua',

        ];
        App\Options::create(['key' => 'language', 'data' => $data]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        App\Options::where('key','language')->delete();

    }
}
