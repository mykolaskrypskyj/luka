<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSettingsForPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            'name' => 'ВБО &quot;Новий фонд допомоги&quot;',
            'name2' => 'Печерське від ПАТ &quot;ЗАХВАТБАНК&quot; м. Київ',
            'kod' => '12345678',
            'score' => '12345678901234',
            'mfo' => '123456',
        ];        
        App\Options::create(['key' => 'payment', 'data' => $data]);
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        App\Options::where('key','payment')->delete();
    }
}
