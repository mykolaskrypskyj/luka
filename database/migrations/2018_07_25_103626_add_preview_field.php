<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPreviewField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('children', function (Blueprint $table) {
            $table->integer('preview_id')->nullable()->default(null)->unsigned();
            $table->foreign('preview_id')->references('id')->on('photos')->onDelete('set null')->onUpdate('no action');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('children', function (Blueprint $table) {
            $table->dropColumn('preview_id');
            $table->dropForeign('children_preview_id_foreign');

        });
    }
}
