<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',100);
            $table->string('video_ru',100);
            $table->string('video_ua',100)->nullable();
            $table->string('video_en',100)->nullable();
            $table->string('video_de',100)->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('public')->unsigned();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->dropForeign('videos_user_id_foreign');
        });

        Schema::dropIfExists('videos');
    }
}
