<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable()->default(NULL);
            $table->integer('album_id')->unsigned();
            $table->string('mime',100);
            $table->string('name',100);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null')->onUpdate('no action');
            $table->foreign('album_id')->references('id')->on('albums')->onDelete('cascade')->onUpdate('no action');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('photos', function (Blueprint $table) {
                    $table->dropForeign('photos_user_id_foreign');
                    $table->dropForeign('photos_album_id_foreign');
        });
        
        Schema::dropIfExists('photos');
    }
}
