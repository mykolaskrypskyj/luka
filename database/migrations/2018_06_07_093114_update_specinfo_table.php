<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSpecinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('specinfo', function (Blueprint $table) {
            $table->dropColumn('details_ru');
            $table->dropColumn('details_ua');
            $table->dropColumn('details_en');
            $table->dropColumn('details_de');

        });
        Schema::table('specinfo', function (Blueprint $table) {
            $table->text('details_ru');
            $table->text('details_ua')->nullable();
            $table->text('details_en')->nullable();
            $table->text('details_de')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('specinfo', function (Blueprint $table) {
            $table->dropColumn('details_ru');
            $table->dropColumn('details_ua');
            $table->dropColumn('details_en');
            $table->dropColumn('details_de');

        });
        Schema::table('specinfo', function (Blueprint $table) {
            $table->string('details_ru',100);
            $table->string('details_ua',100)->nullable();
            $table->string('details_en',100)->nullable();
            $table->string('details_de',100)->nullable();

        });

    }
}
