<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ru',100);
            $table->string('name_ua',100)->nullable();
            $table->string('name_en',100)->nullable();
            $table->string('name_de',100)->nullable();
            $table->string('position_ru',100);
            $table->string('position_ua',100)->nullable();
            $table->string('position_en',100)->nullable();
            $table->string('position_de',100)->nullable();
            $table->text('contacts');
            $table->integer('photo_id')->unsigned();;
            $table->text('details_ru')->nullable();
            $table->text('details_ua')->nullable();
            $table->text('details_en')->nullable();
            $table->text('details_de')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('public')->unsigned();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('photo_id')->references('id')->on('photos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staff', function (Blueprint $table) {
            $table->dropForeign('staff_photo_id_foreign');
            $table->dropForeign('staff_user_id_foreign');
        });

        Schema::dropIfExists('staff');
    }
}
