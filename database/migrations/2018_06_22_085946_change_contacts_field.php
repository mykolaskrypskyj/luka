<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeContactsField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staff', function($table)
        {
            $table->text('telephon',170)->nullable();
            $table->text('email',170)->nullable();
            $table->text('adress',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staff', function($table)
        {
            $table->dropColumn('telephon',170);
            $table->dropColumn('email',170);
            $table->dropColumn('address',255);
        });
    }
}
