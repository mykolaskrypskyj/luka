<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSpecinfoTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('specinfo', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->string('title_ru',100);
            $table->string('title_ua',100)->nullable();
            $table->string('title_de',100)->nullable();
            $table->string('title_en',100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('specinfo', function (Blueprint $table) {
            $table->dropColumn('title_ru',100);
            $table->dropColumn('title_ua',100);
            $table->dropColumn('title_de',100);
            $table->dropColumn('title_en',100);
            $table->string('title',100);

        });
    }
}
