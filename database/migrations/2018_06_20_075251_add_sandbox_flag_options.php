<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSandboxFlagOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $fields = App\Options::findOrFail(['key' => 'liqpay'])->first();
        $data = json_decode($fields->values, true);
        $data['sandbox']= '1';

        \DB::table('options')->where('key', 'liqpay')->update(['values' => json_encode($data)]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
