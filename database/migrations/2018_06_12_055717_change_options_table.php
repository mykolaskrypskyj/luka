<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('options');

        Schema::create('options', function (Blueprint $table) {
            $table->string('key',100)->primary();
            $table->text('values')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('options');

        Schema::create('options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key',100);
            $table->text('values')->nullable();
            $table->timestamps();
        });
    }
}
