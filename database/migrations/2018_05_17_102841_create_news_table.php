<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_ru');
            $table->string('short_ru');
            $table->text('content_ru');
            $table->string('title_ua')->nullable();
            $table->string('short_ua')->nullable();
            $table->text('content_ua')->nullable();
            $table->string('title_en')->nullable();
            $table->string('short_en')->nullable();
            $table->text('content_en')->nullable();
            $table->string('title_de')->nullable();
            $table->string('short_de')->nullable();
            $table->text('content_de')->nullable();
            $table->integer('user_id')->unsigned();
            $table->boolean('public');
            $table->timestamps();
        });

        Schema::table('news', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
                    $table->dropForeign('news_user_id_foreign');
        });
        
        Schema::dropIfExists('news');
    }
}
