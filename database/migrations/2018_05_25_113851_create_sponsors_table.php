<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSponsorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sponsors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',100);
            $table->integer('photo_id')->unsigned();;
            $table->string('link',100);
            $table->integer('user_id')->unsigned();
            $table->integer('public')->unsigned();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('photo_id')->references('id')->on('photos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sponsors', function (Blueprint $table) {
            $table->dropForeign('sponsors_photo_id_foreign');
            $table->dropForeign('sponsors_user_id_foreign');
        });
        Schema::dropIfExists('sponsors');
    }
}
