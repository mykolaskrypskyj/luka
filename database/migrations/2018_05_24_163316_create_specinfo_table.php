<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specinfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',100);
            $table->string('video_ru',100);
            $table->string('video_ua',100)->nullable();
            $table->string('video_en',100)->nullable();
            $table->string('video_de',100)->nullable();
            $table->string('details_ru',100);
            $table->string('details_ua',100)->nullable();
            $table->string('details_en',100)->nullable();
            $table->string('details_de',100)->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('public')->unsigned();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('specinfo', function (Blueprint $table) {
            $table->dropForeign('specinfo_user_id_foreign');
        });
        Schema::dropIfExists('specinfo');
    }
}
