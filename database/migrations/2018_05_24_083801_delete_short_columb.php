<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteShortColumb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stocks', function (Blueprint $table) {
            $table->dropColumn('short_ru');
            $table->dropColumn('short_ua');
            $table->dropColumn('short_en');
            $table->dropColumn('short_de');
        });
        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('short_ru');
            $table->dropColumn('short_ua');
            $table->dropColumn('short_en');
            $table->dropColumn('short_de');
        });
        Schema::table('children', function (Blueprint $table) {
            $table->dropColumn('short_ru');
            $table->dropColumn('short_ua');
            $table->dropColumn('short_en');
            $table->dropColumn('short_de');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stocks', function (Blueprint $table) {
            $table->text('short_ru');
            $table->text('short_ua');
            $table->text('short_en');
            $table->text('short_de');
        });
        Schema::table('news', function (Blueprint $table) {
            $table->text('short_ru');
            $table->text('short_ua');
            $table->text('short_en');
            $table->text('short_de');
        });
        Schema::table('children', function (Blueprint $table) {
            $table->text('short_ru');
            $table->text('short_ua');
            $table->text('short_en');
            $table->text('short_de');
        });
    }
}
