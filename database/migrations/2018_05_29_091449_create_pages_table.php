<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->text('type');
            $table->string('title_ru');
            $table->text('content_ru')->nullable();
            $table->string('title_ua')->nullable();
            $table->text('content_ua')->nullable();
            $table->string('title_en')->nullable();
            $table->text('content_en')->nullable();
            $table->string('title_de')->nullable();
            $table->text('content_de')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('photo_id')->unsigned()->nullable();
            $table->boolean('public');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('photo_id')->references('id')->on('photos');


        });
        DB::table('pages')->insert([
            'type'=>'takehelp',
            'title_ru'=>'Получить помощь',
            'user_id'=>1,
            'public'=>'1',

        ]);
        DB::table('pages')->insert([
            'type'=>'team',
            'title_ru'=>'Наша команда',
            'user_id'=>1,
            'public'=>'1',
        ]);
        DB::table('pages')->insert([
            'type'=>'programs',
            'title_ru'=>'Программы',
            'user_id'=>1,
            'public'=>'1',
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->dropForeign('pages_user_id_foreign');
            $table->dropForeign('pages_photo_id_foreign');
        });

        Schema::dropIfExists('pages');
    }
}
