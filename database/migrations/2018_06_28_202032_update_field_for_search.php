<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\News;
use App\Stocks;
use App\Staff;
use App\Specinfo;
use App\Children;

class UpdateFieldForSearch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $news = News::all();
        foreach($news as $item) {
            $data['title_ru'] = $item->title_ru.' ';
            $item->update($data);
        }

        $stocks = Stocks::all();
        foreach($stocks as $item) {
            $data['title_ru']= $item->title_ru.' ';
            $item->update($data);
        }

        $staff = Staff::all();
        foreach($staff as $item) {
            $data['name_ru'] = $item->name_ru.' ';
            $item->update($data);
        }

        $specinfo = Specinfo::all();
        foreach($specinfo as $item) {
            $data['title_ru'] = $item->title_ru.' ';
            $item->update( $data );
        }

        $children = Children::all();
        foreach($children as $item) {
            $data['name_ru'] = $item->name_ru.' ';
            $item->update($data);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
